<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Event::class, function (Faker $faker) {
    $randomTimestamp = $faker->dateTimeBetween('-3 month', '+3 month')->getTimestamp();
    $start = $faker->dateTimeBetween('9:00', '17:00')
        ->setDate(date('Y', $randomTimestamp), date('m', $randomTimestamp), date('d', $randomTimestamp));
    return [
        'name' => $faker->word,
        'description' => $faker->text,
        'all_day' => $faker->boolean,
        'start' => clone $start,
        'end' => $start->modify('+' . $faker->numberBetween(900, 3600) . ' seconds'),
        'user_id' => $faker->numberBetween(3, 8)
    ];
});

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'address' => $faker->address,
        'birthday' => $faker->dateTime('-18 years'),
        'phone' => $faker->phoneNumber,
        'phone_additional' => $faker->phoneNumber,
        'email' => $faker->email,
        'email_additional' => $faker->email,
        'social_links' => ''
    ];
});