<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoryTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('story_types')->insert([
            'type' => 'event_created',
            'name' => 'Создано событие',
            'auto' => '1'
        ]);

        DB::table('story_types')->insert([
            'type' => 'event_updated',
            'name' => 'Событие изменено',
            'auto' => '1'
        ]);

        DB::table('story_types')->insert([
            'type' => 'event_canceled',
            'name' => 'Событие отменено',
            'auto' => '1'
        ]);

        DB::table('story_types')->insert([
            'type' => 'event_deleted',
            'name' => 'Событие удалено',
            'auto' => '1'
        ]);

        DB::table('story_types')->insert([
            'type' => 'call_in',
            'name' => 'Входящий звонок',
            'auto' => '0'
        ]);

        DB::table('story_types')->insert([
            'type' => 'call_out',
            'name' => 'Исходящий звонок',
            'auto' => '0'
        ]);
    }
}
