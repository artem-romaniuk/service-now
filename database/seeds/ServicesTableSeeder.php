<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'name' => 'Консультация врача - терапевта',
            'description' => '',
            'duration_variable' => 30 * 60,
            'duration_text' => '30 минут',
            'service_category_id' => '1',
        ]);

        DB::table('services')->insert([
            'name' => 'Лечение кариеса с наложением пломбы из светоотверждающих композитов (малая)',
            'description' => '',
            'duration_variable' => 60 * 60,
            'duration_text' => '1 час',
            'service_category_id' => '1',
        ]);

        DB::table('services')->insert([
            'name' => 'Изготовление протеза (металлокерамика)',
            'description' => '',
            'duration_variable' => 90 * 60,
            'duration_text' => '1,5 часа',
            'service_category_id' => '2',
        ]);

        DB::table('services')->insert([
            'name' => 'Консультация врача — хирурга',
            'description' => '',
            'duration_variable' => 15 * 60,
            'duration_text' => '15 минут',
            'service_category_id' => '3',
        ]);

        DB::table('services')->insert([
            'name' => 'Удаление зуба (без учета анестезии)',
            'description' => '',
            'duration_variable' => 30 * 60,
            'duration_text' => '30 минут',
            'service_category_id' => '3',
        ]);

        DB::table('services')->insert([
            'name' => 'Анестезия инфильтрационная',
            'description' => '',
            'duration_variable' => 5 * 60,
            'duration_text' => '5 минут',
            'service_category_id' => '4',
        ]);
    }
}
