<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Иван Иванович Иванов',
            'email' => 'artem.romaniuk@gmail.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Директор',
            'image' => '',
            'color' => '#a80000',
            'user_role_id' => '1',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);

        DB::table('users')->insert([
            'name' => 'Анастасия Сергеевна Плюшкина',
            'email' => 'administrator@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Администратор',
            'image' => '',
            'color' => '#edae00',
            'user_role_id' => '2',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);

        DB::table('users')->insert([
            'name' => 'Петр Петрович Петренко',
            'email' => 'employee_1@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Челюстно-лицевой хирур',
            'image' => '',
            'color' => '#1600bd',
            'user_role_id' => '3',
            'user_status_id' => '1',
            'api_token' => str_random(60),
        ]);

        DB::table('users')->insert([
            'name' => 'Сергей Сергеевич Сергеев',
            'email' => 'employee_2@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Челюстно-лицевой хирур',
            'image' => '',
            'color' => '#02ba5e',
            'user_role_id' => '3',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);

        DB::table('users')->insert([
            'name' => 'Василий Васильевич Васечкин',
            'email' => 'employee_3@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Анастезиолог',
            'image' => '',
            'color' => '#ba8902',
            'user_role_id' => '3',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);

        DB::table('users')->insert([
            'name' => 'Дмитрий Дмитриевич Дмитров',
            'email' => 'employee_4@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Протезист',
            'image' => '',
            'color' => '#747696',
            'user_role_id' => '3',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);

        DB::table('users')->insert([
            'name' => 'Ольга Сереевна Васютина',
            'email' => 'employee_5@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Детский терапевт',
            'image' => '',
            'color' => '#74968a',
            'user_role_id' => '3',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);

        DB::table('users')->insert([
            'name' => 'Елена Васильевна Бойко',
            'email' => 'employee_6@user.com',
            'password' => bcrypt('111111'),
            'phone' => '+380991112233',
            'phone_additional' => '',
            'address' => '',
            'position' => 'Детский терапевт',
            'image' => '',
            'color' => '#695112',
            'user_role_id' => '3',
            'user_status_id' => '1',
            'api_token' => str_random(60)
        ]);
    }
}
