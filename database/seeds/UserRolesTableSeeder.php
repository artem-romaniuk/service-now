<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'name' => 'Директор',
            'description' => 'Роль директора предполагает права суперпользователя, который имеет доступ ко всем разделам кабинета с правами создание, редактирования и удаления. А также данная роль позволяет видеть статистическую информацию.',
        ]);

        DB::table('user_roles')->insert([
            'name' => 'Администратор',
            'description' => 'Роль администратора предполагает права пользователя, который имеет доступ ко всем разделам кабинета с правами создание, редактирования и удаления.'
        ]);

        DB::table('user_roles')->insert([
            'name' => 'Работник',
            'description' => 'Роль работника предусматривает доступ только к своим данным.'
        ]);
    }
}
