<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_statuses')->insert([
            'name' => 'Работает',
            'description' => '',
        ]);

        DB::table('user_statuses')->insert([
            'name' => 'Уволен',
            'description' => '',
        ]);

        DB::table('user_statuses')->insert([
            'name' => 'Не активен',
            'description' => '',
        ]);

        DB::table('user_statuses')->insert([
            'name' => 'В отпуске',
            'description' => '',
        ]);
    }
}
