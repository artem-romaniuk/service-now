<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_user')->insert([
            'service_id' => '4',
            'user_id' => '3',
        ]);

        DB::table('service_user')->insert([
            'service_id' => '4',
            'user_id' => '4',
        ]);

        DB::table('service_user')->insert([
            'service_id' => '6',
            'user_id' => '5',
        ]);

        DB::table('service_user')->insert([
            'service_id' => '3',
            'user_id' => '6',
        ]);

        DB::table('service_user')->insert([
            'service_id' => '1',
            'user_id' => '7',
        ]);

        DB::table('service_user')->insert([
            'service_id' => '4',
            'user_id' => '8',
        ]);

        DB::table('service_user')->insert([
            'service_id' => '5',
            'user_id' => '8',
        ]);
    }
}
