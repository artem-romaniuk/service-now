<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_categories')->insert([
            'name' => 'Терапия',
            'description' => '',
        ]);

        DB::table('service_categories')->insert([
            'name' => 'Протезирование',
            'description' => '',
        ]);

        DB::table('service_categories')->insert([
            'name' => 'Хирургия',
            'description' => '',
        ]);

        DB::table('service_categories')->insert([
            'name' => 'Анастезиология',
            'description' => '',
        ]);

        DB::table('service_categories')->insert([
            'name' => 'Детская стоматология',
            'description' => '',
        ]);

        DB::table('service_categories')->insert([
            'name' => 'Ортодонтология',
            'description' => '',
        ]);
    }
}
