<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $this->createRow();
        }
    }

    protected function createRow()
    {
        $faker = resolve('Faker\Generator');
        DB::table('event_client')->insert([
            'event_id' => $faker->numberBetween(1, 10),
            'client_id' => $faker->numberBetween(1, 10)
        ]);
    }
}
