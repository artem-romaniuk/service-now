<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserRolesTableSeeder::class,
            UserStatusesTableSeeder::class,
            UsersTableSeeder::class,
            ServiceCategoriesTableSeeder::class,
            ServicesTableSeeder::class,
            ServiceUserTableSeeder::class,
            StoryTypesTableSeeder::class,
        ]);

        factory(App\Event::class, 10)->create();
        factory(App\Client::class, 10)->create();

        $this->call(EventServiceTableSeeder::class);
        $this->call(EventClientTableSeeder::class);
    }
}