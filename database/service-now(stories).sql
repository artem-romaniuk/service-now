-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `stories`;
CREATE TABLE `stories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `event_id` int(10) unsigned DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL,
  `details` json DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stories_story_type_id_foreign` (`type_id`),
  KEY `stories_client_id_foreign` (`client_id`),
  CONSTRAINT `stories_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stories_story_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `story_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `story_details`;
CREATE TABLE `story_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `story_details` (`id`, `name`) VALUES
(1,	'Добавили услугу'),
(2,	'Добавили комментарий'),
(3,	'Добавили адрес'),
(4,	'Добавили дату рождения'),
(5,	'Добавили телефон'),
(6,	'Добавили email'),
(7,	'Добавили соц. сеть'),
(8,	'Сменили сотрудника'),
(9,	'Сменили комментарий'),
(10,	'Сменили Ф.И.О.'),
(11,	'Сменили адрес'),
(12,	'Сменили дату рождения'),
(13,	'Сменили телефон'),
(14,	'Сменили email'),
(15,	'Сменили соц. сеть'),
(16,	'Удалили услугу'),
(17,	'Удалили комментарий'),
(18,	'Удалили адрес'),
(19,	'Удалили дату рождения'),
(20,	'Удалили телефон'),
(21,	'Удалили email'),
(22,	'Удалили соц. сеть'),
(23,	'Перенесена дата сеанса'),
(24,	'Неявка'),
(25,	'Отмена'),
(26,	'Вручную'),
(27,	'Автоматическое');

DROP TABLE IF EXISTS `story_types`;
CREATE TABLE `story_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `story_types` (`id`, `name`, `auto`, `icon`) VALUES
(1,	'Добавлен клиент',	1,	'client_plus.png'),
(2,	'Изменение информации о клиенте',	1,	'client-edit.png'),
(3,	'Добавлена запись',	1,	'new-event.png'),
(4,	'Изменена запись',	1,	'edit-event.png'),
(5,	'Отменён сеанс',	1,	'event-cancel.png'),
(6,	'Удален сеанс',	1,	'event-delete.png'),
(7,	'СМС сообщения клиенту',	2,	'sms.png'),
(8,	'Входящий звонок',	0,	'incoming-call.png'),
(9,	'Исходящий звонок',	0,	'call-out.png'),
(10,	'Пропущенный звонок',	0,	'missed-call.png');

-- 2019-02-13 09:37:33
