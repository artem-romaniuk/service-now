<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->foreign('user_role_id')->references('id')->on('user_roles')->onDelete('restrict');
            $table->foreign('user_status_id')->references('id')->on('user_statuses')->onDelete('restrict');
        });

        Schema::table('events', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
        });

        Schema::table('services', function(Blueprint $table) {
            $table->foreign('service_category_id')->references('id')->on('service_categories')->onDelete('restrict');
        });

        Schema::table('stories', function(Blueprint $table) {
            $table->foreign('story_type_id')->references('id')->on('story_types')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });

        Schema::table('service_user', function(Blueprint $table) {
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');;
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
        });

        Schema::table('event_client', function(Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');;
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');;
        });

        Schema::table('event_service', function(Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');;
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
}
