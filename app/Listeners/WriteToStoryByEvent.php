<?php

namespace App\Listeners;

use App\Events\EventOperation;
use App\Services\StoryType;
use App\Services\Story;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

class WriteToStoryByEvent
{
    public function __construct()
    {
        //
    }

    public function handle(EventOperation $object)
    {

        $event = $object->event;
        $oldEvent = $object->oldEvent;
        $comment = $object->comment;
        $type = $object->type;
        $oldServices = $object->oldServices;
        $cancelType = $object->cancelType;

        $details = [];

        /*добавлена запись*/
        if($type == 3) {
            $datetime = Carbon::createFromFormat('Y-m-d H:i:s',$event->start);
            $services = $event->services()->get();
            $user = $event->user()->first();
            $comment = "Назначен сеанс на <strong>" . $datetime->format('d.m.Y H:i') . "</strong><br> к сотруднику <strong>" . $user->name . "</strong><br> на услуги:<br>";
            foreach ($services as $service){
                $comment .= "<strong>" . $service->name . "</strong><br>";
            }
        } elseif($type == 4) { /*Изменена запись*/

            /*Проверяем дату старта - изменилась или нет*/
            if($oldEvent->start != $event->start){
                $oldDatetime = Carbon::createFromFormat('Y-m-d H:i:s',$oldEvent->start)->format('d.m.Y H:i');
                $newDatetime = Carbon::createFromFormat('Y-m-d H:i:s',$event->start)->format('d.m.Y H:i');
                $details[23] = ['o' => $oldDatetime,'n' => $newDatetime]; /*Перенесена дата сеанса*/
            }

            /*Сменили сотрудника*/
            if($oldEvent->user_id != $event->user_id){
                $oldUserName = $oldEvent->user()->first()->name;
                $newUserName = $event->user()->first()->name;
                $details[8] = ['o' => $oldUserName,'n' => $newUserName]; /*Сменили сотрудника*/
            }

            /*Проверяем услуги - изменились или нет*/
            $oldServicesIds = $oldServices->pluck('id')->toArray();
            $newServicesIds = $event->services()->pluck('id')->toArray();

            $addedServicesIds = array_diff($newServicesIds, $oldServicesIds); /*ids - которые добавили*/
            $deletedServicesIds = array_diff($oldServicesIds, $newServicesIds); /*ids - которые удалили*/

            if(count($addedServicesIds) > 0) {
                $addedServicesNames = [];
                foreach($event->services()->get() as $service){
                    if(in_array($service->id,$addedServicesIds)) $addedServicesNames[] =  $service->name;
                }
                $details[1] = ['o' => '','n' => implode(', ',$addedServicesNames)]; /*добавили услуги*/
            }

            if(count($deletedServicesIds) > 0) {
                $deletedServicesNames = [];
                foreach($oldServices as $service){
                    if(in_array($service->id,$deletedServicesIds)) $deletedServicesNames[] =  $service->name;
                }
                $details[16] = ['o' => implode(', ',$deletedServicesNames),'n' => '']; /*удалили услуги*/
            }

            /*проверяем комментарий - изменился или нет*/
            if($oldEvent->description != $event->description) {
                if($oldEvent->description == '') $details[2] = ['o' => '','n' => $event->description]; /*Добавили комментарий*/
                elseif($event->description == '') $details[17] = ['o' => $oldEvent->description,'n' => '']; /*Удалили комментарий*/
                else $details[9] = ['o' => $oldEvent->description,'n' => $event->description]; /*Сменили комментарий*/
            }

        } elseif($type == 6) { /*Удален сеанс*/

            $datetime = Carbon::createFromFormat('Y-m-d H:i:s',$event->start);
            $comment = "Сеанс на <strong>" . $datetime->format('d.m.Y H:i') . "</strong> - удален";

        } elseif($type == 5) { /*Сеанс отменен*/
            $datetime = Carbon::createFromFormat('Y-m-d H:i:s',$event->start);
            if($cancelType == 1) {
                $c = "Клиент отменил сеанс на: " . $datetime->format('d.m.Y H:i');
                $details[25] = ['o' => $c,'n' => '']; /*Клиент отменил сеанс + Комментарий*/
            } elseif ($cancelType == 2) { /*Клиент не явился на сеанс + Комментарий*/
                $c = "Клиент не явился на сеанс на: " . $datetime->format('d.m.Y H:i');
                $details[24] = ['o' => $c,'n' => '']; /*Клиент не явился на сеанс + Комментарий*/
            }
        }

        if ($type && $event->clients->count() > 0)
        {
            foreach ($event->clients as $client)
            {
                Story::store([
                    'comment' => $comment,
                    'client_id' => $client->id,
                    'event_id' => $event->id,
                    'type_id' => $type,
                    'details' => json_encode($details),
                    'date' => time(),
                ]);
            }
        }
    }
}
