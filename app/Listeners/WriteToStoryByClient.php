<?php

namespace App\Listeners;

use App\Events\ClientOperation;
use App\Services\StoryType;
use App\Services\Story;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WriteToStoryByClient
{
    public function __construct()
    {
        //
    }

    public function phoneToMask($phone){
        $mask = "+38 (";
        $mask .= $phone[0] . $phone[1] . $phone[2] . ") ";
        $mask .= $phone[3] . $phone[4] . $phone[5] . "-";
        $mask .= $phone[6] . $phone[7] . "-";
        $mask .= $phone[8] . $phone[9];
        return $mask;
    }

    public function handle(ClientOperation $object)
    {

        $newClient = $object->newClient;
        $oldClient = $object->oldClient;
        $comment = $object->comment;
        $type = $object->type;

        $details = [];

        $flag = false; /*Ничего не изменилось*/


        /*Добавлен клиент*/
        if($type == 1) {
            $comment = "Дата добавления: " . $newClient->created_at;
            $flag = true; /*что то изменилось*/
        }

        /*Сравнить старую и новую модель*/

        /*fio*/
        if($oldClient->name != $newClient->name){
            $details[10] = ['o' => $oldClient->name,'n' => $newClient->name];
            $flag = true; /*что то изменилось*/
        }

        /*address*/
        if($oldClient->address != $newClient->address){
            if($oldClient->address == '') $details[3] = ['o' => '','n' => $newClient->address]; /*Добавили адрес*/
            elseif($newClient->address == '') $details[18] = ['o' => $oldClient->address,'n' => '']; /*Удалили адрес*/
            else $details[11] = ['o' => $oldClient->address,'n' => $newClient->address]; /*Сменили адрес*/
            $flag = true; /*что то изменилось*/
        }

        /*birthday*/
        $oldBirthday = $oldClient->birthday ? $oldClient->birthday->toDateString() : null;
        $newBirthday = $newClient->birthday ? $newClient->birthday->toDateString() : null;
        if($oldBirthday != $newBirthday){
            if($oldBirthday == null) $details[4] = ['o' => '','n' => $newBirthday]; /*Добавили дату рождения*/
            elseif($newBirthday == null) $details[19] = ['o' => $oldBirthday,'n' => '']; /*Удалили дату рождения*/
            else $details[12] = ['o' => $oldBirthday,'n' => $newBirthday]; /*Сменили дату рождения*/
            $flag = true; /*что то изменилось*/
        }

        /*phones main*/
        if($oldClient->phone != $newClient->phone){
            $details[13] = ['o' => $this->phoneToMask($oldClient->phone),'n' => $this->phoneToMask($newClient->phone)]; /*Изменили телефон*/
            $flag = true; /*что то изменилось*/
        }

        /*phones additional*/
        if($oldClient->phone_additional != $newClient->phone_additional){
            $oldPhones = json_decode($oldClient->phone_additional,true);
            $newPhones = json_decode($newClient->phone_additional,true);
            if(count($oldPhones) == count($newPhones)){ /*Изменили телефон(ы)*/
                $result = array_diff($newPhones, $oldPhones);
                if(count($result) > 0) $details[13] = ['o' => implode(', ',$oldPhones),'n' => implode(', ',$newPhones)];
            } elseif(count($oldPhones) < count($newPhones)) { /*Добавили телефон(ы)*/
                $result = array_diff($newPhones, $oldPhones);
                if(count($result) > 0) $details[5] = ['o' => '','n' => implode(', ',$result)];
            } else { /*Удалили телефон(ы)*/
                $result = array_diff($oldPhones, $newPhones);
                if(count($result) > 0) $details[20] = ['o' => implode(', ',$result),'n' => ''];
            }
            $flag = true; /*что то изменилось*/
        }

        /*email main*/
        if($oldClient->email != $newClient->email) {
            if($oldClient->email == '') $details[6] = ['o' => '','n' => $newClient->email]; /*Добавили email*/
            elseif($newClient->email == '') $details[21] = ['o' => $oldClient->email,'n' => '']; /*Удалили email*/
            else $details[14] = ['o' => $oldClient->email,'n' => $newClient->email]; /*Сменили email*/
            $flag = true; /*что то изменилось*/
        }

        /*emails additional*/
        if($oldClient->email_additional != $newClient->email_additional){
            $oldEmails = json_decode($oldClient->email_additional,true);
            $newEmails = json_decode($newClient->email_additional,true);
            if(count($oldEmails) == count($newEmails)){ /*Изменили email(ы)*/
                $result = array_diff($newEmails, $oldEmails);
                if(count($result) > 0) $details[14] = ['o' => implode(', ',$oldEmails),'n' => implode(', ',$newEmails)];
            } elseif(count($oldEmails) < count($newEmails)) { /*Добавили email(ы)*/
                $result = array_diff($newEmails, $oldEmails);
                if(count($result) > 0) $details[6] = ['o' => '','n' => implode(', ',$result)];
            } else { /*Удалили email(ы)*/
                $result = array_diff($oldEmails, $newEmails);
                if(count($result) > 0) $details[21] = ['o' => implode(', ',$result),'n' => ''];
            }
            $flag = true; /*что то изменилось*/
        }

        /*comment*/
        if($oldClient->description != $newClient->description) {
            if($oldClient->description == '') $details[2] = ['o' => '','n' => $newClient->description]; /*Добавили комментарий*/
            elseif($newClient->description == '') $details[17] = ['o' => $oldClient->description,'n' => '']; /*Удалили комментарий*/
            else $details[9] = ['o' => $oldClient->description,'n' => $newClient->description]; /*Сменили комментарий*/
            $flag = true; /*что то изменилось*/
        }

        /*socials links*/
        if($oldClient->social_links != $newClient->social_links){
            $oldSocials = $oldClient->social_links;
            $newSocials = $newClient->social_links;
            if(count($oldSocials) == count($newSocials)){ /*Изменили socials*/
                $result = array_diff($newSocials, $oldSocials);
                if(count($result) > 0) {
                    $details[15] = [
                        'o' => $oldSocials ? implode(', ',$oldSocials) : '',
                        'n' => $newSocials ? implode(', ',$newSocials) : ''
                    ];
                    $flag = true; /*что то изменилось*/
                } else {
                    $result = array_diff($oldSocials, $newSocials);
                    if(count($result) > 0) $details[22] = ['o' => implode(', ',$result),'n' => ''];
                    $flag = true; /*что то изменилось*/
                }
            } elseif(count($oldSocials) < count($newSocials)) { /*Добавили socials*/
                $result = array_diff($newSocials, $oldSocials);
                if(count($result) > 0) $details[7] = [
                    'o' => '',
                    'n' => implode(', ',$result)
                ];
                $flag = true; /*что то изменилось*/
            } else { /*Удалили socials*/
                $result = array_diff($oldSocials, $newSocials);
                if(count($result) > 0) $details[22] = ['o' => implode(', ',$result),'n' => ''];
                $flag = true; /*что то изменилось*/
            }
        }


        if($flag){
            Story::store([
                'client_id' => $newClient->id,
                'type_id' => $type,
                'comment' => $comment,
                'details' => json_encode($details),
                'date' => time(),
            ]);
        }

    }
}
