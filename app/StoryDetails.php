<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilterCreator;

class StoryDetails extends Model
{
    protected $fillable = [
        'name',
    ];

    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */
}
