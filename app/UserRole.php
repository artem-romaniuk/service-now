<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilterCreator;

class UserRole extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];


    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */

    /* Relations */
    public function users()
    {
        return $this->hasMany('App\User', 'user_role_id', 'id');
    }
    /* End Relations */
}
