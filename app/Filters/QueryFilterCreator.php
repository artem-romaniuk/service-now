<?php

namespace App\Filters;

use Illuminate\Http\Request;

abstract class QueryFilterCreator
{
    protected $query;

    protected $request;


    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($query)
    {
        $this->query = $query;

        foreach ($this->filters() as $filter => $value) {
            $filterMethod = 'filter' . ucwords($filter);

            if (method_exists($this, $filterMethod)) {
                $this->$filterMethod($value);
            }
        }
    }

    protected function filters()
    {
        return $this->request->all();
    }
}