<?php

namespace App\Filters;


class ScheduleExceptionFilter extends QueryFilterCreator
{
    public function filterUser($value)
    {
        $this->query->where('user_id', '=', $value);
    }
}