<?php

namespace App\Filters;

use App\Filters\QueryFilterCreator;

class StoryTypeFilter extends QueryFilterCreator
{
    public function filterName($value)
    {
        $this->query->name('name', 'like', '%' . $value . '%');
    }
}