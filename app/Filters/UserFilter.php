<?php

namespace App\Filters;

use App\Filters\QueryFilterCreator;

class UserFilter extends QueryFilterCreator
{
    public function filterName($value)
    {
        $this->query->where('name', 'like', '%' . $value . '%');
    }

    public function filterRole($value)
    {
        $this->query->where('user_role_id', '=', (int) $value);
    }

    public function filterDate($value)
    {
        $this->query->where('user_status_id', '=', '1');
    }
}