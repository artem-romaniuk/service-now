<?php

namespace App\Filters;

use App\Filters\QueryFilterCreator;

class ServiceCategoryFilter extends QueryFilterCreator
{
    public function filterName($value)
    {
        $this->query->where('name', 'like', '%' . $value . '%');
    }
}