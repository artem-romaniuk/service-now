<?php

namespace App\Filters;

use App\Filters\QueryFilterCreator;

class EventFilter extends QueryFilterCreator
{
    public function filterName($value)
    {
        $this->query->where('name', 'like', '%' . $value . '%');
    }
}