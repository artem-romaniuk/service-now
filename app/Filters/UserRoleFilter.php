<?php
/**
 * Created by PhpStorm.
 * User: Energy
 * Date: 22.10.2018
 * Time: 12:07
 */

namespace App\Filters;


class UserRoleFilter extends QueryFilterCreator
{
    public function filterName($value)
    {
        $this->query->where('name', 'like', '%' . $value . '%');
    }
}