<?php

namespace App\Filters;

use App\Filters\QueryFilterCreator;

class StoryFilter extends QueryFilterCreator
{
    public function filterComment($value)
    {
        $this->query->where('comment', 'like', '%' . $value . '%');
    }
}