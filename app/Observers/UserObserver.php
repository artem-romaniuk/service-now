<?php

namespace App\Observers;

use App\Mail\RegisterUserNotificationMail;
use App\User;
use Illuminate\Support\Facades\Mail;

class UserObserver
{
    public function creating(User $user)
    {
        if (empty($user->api_token)) {

            $user->api_token = str_random(60);
        }

        Mail::send(new RegisterUserNotificationMail($user));

        $user->password = bcrypt($user->password);
    }
}