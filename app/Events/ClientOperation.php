<?php

namespace App\Events;

use App\Client;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ClientOperation
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $newClient;

    public $oldClient;

    public $type;

    public $comment;


    public function __construct(Client $newClient, Client $oldClient, $type, $comment = '')
    {
        $this->newClient = $newClient;
        $this->oldClient = $oldClient;
        $this->type = $type;
        $this->comment = $comment;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
