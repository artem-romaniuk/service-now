<?php

namespace App\Events;

use App\Event;
use App\Services;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventOperation
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $event;

    public $oldEvent;

    public $type;

    public $comment;

    public $oldServices;

    public $cancelType;


    public function __construct(Event $event, Event $oldEvent, $type, $oldServices, $comment = '', $cancelType = null)
    {
        $this->event = $event;
        $this->oldEvent = $oldEvent;
        $this->type = $type;
        $this->comment = $comment;
        $this->oldServices = $oldServices;
        $this->cancelType = $cancelType;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
