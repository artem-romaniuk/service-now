<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilterCreator;

class Story extends Model
{
    protected $fillable = [
        'comment',
        'client_id',
        'event_id',
        'type_id',
        'details',
        'date',
    ];

    public $timestamps = false;

    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */


    /* Relations */
    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }
    /* End Relations */
}
