<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilterCreator;

class StoryType extends Model
{
    protected $fillable = [
        'type',
        'name',
        'auto',
    ];

    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */
}
