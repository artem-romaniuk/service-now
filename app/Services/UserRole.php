<?php

namespace App\Services;

use App\UserRole as UserRoleModel;

class UserRole
{
    public static function all($filters)
    {
        return UserRoleModel::with(['users'])->filter($filters)->get();
    }
}