<?php

namespace App\Services;

use App\Event as EventModel;
use Carbon\Carbon;
use App\Events\EventOperation;

use App\Service;

class Event
{
    public static function all($filters)
    {
        return EventModel::with(['user', 'services', 'clients'])->filter($filters)->get();
    }

    public static function find($id)
    {
        return EventModel::with(['user', 'services', 'clients'])->findOrFail($id);
    }

    public static function store(array $data)
    {
        $data['all_day'] = isset($data['all_day']) ? $data['all_day'] : 0;

        $comment = isset($data['comment']) ? $data['comment'] : '';

        $event = EventModel::create($data);

        $event->clients()->attach($data['client_id']);

        $event->services()->attach($data['service_id']);

        event(new EventOperation($event, $event, 3, new Service(), $comment));

        return $event;
    }

    public static function update(array $data, $id)
    {
        $comment = isset($data['comment']) ? $data['comment'] : '';

        $event = EventModel::findOrFail($id);

        $oldEvent = $event->replicate();

        $oldServices = $event->services()->get();

        $event->clients()->sync($data['client_id']);

        $event->services()->sync($data['service_id']);

        $newEvent = $event->update($data);

        if($newEvent) $newEvent = EventModel::findOrFail($id);

        event(new EventOperation($newEvent, $oldEvent, 4, $oldServices, $comment));

        return $newEvent;
    }

    public static function destroy($id, $softDeletes)
    {
        $comment = request()->has('comment') ? request()->input('comment') : '';

        $cancelType = request()->has('type') ? request()->input('type') : null;

        $event = EventModel::findOrFail($id);

        if (true === $softDeletes)
        {
            event(new EventOperation($event, $event, 5, new Service(), $comment, $cancelType));

            return $event->delete();
        }
        else
        {
            event(new EventOperation($event, $event, 6, new Service(), $comment));

            return $event->forceDelete();
        }
    }


    public static function checkFreeTime($user, array $services, $start, $current = 0)
    {
        $duration = self::totalServiceDuration($services);

        $end = self::getEnd($start, $duration);

        $events = EventModel::where('user_id', (int) $user)
            ->whereNotIn('id', (array) $current)
            ->where('start', '<', $end)
            ->where('end', '>', $start)
            ->get();

        return $events->count() == 0 ? true : false;
    }


    protected static function totalServiceDuration(array $services)
    {
        $totalDuration = 0;

        $query = Service::find($services);

        foreach ($query as $service)
        {
            $totalDuration += (int) $service->duration_variable;
        }

        return $totalDuration;
    }

    protected static function getEnd($start, $duration = 0)
    {
        $endInSeconds = Carbon::parse($start)->getTimestamp() + (int) $duration;

        return Carbon::createFromTimestamp($endInSeconds)->toDateTimeString();
    }
}