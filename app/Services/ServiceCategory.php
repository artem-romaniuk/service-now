<?php

namespace App\Services;

use App\ServiceCategory as ServiceCategoryModel;

class ServiceCategory
{
    public static function all($filters)
    {
        return ServiceCategoryModel::with(['services'])->filter($filters)->get();
    }

    public static function find($id)
    {
        return ServiceCategoryModel::with(['services'])->findOrFail($id);
    }

    public static function store(array $data)
    {
        return ServiceCategoryModel::create($data);
    }

    public static function update(array $data, $id)
    {
        $service = ServiceCategoryModel::findOrFail($id);

        return $service->update($data);
    }

    public static function destroy($id)
    {
        $service = ServiceCategoryModel::findOrFail($id);

        return $service->delete();
    }
}