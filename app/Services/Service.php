<?php

namespace App\Services;

use App\Service as ServiceModel;

class Service
{
    public static function all($filters)
    {
        return ServiceModel::with(['category', 'events', 'users'])->filter($filters)->get();
    }

    public static function find($id)
    {
        return ServiceModel::with(['category', 'events', 'users'])->findOrFail($id);
    }

    public static function store(array $data)
    {
        $service = ServiceModel::create($data);

        if (isset($data['user_id']))
        {
            $service->users()->attach($data['user_id']);
        }

        return $service;
    }

    public static function update(array $data, $id)
    {
        $service = ServiceModel::findOrFail($id);

        if (isset($data['user_id']))
        {
            $service->users()->attach($data['user_id']);
        }

        return $service->update($data);
    }

    public static function destroy($id)
    {
        $service = ServiceModel::findOrFail($id);

        return $service->delete();
    }
}