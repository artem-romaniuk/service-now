<?php

namespace App\Services;

use App\Client as ClientModel;
use App\Events\ClientOperation;

class Client
{
    public static function all($filters)
    {
        return ClientModel::with(['events', 'stories'])->filter($filters)->get();
    }

    public static function find($id)
    {
        return ClientModel::with(['events', 'stories'])->findOrFail($id);
    }

    public static function store(array $data)
    {
        $client = ClientModel::create($data);

        event(new ClientOperation($client, $client, 1));

        return $client;
    }

    public static function update(array $data, $id)
    {
        $client = ClientModel::findOrFail($id);

        $oldClient = $client->replicate();

        $newClient = $client->update($data);

        if($newClient) $newClient = ClientModel::findOrFail($id);

        event(new ClientOperation($newClient, $oldClient, 2));

        return $newClient;
    }

    public static function destroy($id)
    {
        $client = ClientModel::findOrFail($id);

        return $client->delete();
    }
}