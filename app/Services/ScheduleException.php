<?php

namespace App\Services;

use App\ScheduleException as ScheduleExceptionModel;

class ScheduleException
{
    public static function all($filters)
    {
        return ScheduleExceptionModel::filter($filters)->get();
    }

    public static function find($id)
    {
        return ScheduleExceptionModel::findOrFail($id);
    }

    public static function store(array $data)
    {
        $schedule = ScheduleExceptionModel::create($data);

        return $schedule;
    }

    public static function update(array $data, $id)
    {
        $schedule = ScheduleExceptionModel::findOrFail($id);

        return $schedule->update($data);
    }

    public static function destroy($id)
    {
        $schedule = ScheduleExceptionModel::findOrFail($id);

        return $schedule->delete();
    }
}