<?php

namespace App\Services;

use App\StoryType as StoryTypeModel;

class StoryType
{
    public static function model()
    {
        return new StoryTypeModel();
    }

    public static function all($filters)
    {
        return StoryTypeModel::filter($filters)->get();
    }

    public static function find($id)
    {
        return StoryTypeModel::findOrFail($id);
    }
}