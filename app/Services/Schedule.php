<?php

namespace App\Services;

use App\Schedule as ScheduleModel;
use App\ScheduleException as ScheduleExceptionModel;

class Schedule
{
    public static function all($filters)
    {
        return ScheduleModel::filter($filters)->with(['user'])->get();
    }

    public static function find($id)
    {
        $organization_schedule = ScheduleModel::where('user_id', 0)->get();
        $user_schedule = ScheduleModel::where('user_id', (int) $id)->get();
        $user_schedule_exceptions = ScheduleExceptionModel::where('user_id', (int) $id)->get();

        return [
            $organization_schedule,
            $user_schedule,
            $user_schedule_exceptions
        ];
    }

    public static function store(array $data)
    {
        $schedule = ScheduleModel::create($data);

        return $schedule;
    }

    public static function update(array $data, $id)
    {
        $schedule = ScheduleModel::findOrFail($id);

        return $schedule->update($data);
    }

    public static function destroy($id)
    {
        $schedule = ScheduleModel::findOrFail($id);

        return $schedule->delete();
    }
}