<?php

namespace App\Services;

use App\Story as StoryModel;

class Story
{
    public static function all($filters)
    {
        return StoryModel::with(['client'])->filter($filters)->get();
    }

    public static function find($id)
    {
        return StoryModel::with(['client'])->findOrFail($id);
    }

    public static function store(array $data)
    {

        if((int)$data['type_id'] == 7) {
            if(!isset($data['auto'])) {
                $data['details'] = json_encode([
                    '26' => ['o' => '','n' => 'Клиенту было отправлено ручное СМС сообщение с текстом']
                ]);
            } else {
                $data['details'] = json_encode([
                    '27' => ['o' => '','n' => 'Клиенту было отправлено автоматическое СМС сообщение с текстом']
                ]);
            }
        }
        $story = StoryModel::create($data);

        return $story;
    }

    public static function update(array $data, $id)
    {
        $story = StoryModel::findOrFail($id);

        return $story->update($data);
    }

    public static function destroy($id)
    {
        $story = StoryModel::findOrFail($id);

        return $story->delete();
    }
}