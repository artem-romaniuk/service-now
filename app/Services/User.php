<?php

namespace App\Services;

use App\User as UserModel;
use Illuminate\Support\Facades\Auth;

class User
{
    public static function all($filters)
    {
        if (\request()->has('date')) {

            $working_users = collect();
            $user_schedules = collect();
            $user_schedules_exception = collect();


            $query = UserModel::where('user_status_id', '=', '1');

            if (Auth::check() && Auth::user()->user_role_id != 1 && Auth::user()->user_role_id != 2) {
                $query->where('id', '=', Auth::user()->id);
            }

            $users = $query->get();

            $schedules = \App\Schedule::with(['user' => function($query) {
                $query->where('user_status_id', '=', '1');
            }])
                ->where(function ($query) {
                    $query->whereNotNull('end')
                        ->where('start', '<=', request()->input('date'))
                        ->where('end', '=>', request()->input('date'));
                })
                ->orWhere('start', '=', strtotime(request()->input('date')))
                ->has('user')
                ->get();

            $schedules_exception = \App\ScheduleException::with(['user'])
                ->whereNotIn('type', ['1', '2'])
                ->where(function ($query) {
                    $query->whereNotNull('end_date')
                        ->where('end_date', '>=', request()->input('date'))
                        ->where('start_date', '<=', request()->input('date'));
                })
                ->orWhere('start_date', '=', request()->input('date'))
                ->has('user')
                ->get();


            foreach ($users as $user) {
                $working_users->put($user->id, $user);
            }

            foreach ($schedules as $schedule) {
                if ($schedule->user) {
                    $user_schedules->put($schedule->user->id, $schedule->user);
                }
            }

            foreach ($schedules_exception as $schedule_exception) {
                if ($schedule_exception->user) {
                    $user_schedules_exception->put($schedule_exception->user->id, $schedule_exception->user);
                }
            }

            $working = $working_users->diffKeys($user_schedules->toArray())
                ->diffKeys($user_schedules_exception->toArray());

            $free = $user_schedules->diffKeys($user_schedules_exception->toArray());

            $result_collect = collect();

            $result = $free->union($working);

            foreach ($result as $item) {
                $result_collect->push($item);
            }

            return $result_collect;
        }

        $query = UserModel::with(['role', 'status', 'services', 'schedules', 'exceptions'])->filter($filters);

        if (Auth::check() && Auth::user()->user_role_id != 1 && Auth::user()->user_role_id != 2) {
            $query->where('id', '=', Auth::user()->id);
        }

        return $query->get();
    }

    public static function getDirectors()
    {
        return UserModel::with(['role', 'status', 'services'])->director()->get();
    }

    public static function getAdministrators()
    {
        return UserModel::with(['role', 'status', 'services'])->administrator()->get();
    }

    public static function getWorkers()
    {
        return UserModel::with(['role', 'status', 'services'])->worker()->get();
    }

    public static function find($id)
    {
        return UserModel::with(['role', 'status', 'services'])->findOrFail($id);
    }

    public static function store(array $data)
    {
        $user = UserModel::create($data);

        $user->services()->attach($data['services']);

        return $user;
    }

    public static function update(array $data, $id)
    {
        $user = UserModel::findOrFail($id);

        $user->services()->sync($data['services']);

        return $user->update($data);
    }

    public static function destroy($id)
    {
        $user = UserModel::findOrFail($id);

        return $user->delete();
    }
}