<?php

namespace App\Http\Controllers\Control;

use App\StoryType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StoryType  $storyType
     * @return \Illuminate\Http\Response
     */
    public function show(StoryType $storyType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StoryType  $storyType
     * @return \Illuminate\Http\Response
     */
    public function edit(StoryType $storyType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StoryType  $storyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoryType $storyType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoryType  $storyType
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoryType $storyType)
    {
        //
    }
}
