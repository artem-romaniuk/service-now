<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Services\Calendar;


class HomeController extends Controller
{
    public function index()
    {
        return view('control.home.index');
    }
}
