<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Event;
use App\Filters\EventFilter;
use Illuminate\Http\Response;

class EventController extends Controller
{
    public function index(EventFilter $filters)
    {
        return Event::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $temp_id = $request->has('temp_id') ? $request->input('temp_id') : 0;

        if (!Event::checkFreeTime($request->input('user_id'), $request->input('service_id'), $request->input('start')))
        {
            return response()->json([
                'status' => 3,
                'message' => 'Time taken',
                'temp_id' => $temp_id
            ]);
        }

        $event = Event::store($request->all());

        return $event
            ? response()->json([
                'status' => 0,
                'message' => 'Event created',
                'id' => $event->id,
                'temp_id' => $temp_id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error event created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return Event::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        if (!Event::checkFreeTime($request->input('user_id'), $request->input('service_id'), $request->input('start'), $id))
        {
            return response()->json([
                'status' => 3,
                'message' => 'Time taken',
            ]);
        }

        return Event::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        $softDeletes = (request()->has('action') && request()->input('action') == 'cancel') ? true : false;

        return Event::destroy($id, $softDeletes)
            ? response()->json([
                'status' => 0,
                'message' => $softDeletes  ? 'Data canceled' : 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }
}
