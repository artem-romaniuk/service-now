<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ServiceCategory;
use App\Filters\ServiceCategoryFilter;

class ServiceCategoryController extends Controller
{
    public function index(ServiceCategoryFilter $filters)
    {
        return ServiceCategory::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $category = ServiceCategory::store($request->all());

        return $category
            ? response()->json([
                'status' => 0,
                'message' => 'Category created',
                'id' => $category->id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error category created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return ServiceCategory::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return ServiceCategory::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return ServiceCategory::destroy($id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }
}
