<?php

namespace App\Http\Controllers\Api;

use App\Filters\StoryTypeFilter;
use App\Services\StoryType;
use App\StoryDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoryTypeController extends Controller
{
    public function index(StoryTypeFilter $filters)
    {
        $types = StoryType::all($filters);

        $details = StoryDetails::all()->keyBy('id');

        return response()->json([
            'types' => $types,
            'details' => $details,
        ]);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        return abort(404);
    }

    public function show($id)
    {
        return StoryType::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return abort(404);
    }

    public function destroy($id)
    {
        return abort(404);
    }
}
