<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;
use SoapClient;
use App\Services\StoryType;
use App\Services\Story;
use App\Http\Controllers\Controller;


class SmsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $validation = Validator::make($request->input(), [
        'message' => 'required|max:140'
        ]);
        if($validation->fails()){
          return response()->json([
              'status' => 1,
              'message' => 'Error input',
              'id' => null
            ]);

        } else{ //TODO: check sms lenght, split if too long
          $sms_result = $this->send('+380936976122',$request['message']);//TODO: format phone number from request

           if(isset($sms_result['error'])) {

             return response()->json([
               'status' => 1,
               'error' => $sms_result['error'],
               'message' => 'Error sms sender: '.$sms_result['error'],
               'id' => null
             ]);
           } else {
             //dd($sms_result['data']);

             //client story
             $type = 7; /*sms*/

             if(count($request['client_ids']) == 1){//One client sms

               $story_id = 0;
               if ($type && count($request['client_ids']) > 0) {
                   $story = Story::store([
                     'comment' => $request['message'],
                     'client_id' => $request['client_ids'][0],
                     'type_id' => $type,
                     'date' => time(),
                     'auto' => true, /*автоматическое*/
                   ]);
                   $story_id = $story->id;
               }

               return response()->json([
                 'status' => 0,
                 'message' => $request['message'],
                 'id' => $story_id
               ]);

             } else {//sms newsletter
               $story_id = 0;
               if ($type && count($request['client_ids']) > 0) {
                 foreach ($request['client_ids'] as $client) {
                   $story = Story::store([
                     'comment' => $request['message'],
                     'client_id' => $client,
                     'type_id' => $type->id,
                     'date' => time(),
                     'auto' => true, /*автоматическое*/
                   ]);
                   $story_id = $story->id;
                 }
               }

               return response()->json([
                 'status' => 0,
                 'message' => $request['message'],
                 'id' => $sms_result
               ]);
             }
             //end client story
           }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** Send function
     * $to string or array
     *
     **/
    public function send($to,$message) {
      $username = env("SMS_USER", "");
      $password = env("SMS_PASSWORD", "");
      $sender = env("SMS_SENDER", "");
      $admin = env("SMS_ADMIN", "");
      $alert = env("SMS_ALERT_LOW_BALANCE", false);

      $result = array();

      $client = new SoapClient ('http://turbosms.in.ua/api/wsdl.html');

      $credentials = Array (
        'login' => $username,
        'password' => $password
      );

      $auth = $client->Auth($credentials);

      if($auth->AuthResult == 'Неверный логин или пароль'){

        $result['error'] = 'SMS Gate login error';
        return $result;
      }

      $balance = $client->GetCreditBalance();
      if($balance->GetCreditBalanceResult <= 2){
        $result['error'] = 'Low Ballance';
        if($alert){
          $admin_sms = Array (
            'sender' => $sender,
            'destination' => $admin,
            'text' => 'Turbo SMS low balance'
          );
          $client->SendSMS($admin_sms);
        }
        $result['error'] = 'Low balance';
        return $result;
      }
      /*$sms = ['MessageId' => '88d1fc64-5c23-916e-5d43-ac5284ffb8c7'];
      $status[] = $client->GetMessageStatus($sms);
      $sms = ['MessageId' => '88d33157-5c23-916e-5d43-8c9d2b19fd17'];
      $status[] = $client->GetMessageStatus($sms);
      dd($status);*/

      if(is_array($to)){
        $numbers = implode(",", $to);
      } elseif($to){
        $numbers = $to;
      } else {
        $numbers = false;
      }

      if($numbers){
        $sms = Array (
          'sender' => $sender,
          'destination' => $numbers,
          'text' => $message
        );

        $result['data'] = $client->SendSMS($sms);
        return $result;

      } else {
        $result['error'] = 'Phone number format error';
        return $result;
      }
    }
}
