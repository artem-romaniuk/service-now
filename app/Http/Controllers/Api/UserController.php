<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\User;
use App\Filters\UserFilter;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(UserFilter $filters)
    {
        return User::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ])->validate();

        $user = User::store($request->all());

        return $user
            ? response()->json([
                'status' => 0,
                'message' => 'User created',
                'id' => $user->id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error user created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return User::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return User::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return User::destroy($id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }
}
