<?php

namespace App\Http\Controllers\Api;

use App\Filters\ScheduleFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Schedule;

class ScheduleController extends Controller
{
    public function index(ScheduleFilter $filters)
    {
        return Schedule::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $schedule_temp_id = $request->has('schedule_temp_id') ? $request->input('schedule_temp_id') : 0;

        $schedule = Schedule::store($request->all());

        return $schedule
            ? response()->json([
                'status' => 0,
                'message' => 'Schedule created',
                'id' => $schedule->id,
                'schedule_temp_id' => $schedule_temp_id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error schedule created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return Schedule::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return Schedule::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return Schedule::destroy($id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }
}
