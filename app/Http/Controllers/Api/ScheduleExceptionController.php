<?php

namespace App\Http\Controllers\Api;

use App\Filters\ScheduleExceptionFilter;
use App\Services\ScheduleException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleExceptionController extends Controller
{
    public function index(ScheduleExceptionFilter $filters)
    {
        return ScheduleException::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $exception_temp_id = $request->has('exception_temp_id') ? $request->input('exception_temp_id') : 0;

        $schedule = ScheduleException::store($request->all());

        return $schedule
            ? response()->json([
                'status' => 0,
                'message' => 'Schedule exception created',
                'id' => $schedule->id,
                'schedule_temp_id' => $exception_temp_id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error schedule exception created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return ScheduleException::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return ScheduleException::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return ScheduleException::destroy($id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }
}
