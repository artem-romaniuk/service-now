<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Client;
use App\Filters\ClientFilter;

class ClientController extends Controller
{
    public function index(ClientFilter $filters)
    {
        return Client::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $client = Client::store($request->all());

        return $client
            ? response()->json([
                'status' => 0,
                'message' => 'Client created',
                'id' => $client->id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error client created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return Client::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return Client::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return Client::destroy($id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }

}
