<?php

namespace App\Http\Controllers\Api;

use App\Filters\StoryFilter;
use App\Services\Story;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoryController extends Controller
{
    public function index(StoryFilter $filters)
    {
        return Story::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $story = Story::store($request->all());

        return $story
            ? response()->json([
                'status' => 0,
                'message' => 'Story created',
                'id' => $story->id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error story created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return Story::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return Story::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return abort(404);
    }
}
