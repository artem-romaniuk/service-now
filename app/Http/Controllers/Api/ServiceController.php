<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Service;
use App\Filters\ServiceFilter;

class ServiceController extends Controller
{
    public function index(ServiceFilter $filters)
    {
        return Service::all($filters);
    }

    public function create()
    {
        return abort(404);
    }

    public function store(Request $request)
    {
        $service = Service::store($request->all());

        return $service
            ? response()->json([
                'status' => 0,
                'message' => 'Service created',
                'id' => $service->id
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Error service created',
                'id' => null
            ]);
    }

    public function show($id)
    {
        return Service::find($id);
    }

    public function edit($id)
    {
        return abort(404);
    }

    public function update(Request $request, $id)
    {
        return Service::update($request->all(), $id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data updated'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Updated error'
            ]);
    }

    public function destroy($id)
    {
        return Service::destroy($id)
            ? response()->json([
                'status' => 0,
                'message' => 'Data deleted'
            ])
            : response()->json([
                'status' => 1,
                'message' => 'Deleted error'
            ]);
    }
}
