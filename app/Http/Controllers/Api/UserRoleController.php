<?php

namespace App\Http\Controllers\Api;

use App\Filters\UserRoleFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserRole;

class UserRoleController extends Controller
{
    public function index(UserRoleFilter $filters)
    {
        return UserRole::all($filters);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
