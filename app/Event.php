<?php

namespace App;

use App\Filters\QueryFilterCreator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'all_day',
        'start',
        'end',
        'user_id'
    ];

    protected $dates = [
        'start',
        'end',
        'deleted_at',
    ];

    protected $casts = [
        'all_day' => 'boolean',
    ];


    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */

    /* Relations */
    public function services()
    {
        return $this->belongsToMany('App\Service', 'event_service');
    }

    public function clients()
    {
        return $this->belongsToMany('App\Client', 'event_client');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    /* End Relations */
}
