<?php

namespace App;

use App\Filters\QueryFilterCreator;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'description',
        'address',
        'birthday',
        'phone',
        'phone_additional',
        'email',
        'email_additional',
        'social_links'
    ];

    protected $casts = [
        'social_links' => 'array'
    ];

    protected $dates = [
        'birthday'
    ];


    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */


    /* Relations */
    public function events()
    {
        return $this->belongsToMany('App\Event', 'event_client');
    }

    public function stories()
    {
        return $this->hasMany('App\Story', 'client_id', 'id');
    }
    /* End Relations */
}
