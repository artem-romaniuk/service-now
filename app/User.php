<?php

namespace App;

use App\Filters\QueryFilterCreator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PasswordResetNotification;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'phone_additional',
        'address',
        'position',
        'image',
        'color',
        'social_links',
        'user_role_id',
        'user_status_id',
        'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'api_token'
    ];

    protected $casts = [
        'social_links' => 'array'
    ];


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    /* Relations */
    public function role()
    {
        return $this->hasOne('App\UserRole', 'id', 'user_role_id');
    }

    public function status()
    {
        return $this->hasOne('App\UserStatus', 'id', 'user_status_id');
    }

    public function schedules()
    {
        return $this->hasMany('App\Schedule', 'user_id', 'id');
    }

    public function exceptions()
    {
        return $this->hasMany('App\ScheduleException', 'user_id', 'id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'service_user');
    }
    /* End Relations */

    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }

    public function scopeDirector($query)
    {
        return $query->where('user_role_id', 1);
    }

    public function scopeAdministrator($query)
    {
        return $query->where('user_role_id', 2);
    }

    public function scopeWorker($query)
    {
        return $query->where('user_role_id', 3);
    }
    /* End Scopes */
}
