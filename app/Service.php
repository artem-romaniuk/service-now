<?php

namespace App;

use App\Filters\QueryFilterCreator;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name',
        'description',
        'duration_variable',
        'duration_text',
        'space',
        'service_category_id',
    ];


    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */

    /* Relations */
    public function category()
    {
        return $this->belongsTo('App\ServiceCategory', 'service_category_id', 'id');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'event_service');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'service_user');
    }
    /* End Relations */
}
