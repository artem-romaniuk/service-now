<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterUserNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->to($this->user->email, $this->user->name)
            ->subject(__('Регистрация в системе') . ' ' . config('app.name'))
            ->view('emails.register_user_notification')
            ->with([
                'name' => $this->user->name,
                'email' => $this->user->email,
                'password' => $this->user->password,
                'url' => config('app.url'),
            ]);
    }
}
