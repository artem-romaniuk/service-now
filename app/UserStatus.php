<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];


    /* Relations */
    public function users()
    {
        return $this->hasMany('App\User', 'user_status_id', 'id');
    }
    /* End Relations */
}
