<?php

namespace App;

use App\Filters\QueryFilterCreator;
use Illuminate\Database\Eloquent\Model;

class ScheduleException extends Model
{
    protected $fillable = [
        'name',
        'type',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
        'user_id',
    ];


    /* Relations */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    /* End Relations */

    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */
}
