<?php

namespace App;

use App\Filters\QueryFilterCreator;
use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps = false;


    /* Scopes */
    public function scopeFilter($query, QueryFilterCreator $filter)
    {
        return $filter->apply($query);
    }
    /* End Scopes */

    /* Relations */
    public function services()
    {
        return $this->hasMany('App\Service', 'service_category_id', 'id');
    }
    /* End Relations */
}
