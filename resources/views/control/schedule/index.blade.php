@extends('layouts.control')

@section('title', __('Schedules'))

@section('content')

    <div id="app" v-cloak>
        <div class="header">
            <span class="title" style="line-height: 40px">Настройки</span>
        </div>
        <!----------------------------------------------tabs weeks----------------------------------------------->
        <div class="tabs-nav">
            <a href="#" :class="tabs == 0 ? 'active' : ''" @click="setTab(0)">Расписание предприятия</a>
            <a href="#" :class="tabs == 1 ? 'active' : ''" @click="setTab(1)">Другой раздел</a>
        </div>

        <!------------------------------------------SETTINGS SCHEDULE COMPANY------------------------------------------>
        <!------------------------------------------------------------------------------------------------------------->
        <div class="schedule-wrapper" v-if="tabs == 0">
            <span class="caption">Расписание предприятия</span>
            <span class="hint">Общее расписание предприятия</span>
            <div class="schedule-container">
                <div class="schedule">
                    <div class="title-input">
                        <input type="text" v-model="schedule.form.name" />
                        <span class="fa fa-pen toggle-edit-name-btn" onClick="$(this).parent('.title-input').toggleClass('edit')"></span>
                    </div>
                    <div class="header-btns-panel">
                        <span class="btn-cancel" @click="getWeekSchedule()">Отмена</span>
                        <span class="add-schedule-btn" @click="schedule.updateSchedule()">Изменить <i class="fa fa-pen"></i></span>
                    </div>
                    <div class="schedule-days-box">
                        <div class="schedule-single-day" v-for="(day, index) in schedule.form.days">
                            <span class="title">@{{ schedule.daysOfWeek[index] }}</span>
                            <div class="day-block" :class="day.status ? '' : 'disabled'" @click.self="day.status = !day.status">
                                <input type="time" v-model="day.start" />
                                <span>-</span>
                                <input type="time" v-model="day.end" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------------------------------------------------------------------------------------------->
        <!--------------------------------END----------SETTINGS SCHEDULE COMPANY--------------------------------------->

    </div>

@endsection



@push('css')
    <link rel="stylesheet" href="{{ asset('css/settings.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('components/settings/build.js') }}"></script>

    <script>
        $(document).ready(function(){
            console.log('jquery test');
        });
    </script>
@endpush