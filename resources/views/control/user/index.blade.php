@extends('layouts.control')

@section('title', __('Сотрудники'))

@section('content')
    <div id="app" v-cloak>
        <!-------------------modals------------------->
        <div class="overlay" :class="modalDelete.open">
            <div id="delete_user_modal" :class="modalDelete.open">
                <span class="close-modal fa fa-times" @click="modalDelete.close()"></span>
                <span>Внимание! К кому привязать клиентов?</span>
                <div class="delete-users-container">
                    <select2-multiple-control ref="selectDelete" :settings="{multiple: false}" v-model="modalDelete.user_binding_id" :options="allUsersFromSelect"></select2-multiple-control>
                    <span class="btn-delete-user" v-on:click="modalDelete.delete">Привязать</span>
                </div>
            </div>
        </div>
        <!-------------------------------------------->
        <div class="header">
            <span class="title">Сотрудники</span>
            <a href="/users/user/" class="btn-def add-model-btn">Добавить сотрудника</a>
        </div>
        <filter-roles v-bind:roles="roles"></filter-roles>
        <filter-special v-bind:specials="specials"></filter-special>
        {{--<filter-status v-bind:status="usersStatus"></filter-status>--}}
        <!--поиск-->
        <div class="search-box">
            <i class="fa fa-search icon icon-search"></i>
            <i class="fa fa-times icon icon-close" v-on:click="searchClear()"></i>
            <input type="text" v-model="searchText" @keyup="filterSearch">
            <span class="text-search">Поиск</span>
        </div>
        <div class="filter-search-box">
            <i class="fa fa-search filter-icon filter-icon-search"></i>
            <i class="fa fa-times filter-icon filter-icon-close" v-on:click="searchServicesClear()"></i>
            <input type="text" id="services_search_input" v-model="searchTextServices" @keyup="filterServices" list="services_list">
            <datalist id="services_list">
                <option v-for="item in searchServicesDataList" v-bind:value="item">@{{ item }}</option>
            </datalist>
            <span class="filter-text-search">Фильтр по услугам</span>
        </div>
        <users v-bind:users="users"></users>
        <div class="pagination-box">
            <vue-ads-pagination v-if="showPaginateAfterLoad" :total-items="countUsers" :page="page" :items-per-page="50" v-on:page-change="pageChange">
                <template slot-scope="props">
                    <div class="pr-2 leading-loose">
                    <!--@{{ start }} - @{{ end }} из @{{ countUsers }}-->
                    </div>
                </template>
            </vue-ads-pagination>
        </div>
    </div>
@endsection



@push('css')
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('components/users/build.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            console.log('jquery test');

            /*Открыть поле поиска*/
            $('.icon-search').on('click',function (e) {
                $(this).parent('.search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).css('display','none');
                $(this).siblings('.icon-close').css('display','inline-block');
            });
            /*Открыть поле поиска*/
            $('.text-search').on('click',function (e) {
                $(this).parent('.search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).siblings('.icon-search').css('display','none');
                $(this).siblings('.icon-close').css('display','inline-block');
            });
            /*Закрыть поле поиска*/
            $('.icon-close').on('click',function (e) {
                $(this).parent('.search-box').removeClass('open');
                $(this).css('display','none');
                $(this).siblings('.icon-search').css('display','inline-block');
            });

            /*filter*/
            /*Открыть поле поиска*/
            $('.filter-icon-search').on('click',function (e) {
                $(this).parent('.filter-search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).css('display','none');
                $(this).siblings('.filter-icon-close').css('display','inline-block');
            });
            /*Открыть поле поиска*/
            $('.filter-text-search').on('click',function (e) {
                $(this).parent('.filter-search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).siblings('.filter-icon-search').css('display','none');
                $(this).siblings('.filter-icon-close').css('display','inline-block');
            });
            /*Закрыть поле поиска*/
            $('.filter-icon-close').on('click',function (e) {
                $(this).parent('.filter-search-box').removeClass('open');
                $(this).css('display','none');
                $(this).siblings('.filter-icon-search').css('display','inline-block');
            });
        });

        /*Клик вне элемента*/
        $(document).mouseup(function (e) {
            var container = $(".menu");
            if (container.has(e.target).length === 0){
                container.children('ul').removeClass('open');
            }

            var container = $("#filter_modal");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul').removeClass('open');
            }

            var container = $("#filter_modal_special");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul-special').removeClass('open');
            }

            var container = $("#filter_modal_status");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul-status').removeClass('open');
            }

        });
    </script>
@endpush