@extends('layouts.control')

@section('title', __('Сотрудник'))

@section('content')
    <div id="app" v-cloak>
        <!-------------------modals------------------->
        <div class="overlay" :class="modalDelete.open">
            <div id="delete_user_modal" :class="modalDelete.open">
                <span class="close-modal fa fa-times" @click="modalDelete.close()"></span>
                <span>Внимание! К кому привязать клиентов?</span>
                <div class="delete-users-container">
                    <select2-multiple-control ref="selectDelete" :settings="{multiple: false}" v-model="modalDelete.user_binding_id" :options="allUsersFromSelect"></select2-multiple-control>
                    <span class="btn-delete-user" v-on:click="modalDelete.delete">Привязать</span>
                </div>
            </div>
        </div>
        <!-------------------------------------------->
        <div class="header">
            <span class="title" style="line-height: 40px">Сотрудник</span>
            <span v-if="userForm.createUpdate" class="delete-btn" @click="deleteUser">Удалить <i class="fa fa-trash"></i></span>
        </div>
        <div class="wrapper">
            <div class="form-wrapper">
                <div class="form-fon">
                    <span class="title">Общие данные</span>
                    <div class="form-header">
                        <div class="user-pic" :style="'background-color:' + userForm.user.color">
                            <span>@{{ userForm.user.name | initials }}</span>
                        </div>
                        <div class="socials-box">
                            <div class="soc-elem facebook">
                                <input type="text" v-model="userForm.user.social_links_fb" :class="userForm.errorsClass.social_links_fb" placeholder="https://www.facebook.com">
                            </div>
                            <div class="soc-elem instagram">
                                <input type="text" v-model="userForm.user.social_links_insta" :class="userForm.errorsClass.social_links_insta" placeholder="https://www.instagram.com">
                            </div>
                        </div>
                    </div>
                    <div class="fio-container">
                        <span class="text">ФИО</span>
                        <input type="text" placeholder="Введите ФИО" v-model="userForm.user.name" :class="userForm.errorsClass.name">
                    </div>
                    <div class="role-container">
                        <span class="text">Роль</span>
                        <div class="role-box" :class="userForm.errorsClass.role">
                            <select2-multiple-control ref="selectRoles" :settings="{multiple: false, placeholder: 'Выберите роль'}" v-model="userForm.user.role" :options="allRolesFromSelect"></select2-multiple-control>
                        </div>
                    </div>
                    <div class="color-container">
                        <span class="text">Цвет</span>
                        <div class="color-component">
                            <swatches v-model="userForm.user.color" colors="text-advanced" popover-to="left"></swatches>
                            {{--<swatches v-model="userForm.user.color" :colors="colors" row-length="5" show-fallback popover-to="left"></swatches>--}}
                        </div>
                    </div>
                    <div class="services-container">
                        <span class="text">Услуги</span>
                        <div class="services-component" :class="userForm.errorsClass.services">
                            <select2-multiple-control ref="selectServices" :settings="{closeOnSelect:false, placeholder: 'Выберите услуги'}" v-model="userForm.user.services" :options="allServicesFromSelect"></select2-multiple-control>
                        </div>
                    </div>
                    <div class="group">
                        <label>Тел</label>
                        <div class="wrapper-input">
                            <!--<input type="text"  v-model="userForm.user.phone" :class="userForm.errorsClass.phone" />-->
                            <masked-input mask="\+\38 (111) 111-11-11" placeholder="Введите телефон" class="watch-mask" ref="inputPhone" v-model="userForm.user.maskPhone" :class="userForm.errorsClass.phone"></masked-input>
                            <span class="fa fa-phone icon"></span>
                        </div>
                    </div>
                    <div id="mx_wrapper_phone_additional" class="wrapper-phones">
                        <!-------------------------------------------------------->
                        <div v-for="(phone, index) in userForm.user.phone_additional" class="group">
                            <label>Тел @{{ index + 2 }}</label>
                            <div class="wrapper-input" :class="'phone-input-' + userForm.phoneCounter">
                                <masked-input mask="\+\38 (111) 111-11-11" placeholder="Введите телефон" v-model="userForm.user.phone_additional[index]"></masked-input>
                                <!--<input type="text" placeholder="Введите телефон" v-model="userForm.user.phone_additional[index]"></input>-->
                                <span class="fa fa-phone icon"></span>
                            </div>
                        </div>
                        <!-------------------------------------------------------->
                        <div class="group" id="phone_insert">
                        <!--<label id="phone_additional_label_title">Тел @{{userForm.phoneCounter}}</label>-->
                            <span class="fa fa-plus btn-plus" v-on:click="userForm.addPhone"><i>Добавить телефон</i></span>
                        </div>
                    </div>
                    <div class="password-container" v-if="userForm.createUpdate == 0">
                        <span class="text" style="width: 100px">Пароль</span>
                        <input type="password" style="width: 315px" v-model="userForm.user.password" :class="userForm.errorsClass.password">
                    </div>
                    <div class="password-container" v-if="userForm.createUpdate == 0">
                        <span class="text" style="width: 100px">Подтвердите пароль</span>
                        <input type="password" style="width: 315px" v-model="userForm.user.password_confirmation" :class="userForm.errorsClass.password_confirmation">
                    </div>
                    <div class="group"><label>E-mail</label>
                        <div class="wrapper-input">
                            <input type="text" v-model="userForm.user.email" :class="userForm.errorsClass.email" placeholder="Введите e-mail">
                            <span class="fa fa-at icon"></span>
                        </div>
                    </div>
                    <div id="mx_wrapper_email_additional" class="wrapper-emails" v-if="false">
                        <!-------------------------------------------------------->
                        <div v-for="(email, index) in userForm.user.email_additional" class="group">
                            <label>E-mail @{{ index + 2 }}</label>
                            <div class="wrapper-input">
                                <input type="text" v-model="userForm.user.email_additional[index]">
                                <span class="fa fa-at icon"></span>
                            </div>
                        </div>
                        <!-------------------------------------------------------->
                        <div class="group" id="email_insert">
                            <label id="email_additional_label_title">E-mail @{{userForm.emailCounter}}</label>
                            <span class="fa fa-plus btn-plus" v-on:click="userForm.addEmail"></span>
                        </div>
                    </div>
                    <div class="btns-panel">
                        {{--<span class="userForm-btn-cancel">Отмена</span>--}}
                        <span class="userForm-btn-save" @click="userForm.save">Сохранить</span>
                    </div>
                </div>
            </div>
            <!----------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------->
            <!----------------------------------------------------------------------------------------------------------->
            <div class="schedule-wrapper">
                <span class="caption">Расписание</span>
                <span class="hint" v-if="userForm.createUpdate">Добавьте уникальное расписание для сотрудника (по умолчанию форма содержит общее расписание предприятия)</span>
                <span class="hint" v-if="userForm.createUpdate == 0">Что бы добавить расписание, сначала создайте сотрудника!</span>
                <div class="schedule-container" v-if="userForm.createUpdate">
                    <!--------------------------Форма добавления недели------------------------------------------------------>
                    <div class="schedule">
                        <div class="title-input">
                            <input type="text" v-model="schedule.week.form.name" />
                            <span class="fa fa-pen toggle-edit-name-btn" onClick="$(this).parent('.title-input').toggleClass('edit')"></span>
                        </div>
                        <div class="header-btns-panel">
                            <!--<span class="btn-remove">Удалить <i class="fa fa-trash"></i></span>-->
                            <span class="btn-cancel" @click="schedule.week.resetFormWeek()">Отмена</span>
                            <span class="add-schedule-btn" @click="schedule.week.addWeek">Добавить <i class="fa fa-plus"></i></span>
                        </div>
                        <div class="schedule-days-box">
                            <div class="schedule-single-day" v-for="(day, index) in schedule.week.form.days">
                                <span class="title">@{{ schedule.week.daysOfWeek[index] }}</span>
                                <div class="day-block" :title="day.status ? 'Сделать выходным' : 'Сделать рабочим'" :class="day.status ? '' : 'disabled'" @click.self="day.status = !day.status">
                                    <input type="time" v-model="day.start" />
                                    <span>-</span>
                                    <input type="time" v-model="day.end" />
                                </div>
                            </div>
                        </div>
                        <div class="range-status-box">
                            <div class="range-box">
                                <span class="start">От</span>
                                <datepicker :monday-first="true" placeholder="Выбрать" :disabled-dates="datePickerConfig.disabledDates" v-model="schedule.week.form.range.start" :language="datePickerConfig.langDatapicker" :format="customFormatter"></datepicker>
                                <span class="end">До</span>
                                <datepicker :monday-first="true" placeholder="Выбрать" :disabled-dates="datePickerConfig.disabledDates" v-model="schedule.week.form.range.end" :language="datePickerConfig.langDatapicker" :format="customFormatter"></datepicker>
                            </div>
                        </div>
                    </div>
                    <!---------------------------------------------End form week--------------------------------------------->
                    <!----------------------------------------------tabs weeks----------------------------------------------->
                    <div class="tabs-nav">
                        <a href="#" :class="schedule.week.tab == 0 ? 'active' : ''" @click="schedule.week.setTab(0)">Актуальные расписание</a>
                        <a href="#" :class="schedule.week.tab == 1 ? 'active' : ''" @click="schedule.week.setTab(1)">Прошедшие расписания</a>
                    </div>
                    <!------------------------------------------Актуальные расписание---------------------------------------->
                    <div v-if="schedule.week.tab == 0">
                        <div class="schedule" :class="week.show ? 'open' : ''" v-for="(week, index) in schedule.week.weeks" v-if="schedule.week.checkCurrentDate(week.range.end)">
                            <div class="title-input">
                                <input type="text" v-model="week.name" />
                                <span class="fa fa-pen toggle-edit-name-btn" v-if="week.show" onClick="$(this).parent('.title-input').toggleClass('edit')"></span>
                            </div>
                            <div class="desc" v-if="week.show == false">
                                Действует с @{{ week.range.start | moment("DD.MM.YYYY") }} по @{{ week.range.end | moment("DD.MM.YYYY") }}
                            </div>
                            <div class="header-btns-panel">
                                <span class="btn-remove" @click="schedule.week.deleteWeek(week.id)">Удалить <i class="fa fa-trash"></i></span>
                                <span class="btn-cancel" v-if="week.show" @click="week.show = false;schedule.week.cancelEditWeek()" onClick="$(this).parent('.header-btns-panel').siblings('.title-input').removeClass('edit')">Отмена</span>
                                <span class="add-schedule-btn" v-if="week.show == false" @click="schedule.week.editWeek(week.id);week.show = true">Редактировать <i class="fa fa-pen"></i></span>
                                <span class="add-schedule-btn" v-if="week.show" @click="schedule.week.updateWeek(week)">Сохранить <i class="fa fa-check"></i></span>
                            </div>
                            <div class="schedule-days-box" v-if="week.show">
                                <div class="schedule-single-day" v-for="(day, index) in week.days">
                                    <span class="title">@{{ schedule.week.daysOfWeek[index] }}</span>
                                    <div class="day-block" :class="day.status ? '' : 'disabled'" @click.self="day.status = !day.status">
                                        <input type="time" v-model="day.start" />
                                        <span>-</span>
                                        <input type="time" v-model="day.end" />
                                    </div>
                                </div>
                            </div>
                            <div class="range-status-box" v-if="week.show">
                                <div class="range-box">
                                    <span class="start">От</span>
                                    <datepicker :monday-first="true" v-model="week.range.start" :disabled-dates="datePickerConfig.disabledPrevDates" :language="datePickerConfig.langDatapicker" :format="customFormatter"></datepicker>
                                    <span class="end">До</span>
                                    <datepicker :monday-first="true" v-model="week.range.end" :disabled-dates="datePickerConfig.disabledPrevDates" :language="datePickerConfig.langDatapicker" :format="customFormatter"></datepicker>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-------------------------------------------Прошедшие расписания---------------------------------------->
                    <div v-if="schedule.week.tab == 1">
                        <div class="schedule" v-for="(week, index) in schedule.week.weeks" v-if="!schedule.week.checkCurrentDate(week.range.end)">
                            <div class="title-input">
                                <input type="text" v-model="week.name" />
                            </div>
                            <div class="desc" v-if="week.show == false">
                                Действует с @{{ week.range.start | moment("DD.MM.YYYY") }} по @{{ week.range.end | moment("DD.MM.YYYY") }}
                            </div>
                            <div class="header-btns-panel">
                                <span class="add-schedule-btn" v-if="week.show == false" @click="week.show = true">Просмотр <i class="fa fa-eye"></i></span>
                                <span class="add-schedule-btn" v-if="week.show" @click="week.show = false">Скрыть</span>
                            </div>
                            <div class="schedule-days-box" v-if="week.show">
                                <div class="schedule-single-day" v-for="(day, index) in week.days">
                                    <span class="title">@{{ schedule.week.daysOfWeek[index] }}</span>
                                    <div class="day-block" :class="day.status ? '' : 'disabled'">
                                        <input type="time" v-model="day.start" readonly />
                                        <span>-</span>
                                        <input type="time" v-model="day.end" readonly />
                                    </div>
                                </div>
                            </div>
                            <div class="range-status-box" v-if="week.show">
                                <div class="range-box">
                                    <span class="start">От</span>
                                    <div class="input-date">@{{ week.range.start | moment("DD.MM.YYYY") }}</div>
                                    <span class="end">До</span>
                                    <div class="input-date">@{{ week.range.end | moment("DD.MM.YYYY") }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!------------------------------------------------------------------------------------------------------->
                </div>

                <!------------------------------------------Исключение/статус---------------------------------------------->
                <!--------------------------------------------------------------------------------------------------------->
                <!--------------------------------------------------------------------------------------------------------->
                <div class="schedule-container" v-if="userForm.createUpdate">
                    <span class="caption">Исключения в расписании</span>

                    <div class="schedule">
                        <span class="title">Исключение/статус</span>
                        <span class="desc-exc">задайте уникальное расписание</span>
                        <div class="add-exception-box">
                            <div class="add-header">
                                <div class="select-box">
                                    <select2-multiple-control v-model="schedule.exceptions.optionsValue" :settings="{multiple: false, placeholder: 'Выберите исключение'}" :options="schedule.exceptions.addOptions"></select2-multiple-control>
                                </div>
                                <span class="add-history-btn" @click="schedule.exceptions.addException">Добавить</span>
                            </div>
                        </div>
                    </div>
                    <!----------------------------------------------Exceptions-tabs------------------------------------------>
                    <div class="tabs-nav">
                        <a href="#" :class="schedule.exceptions.tab == 0 ? 'active' : ''" @click="schedule.exceptions.setTab(0)">Актуальные исключения</a>
                        <a href="#" :class="schedule.exceptions.tab == 1 ? 'active' : ''" @click="schedule.exceptions.setTab(1)">Исключения на прошедшие периоды</a>
                    </div>
                    <!-------------------------------------------tab Актуальные исключения----------------------------------->
                    <div v-if="schedule.exceptions.tab == 0">
                        <div class="schedule" :class="item.status == 'show' ? 'open' : ''" v-for="(item, index) in schedule.exceptions.exceptionsList" v-if="schedule.exceptions.checkCurrentDate(item)">
                            <div class="title-input">
                                <input type="text" v-model="item.name" />
                                <span class="fa fa-pen toggle-edit-name-btn" v-if="item.status == 'show'" onClick="$(this).parent('.title-input').toggleClass('edit')"></span>
                                <span class="desc" v-if="item.status == 'hide'" style="text-decoration: none;">
                      <span v-if="item.type == 2 || item.type == 3 || item.type == 5">С </span>
                      <span>@{{ item.start_date | moment("DD.MM.YYYY") }}</span>
                      <span v-if="item.type == 2 || item.type == 3 || item.type == 5">по</span>
                      <span v-if="item.type == 2 || item.type == 3 || item.type == 5">@{{ item.end_date | moment("DD.MM.YYYY") }}</span>
                      <span v-if="item.type == 1 || item.type == 2" style="margin-left: 20px;">время </span>
                      <span v-if="item.type == 1 || item.type == 2">@{{ item.start_time }} - @{{ item.end_time }}</span>
                    </span>
                            </div>
                            <div class="header-btns-panel">
                                <span class="btn-remove" v-if="item.status != 'new'" @click="schedule.exceptions.removeException(item.id)">Удалить <i class="fa fa-trash"></i></span>
                                <span class="btn-cancel" v-if="item.status == 'show'" @click="item.status = 'hide';schedule.exceptions.cancelEditException()" onClick="$(this).parent('.header-btns-panel').siblings('.title-input').removeClass('edit')">Отмена</span>
                                <span class="add-schedule-btn" v-if="item.status == 'hide'" @click="schedule.exceptions.editException(item.id);item.status = 'show'">Редактировать <i class="fa fa-pen"></i></span>
                                <span class="add-schedule-btn" v-if="item.status == 'new'" @click="schedule.exceptions.saveException(item)">Сохранить <i class="fa fa-check"></i></span>
                                <span class="add-schedule-btn" v-if="item.status == 'show'" @click="schedule.exceptions.updateException(item)">Сохранить <i class="fa fa-check"></i></span>
                            </div>
                            <div v-if="item.status == 'new' || item.status == 'show'" style="padding:10px">
                                <span class="to">Дата </span>
                                <span class="to" v-if="item.type == 2 || item.type == 3 || item.type == 5">С</span>
                                <datepicker :monday-first="true" placeholder="Выбрать" :disabled-dates="datePickerConfig.disabledPrevDates" :language="datePickerConfig.langDatapicker" v-model="item.start_date" :format="customFormatter"></datepicker>
                                <span class="to" v-if="item.type == 2 || item.type == 3 || item.type == 5">По</span>
                                <datepicker :monday-first="true" placeholder="Выбрать" :disabled-dates="datePickerConfig.disabledPrevDates" v-if="item.type == 2 || item.type == 3 || item.type == 5" v-model="item.end_date" :language="datePickerConfig.langDatapicker" :format="customFormatter"></datepicker>
                                <span v-if="item.type == 1 || item.type == 2" style="margin-left: 20px;"  class="to">Время </span>
                                <div class="times-box" v-if="item.type == 1 || item.type == 2">
                                    <input type="time" v-model="item.start_time" />
                                    <span>-</span>
                                    <input type="time" v-model="item.end_time" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--------------------------------------tab Исключения на прошедшие периоды------------------------------>
                    <div v-if="schedule.exceptions.tab == 1">
                        <div class="schedule" v-for="(item, index) in schedule.exceptions.exceptionsList" v-if="!schedule.exceptions.checkCurrentDate(item)">
                            <div class="title-input">
                                <input type="text" v-model="item.name" />
                                <span class="desc" style="text-decoration: none;">
                      <span v-if="item.type == 2 || item.type == 3 || item.type == 5">С </span>
                      <span>@{{ item.start_date | moment("DD.MM.YYYY") }}</span>
                      <span v-if="item.type == 2 || item.type == 3 || item.type == 5">по</span>
                      <span v-if="item.type == 2 || item.type == 3 || item.type == 5">@{{ item.end_date | moment("DD.MM.YYYY") }}</span>
                      <span v-if="item.type == 1 || item.type == 2" style="margin-left: 20px;">время </span>
                      <span v-if="item.type == 1 || item.type == 2">@{{ item.start_time }} - @{{ item.end_time }}</span>
                    </span>
                            </div>
                        </div>
                    </div>
                    <!------------------------------------------------------------------------------------------------------->
                </div>
            </div>
        </div>
    </div>
@endsection


@push('css')
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
@endpush

@push('scripts')
    <script src="{{ asset('components/user/build.js') }}"></script>

    <script>
        /*Наблюдатель за кареткой курсора в инпуте с маской*/
        var maskCaret = {
            setCaretToPos: function(input, pos){
                if (input.setSelectionRange) {
                    input.focus();
                    input.setSelectionRange(pos, pos);
                }
                else if (input.createTextRange) {
                    var range = input.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                }
            },
            reverseStr: function(str){
                return str.split("").reverse().join("");
            },
            watch: function(input){
                var pos = 0;
                var watch = false;
                input.addEventListener('blur',function(){
                    console.log('blur');
                    pos = 0;
                    watch = true;
                    var str = $(this).val();
                    str = maskCaret.reverseStr(str);
                    for(var i=0;i<str.length; i++){
                        if(!str[i].match(/^\d+/)){
                            pos++;
                        }
                        else {
                            break;
                        }
                    }
                    pos = str.length - pos;
                    str = maskCaret.reverseStr(str);
                    if(pos < str.length){
                        while(str[pos] != '_'){
                            pos++;
                        }
                    }
                },false);
                input.addEventListener('mouseup',function(){
                    console.log('mouseup');
                    if(watch){
                        maskCaret.setCaretToPos(input, pos);
                        watch = false;
                    }
                },false);
            },
            /*Запустить наблюдатель*/
            run: function(){
                var inputs = document.getElementsByClassName('watch-mask');
                for(var i=0; i<inputs.length; i++){
                    maskCaret.watch(inputs[i]);
                }
            }
        }

        $(document).ready(function(){
            console.log('jquery test');
            /*Запустить наблюдатель за каретками в инпутах с маской*/
            maskCaret.run();

        });
    </script>
@endpush