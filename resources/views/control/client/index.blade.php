@extends('layouts.control')

@section('title', __('Clients'))

@section('content')
    <div id="app" v-cloak>
        <!--------------------------------------------SEND SMS MODAL----------------------------------------------------->
        <div class="send-sms-modal" v-if="modalSMS.open == 'show'" style="display: none" v-show="modalSMS.open == 'show'">
            <div class="modal-sms">
                <i class="fa fa-times close-sms-modal" @click="modalSMS.hideModal"></i>
                <span class="title-sms">Отправка @{{ modalSMS.countClients }} SMS</span>
                <textarea v-model="modalSMS.message" maxlength="140"></textarea>
                <span class="icon-line"></span>
                <span class="footer-title-sms">Использовано: <span>@{{ modalSMS.message.length | symbols }}</span></span>
                <span class="btn-send-sms" @click="modalSMS.sendSMS"><i class="fa fa-paper-plane"></i> Отправить</span>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------------->
        <!--------------------------------------------MODAL ADD-CLIENT--------------------------------------------------->
        <div class="overlay" :class="modalClient.open">
            <div class="modal-add-client" :class="modalClient.open">
                <div class="mc-header">
                    <i class="fa fa-plus"></i>
                    <span class="mc-title">@{{ modalClient.title }}</span>
                    <i class="mc-form-close fa fa-times" v-on:click="modalClient.close()"></i>
                </div>
                <div class="mc-body">
                    <div class="tabs-nav">
                        <input name="tab" type="radio" id="tab-1" checked="checked" ref="tabMain">
                        <label for="tab-1" class="label-tab">Основная информация</label>
                        <input name="tab" type="radio" id="tab-2" v-if="modalClient.saveUpdate == 'edit'">
                        <label for="tab-2" class="label-tab" v-if="modalClient.saveUpdate == 'edit'">История взаимодействия</label>
                        <label class="label-tab" v-if="modalClient.saveUpdate == 'save'" style="cursor: default;"></label>
                        <div class="tabs-container">
                            <div class="tab tab-1" id="tab_main">
                                <div class="body">
                                    <div class="left-side">
                                        <div class="group">
                                            <label>ФИО*</label>
                                            <input type="text" v-model="modalClient.client.name" placeholder="Введите ФИО" :class="modalClient.errorsClass.name">
                                        </div>
                                        <div class="group">
                                            <label>Адрес</label>
                                            <input type="text" placeholder="Введите адрес" v-model="modalClient.client.address" :class="modalClient.errorsClass.address">
                                        </div>
                                        <div class="group birthday"><label>Дата рождения</label>
                                            <div class="wrapper-input" :class="modalClient.errorsClass.birthday">
                                                <masked-input ref="datapicker" mask="11.11.1111" placeholder="дд.мм.гггг" v-model="modalClient.client.birthday"></masked-input>
                                                <span class="fa fa-calendar icon"></span>
                                            </div>
                                        </div>
                                        <div class="group">
                                            <label>Тел*</label>
                                            <div class="wrapper-input">
                                                <masked-input mask="\+\38 (111) 111-11-11" placeholder="Введите телефон" class="watch-mask" :class="modalClient.errorsClass.phone" v-model="modalClient.client.maskPhone"></masked-input>
                                                <span class="fa fa-phone icon"></span>
                                            </div>
                                        </div>
                                        <div id="mx_wrapper_phone_additional" class="wrapper-phones">
                                            <!-------------------------------------------------------->
                                            <div v-for="(phone, index) in modalClient.client.phone_additional" class="group" style="position: relative">
                                                <label>Тел @{{ index + 2 }}</label>
                                                <div class="wrapper-input">
                                                    <masked-input mask="\+\38 (111) 111-11-11" placeholder="Введите телефон" v-model="modalClient.client.phone_additional[index]" style="width: 230px"></masked-input>
                                                    <span class="fa fa-phone icon"></span>
                                                </div>
                                                <span class="fa fa-times remove-icon" @click="modalClient.removeAdditionalPhone(phone)"></span>
                                            </div>
                                            <!-------------------------------------------------------->
                                            <div class="group" id="phone_insert">
                                                <span class="fa fa-plus btn-plus" v-on:click="modalClient.addPhone"> <i>Добавить телефон</i></span>
                                            </div>
                                        </div>
                                        <div class="group"><label>E-mail</label>
                                            <div class="wrapper-input">
                                                <input type="text" placeholder="Введите e-mail" v-model="modalClient.client.email" :class="modalClient.errorsClass.email">
                                                <span class="fa fa-at icon"></span>
                                            </div>
                                        </div>
                                        <div id="mx_wrapper_email_additional" class="wrapper-emails">
                                            <!-------------------------------------------------------->
                                            <div v-for="(email, index) in modalClient.client.email_additional" class="group" style="position: relative">
                                                <label>E-mail @{{ index + 2 }}</label>
                                                <div class="wrapper-input">
                                                    <input type="text" v-model="modalClient.client.email_additional[index]" style="width: 230px">
                                                    <span class="fa fa-at icon"></span>
                                                </div>
                                                <span class="fa fa-times remove-icon" @click="modalClient.removeAdditionalEmail(email)"></span>
                                            </div>
                                            <!-------------------------------------------------------->
                                            <div class="group" id="email_insert">
                                                <span class="fa fa-plus btn-plus" v-on:click="modalClient.addEmail"><i>Добавить e-mail</i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-side">
                                        <a :href="'http://service.now?client=' + history.client_id" v-if="modalClient.saveUpdate == 'edit'" class="add-event-btn">Запись на прием</a>
                                        <span class="add-sms-btn" v-if="modalClient.saveUpdate == 'edit'" @click="modalSMS.showModalSingle"><i class="fa fa-paper-plane"></i> SMS</span>
                                    </div>
                                </div>
                                <div class="footer">
                                    <div id="mc_wrapper_social" class="wrapper-social">
                                        <div class="soc">
                                            <input type="text" v-model="modalClient.client.social_links_fb" placeholder="https://facebook.com" :class="modalClient.errorsClass.social_links_fb">
                                            <span class="fab fa-facebook-f icon"></span>
                                        </div>
                                        <div class="soc">
                                            <input type="text" v-model="modalClient.client.social_links_insta" placeholder="https://www.instagram.com" :class="modalClient.errorsClass.social_links_insta">
                                            <span class="fab fa-instagram icon"></span>
                                        </div>
                                    </div>
                                    <div id="mc_wrapper_comment">
                                        <label class="textarea-label">Комментарий</label>
                                        <textarea v-model="modalClient.client.description" placeholder="Комментарий" :class="modalClient.errorsClass.description"></textarea>
                                    </div>
                                    <div id="mc_wrapper_validate" class="validate-container"></div>
                                    <div id="mc_wrapper_btns" class="btns-panel">
                                        <span class="btn btn-warning" @click="modalClient.close()">Отменить</span>
                                        <span class="btn btn-success" @click="modalClient.save">Сохранить изменения</span></div>
                                </div>
                            </div>
                            <div class="tab tab-2" id="tab_history">
                                <div class="range-history-container">
                                    <span class="text">Выберите период</span>
                                    <rangedate-picker
                                            ref="randeDatepikerHistory"
                                            format="DD.MM.YYYY"
                                            @selected="history.setRange"
                                            :months="['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']"
                                            :short-days="['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']"
                                            :captions="{'title': 'Выберите дату','ok_button': 'Применить'}"
                                            :preset-ranges="presetRanges"
                                            class="filter-history-range"
                                    >
                                    </rangedate-picker>
                                    <span class="history-range-clear-sel-btn fa fa-trash" v-on:click="history.clearRange"></span>
                                </div>
                                <div class="scroll-container">
                                    <div class="add-by-history-container">
                                        <div class="title">Добавить в историю</div>
                                        <div class="fon">
                                            <div class="add-header">
                                                <span class="icon icon-plus"></span>
                                                <div class="select-box">
                                                    <select2-multiple-control :settings="{multiple: false, placeholder: 'Выберите взаимодействие'}" ref="selectHystoryTypes" v-model="history.typeSelectId" :options="history.typesOptions"></select2-multiple-control>
                                                </div>
                                                <span class="add-history-btn" @click="history.addToHistory">Добавить</span>
                                            </div>
                                            <textarea v-model="history.comment"></textarea>
                                        </div>
                                    </div>
                                    <div v-for="(item, index) in history.client.stories" v-if="item.filter">
                                        <span>@{{ new Date(item.date) | moment("DD.MM.YY") }}</span>
                                        <div class="history-elem-container" v-for="it in item.items">
                                            <div class="fon">
                                                <div class="he-header">
                                                    <span class="icon" :style="'background-image: url(/img/history-types/' + history.typesOptionsAll[it.type_id].icon + ')'"></span>
                                                    <div class="desc">
                                                        @{{ new Date(it.timestamp) | moment("HH:mm") }} @{{ history.typesOptionsAll[it.type_id].name }}
                                                    </div>
                                                    <span class="toggle-btn" @click="history.toggleHistoryElem"></span>
                                                </div>
                                                <div class="textarea">
                                                    <ul>
                                                        <li v-for="(item, index) in it.details">
                                                            <dl>
                                                                <dt><strong>@{{history.details[index].name}}:</strong></dt>
                                                                <dd v-if="item.o != ', '">
                                                                    <strong v-if="[8,9,10,11,12,13,14,15].indexOf(parseInt(index)) != -1">C </strong>
                                                                    <span :style="[24,25].indexOf(parseInt(index)) != -1 ? '' : 'color: red'">@{{item.o}}</span>
                                                                </dd>
                                                                <dd v-if="item.n != ', '">
                                                                    <strong v-if="[8,9,10,11,12,13,14,15].indexOf(parseInt(index)) != -1">На </strong>
                                                                    <span style="color: green">@{{item.n}}</span>
                                                                </dd>
                                                            </dl>
                                                        </li>
                                                    </ul>
                                                    <strong v-if="it.type_id == 5">Комментарий к отмене:<br></strong>
                                                    <span v-if="it.comment != ''" v-html="it.comment" style="padding-left: 15px;"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------------------------------------------------------------------------------------------->
        <div class="header">
            <span class="title">Клиенты</span>
            <span class="btn-def" v-on:click="addClient()" style="margin-left: 30px">Добавить клиента <i class="fa fa-plus"></i></span>
            <div class="selClientBox">
                <div class="select-box">
                    <select2-multiple-control :settings="{multiple: false,minimumResultsForSearch:Infinity}" v-model="smsOrRemove" :options="[{id:'1',text:'SMS рассылка'},{id:'2',text:'Удалить'}]"></select2-multiple-control>
                </div>
                <span class="set-btn" @click="applySelectedClients">Выполнить</span>
            </div>
        </div>
        <div class="content">
            <div class="filters-panel">
                <aside>
                    <span class="title">Фильтры</span>
                    <div class="filter-box open">
                        <div class="fb-head">
                            <span>Период</span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="fb-wrapper">
                            <rangedate-picker
                                    ref="randeDatepiker"
                                    format="DD.MM.YYYY"
                                    @selected="filterRange"
                                    :months="['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']"
                                    :short-days="['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']"
                                    :captions="{'title': 'Выберите дату','ok_button': 'Применить'}"
                                    :preset-ranges="presetRanges"
                                    class="filter-clients-range"
                            >
                            </rangedate-picker>
                            <div class="links-panel">
                                <span class="link-range" @click="initRangeDatepicker('today')">Сегодня</span>
                                <span class="link-range" @click="initRangeDatepicker('week')">За неделю</span>
                                <span class="link-range" @click="initRangeDatepicker('month')">За месяц</span>
                                <span class="link-range" @click="initRangeDatepicker('year')">За год</span>
                                <span class="link-range" @click="initRangeDatepicker('weekStart')">С начала недели</span>
                                <span class="link-range" @click="initRangeDatepicker('monthStart')">С начала месяца</span>
                                <span class="link-range" @click="initRangeDatepicker('tomorrow')">Завтра</span>
                            </div>
                        </div>
                    </div>
                    <div class="filter-box open">
                        <div class="fb-head">
                            <span>По специалисту</span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="fb-wrapper">
                            <select2-multiple-control ref="selectUsers" :settings="{closeOnSelect:false,placeholder:'Выберите специалистов'}" v-model="filter.usersSelectIds" :options="allUsersFromSelect" @change="filterUsers"></select2-multiple-control>
                        </div>
                    </div>
                    <div class="filter-box open">
                        <div class="fb-head">
                            <span>По услуге</span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="fb-wrapper">
                            <select2-multiple-control ref="selectServices" :settings="{closeOnSelect:false,placeholder:'Выберите услуги'}" v-model="filter.servicesSelectIds" :options="allServicesFromSelect" @change="filterServices"></select2-multiple-control>
                        </div>
                    </div>
                    <div class="filter-box" :class="www ? 'open' : ''">
                        <div class="fb-head-range" @click="www = !www;$refs.slider.refresh()">
                            <span>По количеству сеансов</span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="fb-wrapper-range" style="padding-top: 40px;" v-if="www">
                            <vue-slider
                                    ref="slider"
                                    v-model="filter.countVisits"
                                    :min="0"
                                    :max="filter.maxVisits"
                                    :show="true"
                                    @drag-end="filterVisits"
                            >
                            </vue-slider>
                        </div>
                    </div>
                    <!--<div class="filter-box">
                        <div class="fb-head">
                            <span>По статусу</span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="fb-wrapper">
                            Фильтрация по статусу
                        </div>
                    </div>-->
                </aside>
            </div>
            <div class="content-wrapper">
                <div class="top-side">
                    <div class="search-box">
                        <i class="fa fa-search icon icon-search"></i>
                        <i class="fa fa-times icon icon-close" v-on:click="searchClear()"></i>
                        <input type="text" v-model="searchText" @keyup="filterSearch">
                        <span class="text-search">Поиск</span>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="selected-filters showAfterLoad">
                        <div class="result-filter-title" v-if="filter.filterEnabled">
                            Найдено: <span>@{{ filter.filtersClients.length | declension(['клиент','клиента','клиентов']) }}</span>
                        </div>
                        <ul>
                            <li v-if="filter.filterEnabled" class="clear-all" @click="filterAllClear">
                                <i class="fa fa-times"></i>
                                <span>Очистить все</span>
                            </li>
                            <li v-if="filter.filterRangeEnabled">
                                <span v-html="filter.selectFilters.range.name"></span>
                                <i class="fa fa-times" @click="filterRangeClear"></i>
                            </li>
                            <li v-if="filter.filterServicesEnabled" v-for="services in filter.selectFilters.services">
                                <span v-html="services.name"></span>
                                <i class="fa fa-times" @click="filterServicesClear(services.id)"></i>
                            </li>
                            <li v-if="filter.filterUsersEnabled" v-for="users in filter.selectFilters.users">
                                <span v-html="users.name"></span>
                                <i class="fa fa-times" @click="filterUsersClear(users.id)"></i>
                            </li>
                            <li v-if="filter.filterVisitsEnabled">
                                <span v-html="filter.selectFilters.visits.name"></span>
                                <i class="fa fa-times" @click="filterVisitsClear"></i>
                            </li>
                        </ul>
                    </div>
                    <table class="table table-hover table-component">
                        <thead class="thead-clients">
                        <tr>
                            <th style="padding-top: 0px; width: 40px">
                                <input type="checkbox" class="checkbox" id="checkbox_0" @change="selectAllClients">
                                <label for="checkbox_0"></label>
                            </th>
                            <th class="sortable" style="width: 450px" :class="sort.class.name" @click="sort.sorter('name')">ФИО</th>
                            <th class="sortable" style="width: 200px" :class="sort.class.phone" @click="sort.sorter('phone')">Телефон</th>
                            <th>Описание</th>
                            <th class="th-context-menu"></th>
                        </tr>
                        </thead>
                        <tbody class="tbody-clients showAfterLoad">
                        <!--Быстрая форма добавления нового клиента-->
                        <tr class="quick-form-add">
                            <td></td>
                            <td class="fio">
                                <input type="text" placeholder="ФИО" :class="quickForm.errorsClass.name" v-model="quickForm.client.name">
                            </td>
                            <td class="phone" colspan="4">
                                <masked-input mask="\+\38 (111) 111-11-11" @input="quickForm.maskToPhone" class="watch-mask" placeholder="Телефон" :class="quickForm.errorsClass.phone" v-model="quickForm.client.phoneMask"></masked-input>
                                <span class="fa fa-plus quick-add-btn" @click="quickForm.save"></span>
                            </td>
                        </tr>
                        <!------------------------------------------->
                        <tr v-for="(item, index) in clients">
                            <td style="padding-top: 0px;">
                                <input type="checkbox" class="checkbox" v-model="selClientIds" :value="item.id" :id="'checkbox_' + item.id">
                                <label :for="'checkbox_' + item.id"></label>
                            </td>
                            <td><a href="javascript:void(0)" v-on:click="editClient(item.id)" style="font-size:16px">@{{ item.name }}</a></td>
                            <td><a :href="'tel:+38' + item.phone">@{{ item.phone | toMask }}</a></td>
                            <td>@{{ limitStr(item.description) }}</td>
                            <td>
                                <div class="menu">
                                    <span class="fa fa-ellipsis-v btn-toggle-menu" v-on:click="toggleMenuEdit(item.id)"></span>
                                    <ul class="menu-ul" v-bind:data-id="item.id">
                                        <li v-on:click="editClient(item.id)">
                                            <i class="fa fa-pen"></i><span>Редактировать</span>
                                        </li>
                                        <li v-on:click="modalClient.deleteClient(item.id)">
                                            <i class="fa fa-trash"></i><span>Удалить</span>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="pagination-box">
                        <vue-ads-pagination v-if="showPaginateAfterLoad" :total-items="countClients" :page="page" :items-per-page="100" v-on:page-change="pageChange">
                            <template slot-scope="props">
                                <div class="pr-2 leading-loose">
                                <!--@{{ start }} - @{{ end }} из @{{ countServices }}-->
                                </div>
                            </template>
                        </vue-ads-pagination>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@push('css')
    <link rel="stylesheet" href="{{ asset('css/clients.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('components/clients/build.js') }}"></script>

    <script>
        /*Наблюдатель за кареткой курсора в инпуте с маской*/
        var maskCaret = {
            setCaretToPos: function(input, pos){
                if (input.setSelectionRange) {
                    input.focus();
                    input.setSelectionRange(pos, pos);
                }
                else if (input.createTextRange) {
                    var range = input.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                }
            },
            reverseStr: function(str){
                return str.split("").reverse().join("");
            },
            watch: function(input){
                var pos = 0;
                var watch = false;
                input.addEventListener('blur',function(){
                    console.log('blur');
                    pos = 0;
                    watch = true;
                    var str = $(this).val();
                    str = maskCaret.reverseStr(str);
                    for(var i=0;i<str.length; i++){
                        if(!str[i].match(/^\d+/)){
                            pos++;
                        }
                        else {
                            break;
                        }
                    }
                    pos = str.length - pos;
                    str = maskCaret.reverseStr(str);
                    if(pos < str.length){
                        while(str[pos] != '_'){
                            pos++;
                        }
                    }
                },false);
                input.addEventListener('mouseup',function(){
                    console.log('mouseup');
                    if(watch){
                        maskCaret.setCaretToPos(input, pos);
                        watch = false;
                    }
                },false);
            },
            /*Запустить наблюдатель*/
            run: function(){
                var inputs = document.getElementsByClassName('watch-mask');
                for(var i=0; i<inputs.length; i++){
                    maskCaret.watch(inputs[i]);
                }
            }
        }

        $(document).ready(function(){
            console.log('jquery test');
            /*Запустить наблюдатель за каретками в инпутах с маской*/
            maskCaret.run();

            $('.showAfterLoad').fadeIn(300);

            /*Открыть поле поиска*/
            $('.icon-search').on('click',function (e) {
                $(this).parent('.search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).css('display','none');
                $(this).siblings('.icon-close').css('display','inline-block');
            });
            /*Открыть поле поиска*/
            $('.text-search').on('click',function (e) {
                $(this).parent('.search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).siblings('.icon-search').css('display','none');
                $(this).siblings('.icon-close').css('display','inline-block');
            });
            /*Закрыть поле поиска*/
            $('.icon-close').on('click',function (e) {
                $(this).parent('.search-box').removeClass('open');
                $(this).css('display','none');
                $(this).siblings('.icon-search').css('display','inline-block');
            });

            /*Закрыть панель фильтров*/
            $('.close-filter-panel-btn').on('click',function(){
                $('.filters-panel').hide();
            });

            /*открыть закрыть filter-box*/
            $(".fb-head").on('click',function(){
                $(this).parent('.filter-box').toggleClass('open');
            });

        });

        /*Клик вне элемента*/
        $(document).mouseup(function (e) {
            var container = $(".menu");
            if (container.has(e.target).length === 0){
                container.children('ul').removeClass('open');
            }

            var container = $("#filter_modal");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul').removeClass('open');
            }

            var container = $("#filter_modal_roles");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul-roles').removeClass('open');
            }

        });
    </script>
@endpush