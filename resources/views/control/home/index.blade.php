@extends('layouts.control')

@section('title', __('Календарь'))

@section('content')

    <input type="hidden" id="token" value="hCpTSd21EsXhPIGhMOs5I9M4bm9e0cQQpIHDms2u">
    <!--авторизированный пользователь-->
    {{--<input type="hidden" id="auth_user_id" value="{{ auth()->user()->id }}">--}}
    <input type="hidden" id="auth_user_id" value="2">


    <div style='width:100%; height: 100%' class="calendar-container">
        <div class="spinner-fon">
            <span class="fa fa-spinner fa-spin"></span>
        </div>
        <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height: 100%'>
            <div class="dhx_cal_navline">
                <div id="menu_users"></div>
                <div class="dhx_cal_prev_button">&nbsp;</div>
                <div class="dhx_cal_next_button">&nbsp;</div>
                <div class="dhx_cal_today_button"></div>
                <div class="dhx_cal_date" id="dhx_cal_date" onclick="show_minical()"></div>
                <div class="dhx_cal_tab" name="day_tab" style="left:70px; top:11px"></div>
                <div class="dhx_cal_tab" name="week_tab" style="left:159px; top:11px"></div>
                <div class="dhx_cal_tab" name="month_tab" style="left:248px; top:11px"></div>
                <div class="dhx_cal_tab" name="agenda_tab" id="agenda_tab" style="left:337px; top:11px; width: 120px"></div>
            </div>
            <div class="dhx_cal_header"></div>
            <div class="dhx_cal_data"></div>
        </div>
    </div>
    <!--modal event-->
    <div id="event_modal">
        <div id="em_header">
            <i class="fa fa-calendar-plus"></i>
            <span id="em_title"></span>
            <i id="em_form_close" class="fa fa-times"></i>
        </div>
        <div id="em_body">
            <div class="dates">
                <div class="date-box" id="start_date_box">
                    <span class="text">Начало</span>
                    <div id="em_date_start"></div>
                    <div id="em_time_start"></div>
                </div>
                <div class="date-box" id="end_date_box">
                    <span class="text">Конец</span>
                    <div id="em_date_end"></div>
                    <div id="em_time_end"></div>
                </div>
            </div>
            <div class="select-container">
                <span class="text">Сотрудник</span>
                <div id="em_user"></div>
            </div>
            <div class="select-client-container">
                <span class="text">Клиент</span>
                <div id="em_client"></div>
            </div>
            <div class="select-container-multiple">
                <span class="text">Услуги</span>
                <div id="em_service"></div>
            </div>
            <div class="comment-container">
                <span class="text">Комментарий</span>
                <div id="em_comment"></div>
            </div>
            <div id="validate-container"></div>
        </div>
        <div id="em_footer"></div>
    </div>

    <!--Модальное окно Отменить запись-->
    <div id="modal_cancel_event">
        <div id="mce_header">
            <i class="fa fa-ban"></i>
            <span id="mce_title">Отменить запись</span>
            <i id="mce_form_close" class="fa fa-times"></i>
        </div>
        <div id="mce_body">
            <div>
                <input type="radio" name="event_cancel" id="mce_no_show_radio" checked value="2">
                <label for="mce_no_show_radio">Не явка по записи</label>
            </div>
            <div>
                <input type="radio" name="event_cancel" id="mce_cancel_radio" value="1">
                <label for="mce_cancel_radio">Отмена</label>
            </div>
            <div class="mce-comment-container">
                <span class="text">Комментарий</span>
                <textarea id="mce_comment"></textarea>
            </div>
            <div id="mce_validate_container"></div>
        </div>
        <div id="mce_footer">
            <span class="btn btn-success" id="mce_btn_save">Сохранить</span>
        </div>
    </div>
@endsection


@push('css')
    <link rel="stylesheet" href="{{ asset('mod/dhtmlxScheduler_v5.0.0/codebase/dhtmlxscheduler_material.css') }}">
    <link rel="stylesheet" href="{{ asset('mod/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('mod/jonthornton-jquery-timepicker-b53338c/jquery.timepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('mod/select2-4.0.1/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('mod/windows/wins.css') }}">
    <link rel="stylesheet" href="{{ asset('css/calendar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/clients.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('mod/dhtmlxScheduler_v5.0.0/codebase/dhtmlxscheduler.js') }}"></script>
    <script src="{{ asset('mod/dhtmlxScheduler_v5.0.0/codebase/locale/locale_ru.js') }}" charset="utf-8"></script>
    <script src="{{ asset('mod/dhtmlxScheduler_v5.0.0/codebase/ext/dhtmlxscheduler_active_links.js') }}"></script>
    <script src="{{ asset('mod/dhtmlxScheduler_v5.0.0/codebase/ext/dhtmlxscheduler_limit.js') }}"></script>
    <script src="{{ asset('mod/dhtmlxScheduler_v5.0.0/codebase/ext/dhtmlxscheduler_minical.js') }}"></script>
    <script src="{{ asset('mod/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('mod/jonthornton-jquery-timepicker-b53338c/jquery.timepicker.js') }}"></script>
    <script src="{{ asset('mod/windows/lib_ws.js') }}"></script>
    <script src="{{ asset('mod/select2-4.0.1/dist/js/select2.js') }}"></script>
    <script src="{{ asset('mod/js/crud.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.8/dist/sweetalert2.all.min.js"></script>
    <script src="{{ asset('/mod/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js') }}"></script>

    <script>
        scheduler.config.fix_tab_position = false;/*перемещает вкладки «взгляды» слева направо*/
        var __token__ = document.getElementById('token').value;
        var __authUserId__ = document.getElementById('auth_user_id').value;
        var __authUser__ = {};
        var __currentUserIdByCalendar__ = __authUserId__;

        var spinner = {
            calendar: {
                status: false,
                show: function(){
                    console.log('spinner show');
                    if(spinner.calendar.status == false){
                        $('.spinner-fon').css("display", "flex").hide().fadeIn(300);
                        spinner.calendar.status = true;
                    }
                },
                hide: function(){
                    if(spinner.calendar.status == true){
                        $('.spinner-fon').fadeOut(300);
                        spinner.calendar.status = false;
                        console.log('spinner hide');
                    }
                }
            }
        }

        /*Рабочий график*/
        var allSchedule = {
            begin_scale: 1440,
            end_scale: 0,
            mainSchedule: {},
            userSchedule: [],
            userExceptions: [],
            timeToMinutes: function(time){
                time = time.split(':');
                var h = parseInt(time[0]);
                var m = parseInt(time[1]);
                return (h*60) + m;
            },
            markerIds:[],/*ids - помеченых графиков*/
            status:false,/*true - Значет рабочий график отображен*/
            /*get расписание предприятия, графикы пользователя, исключения пользователя*/
            get: function(user_id){
                spinner.calendar.show();
                $.ajax({
                    url:'/api/schedules/' + user_id,
                    type:'get',
                    dataType:'json',
                    headers: {
                        'Authorization': 'Bearer ' + safeData.api_token
                    },
                    data:{},
                    success: function(json){
                        allSchedule.mainSchedule = json[0][0];
                        allSchedule.userSchedule = json[1];
                        allSchedule.userExceptions = json[2];
                        console.log(allSchedule.mainSchedule);
                        console.log(allSchedule.userSchedule);
                        console.log(allSchedule.userExceptions);
                        $.ajax({
                            url:'/api/events',
                            type:'get',
                            dataType:'json',
                            headers: {
                                'Authorization': 'Bearer ' + safeData.api_token
                            },
                            success:function(events){
                                setEventsToCalendar(events,user_id);
                            }
                        });
                        /*Переход со страницы клиенты*/
                        /*Если есть get параметр client, значит нужно открыть lightbox для создание записи для этого клиента*/
                        if(gup('client',null)){
                            var client_id = gup('client',null);
                            var ev_id = new Date().getTime();
                            var formatFunc = scheduler.date.date_to_str("%d.%m.%Y %H:%i");
                            var tomorrow = formatFunc(new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate() + 1,0,0,0));
                            var event = {
                                update: false,
                                text: client_id,
                                services: '',
                                id: client_id,
                                all_day: false,
                                client_id: client_id,
                                service_id: null,
                                user_id: 0,
                                user_name: '',
                                description: '',
                                start_date: tomorrow,
                                end_date: tomorrow,
                                color: '#999'
                            }
                            scheduler.addEvent(event);
                            scheduler.showLightbox(client_id);
                            window.history.pushState(null, null, '/');
                        }
                    }
                });
            },
            /*установить пометку на день*/
            setDay: function(date,day,name='Выходной'){
                var mode = scheduler.getState().mode;
                if(day.status == true){
                    if(mode != 'month'){
                        var zones = {
                            1: 0,
                            2: allSchedule.timeToMinutes(day.start),
                            3: allSchedule.timeToMinutes(day.end),
                            4: 24*60
                        }
                        scheduler.addMarkedTimespan({
                            days:  new Date(date),
                            zones:[
                                zones['1'],
                                zones['2'],
                                zones['3'],
                                zones['4'],
                            ],
                            css:   "gray_section",
//                            type:  "dhx_time_block",
                            type:  "holiday",
                        })
                    }
                }
                else {
                    scheduler.addMarkedTimespan({
                        days:  new Date(date),
                        zones: "fullday",
                        css:   "red_section",
//                        type:  "dhx_time_block",
                        type:  "holiday",
                        html:  "<div style='color:white; text-align:center;margin-top: -6px;'>" + name + "</div>"
                    })
                }
            },
            /*Поиск даты в обьектах (Исключения, график пользователя, график предприятия)*/
            findDate: function(dateT){
                /*Поиск в исключениях*/
                var schedule = allSchedule.userExceptions;
                if(schedule.length > 0){
                    for(var i=0; i<schedule.length; i++){
                        if(schedule[i].end_date != null){
                            var date_start = parseInt(schedule[i].start_date) * 1000;
                            var date_end = parseInt(schedule[i].end_date) * 1000;
                            if(dateT >= date_start && dateT <= date_end){
                                if(schedule[i].start_time != null)
                                    if(allSchedule.timeToMinutes(schedule[i].start_time) < allSchedule.begin_scale)
                                        allSchedule.begin_scale = allSchedule.timeToMinutes(schedule[i].start_time);
                                if(schedule[i].end_time != null)
                                    if(allSchedule.timeToMinutes(schedule[i].end_time) > allSchedule.end_scale)
                                        allSchedule.end_scale = allSchedule.timeToMinutes(schedule[i].end_time);
                                return {
                                    type: 'exception',
                                    o: schedule[i]
                                };
                            }
                        }
                        else {
                            var date = parseInt(schedule[i].start_date) * 1000;
                            if(date == dateT){
                                if(schedule[i].start_time != null)
                                    if(allSchedule.timeToMinutes(schedule[i].start_time) < allSchedule.begin_scale)
                                        allSchedule.begin_scale = allSchedule.timeToMinutes(schedule[i].start_time);
                                if(schedule[i].end_time != null)
                                    if(allSchedule.timeToMinutes(schedule[i].end_time) > allSchedule.end_scale)
                                        allSchedule.end_scale = allSchedule.timeToMinutes(schedule[i].end_time);
                                return {
                                    type: 'exception',
                                    o: schedule[i]
                                };
                            }
                        }
                    }
                }
                /*поиск в расписании сотрудника*/
                schedule = allSchedule.userSchedule;
                if(schedule.length > 0){
                    for(var i=0; i<schedule.length; i++){
                        var start = parseInt(schedule[i].start) * 1000;
                        var end = parseInt(schedule[i].end) * 1000;
                        if(dateT >= start && dateT <= end){
                            var numWeek = new Date(dateT).getDay();
                            if(numWeek == 0) numWeek = 7;
                            days = JSON.parse(schedule[i].days);
                            if(days[numWeek].status){
                                if(allSchedule.timeToMinutes(days[numWeek].start) < allSchedule.begin_scale)
                                    allSchedule.begin_scale = allSchedule.timeToMinutes(days[numWeek].start);
                                if(allSchedule.timeToMinutes(days[numWeek].end) > allSchedule.end_scale)
                                    allSchedule.end_scale = allSchedule.timeToMinutes(days[numWeek].end);
                            }
                            return {
                                type: 'user',
                                date: dateT,
                                day: days[numWeek],
                                o: schedule[i]
                            };
                        }
                    }
                }
                /*Поиск в расмисании предприятия*/
                schedule = allSchedule.mainSchedule;
                var numWeek = new Date(dateT).getDay();
                if(numWeek == 0) numWeek = 7;
                var days = JSON.parse(schedule.days);
                if(days[numWeek].status){
                    if(allSchedule.timeToMinutes(days[numWeek].start) < allSchedule.begin_scale)
                        allSchedule.begin_scale = allSchedule.timeToMinutes(days[numWeek].start);
                    if(allSchedule.timeToMinutes(days[numWeek].end) > allSchedule.end_scale)
                        allSchedule.end_scale = allSchedule.timeToMinutes(days[numWeek].end);
                }
                return {
                    type: 'company',
                    date: dateT,
                    day: days[numWeek],
                    o: schedule
                };
            },
            /*Получить время начала рабочено дня предприятия по дате*/
            findCompanyStarttime: function(dateT){
                /*Поиск в расмисании предприятия*/
                schedule = allSchedule.mainSchedule;
                var numWeek = new Date(dateT).getDay();
                if(numWeek == 0) numWeek = 7;
                var days = JSON.parse(schedule.days);
                if(days[numWeek].status){
                    return days[numWeek].start
                }
                else {
                    return "00:00";
                }
            },
            render: function(){
                console.log('count',allSchedule.userSchedule.length);
//            if(allSchedule.userSchedule.length == 0) return false;
                console.log('render');
                allSchedule.begin_scale = 1440;
                allSchedule.end_scale = 0;
                /*Очисчаем все пометки*/
                scheduler.deleteMarkedTimespan();
                /*Выделяем прошедшие дни другим цветом*/
                scheduler.addMarkedTimespan({
                    start_date: new Date(2000,1,1),
                    end_date: new Date(),
                    css:   "gray_section"
                });
                var start = scheduler.getState().min_date.getTime();
                var end = scheduler.getState().max_date.getTime();
                while(start<end){
                    var find = allSchedule.findDate(start);
                    if(find.type == 'company' || find.type == 'user'){
                        allSchedule.setDay(start,find.day);
                    }
                    else if (find.type == 'exception'){
                        switch(find.o.type) {
                            case 1:
                                allSchedule.setDay(
                                    start,
                                    {status: true, start: find.o.start_time, end: find.o.end_time,}
                                );
                                break;
                            case 2:
                                allSchedule.setDay(
                                    start,
                                    {status: true, start: find.o.start_time, end: find.o.end_time,}
                                );
                                break;
                            case 3:
                                allSchedule.setDay(
                                    start,
                                    {status: false, start: find.o.start_time, end: find.o.end_time,},
                                    find.o.name
                                );
                                break;
                            case 4:
                                allSchedule.setDay(
                                    start,
                                    {status: false, start: find.o.start_time, end: find.o.end_time,},
                                    find.o.name
                                );
                                break;
                            case 5:
                                allSchedule.setDay(
                                    start,
                                    {status: false, start: find.o.start_time, end: find.o.end_time,},
                                    find.o.name
                                );
                                break;
                            case 6:
                                allSchedule.setDay(
                                    start,
                                    {status: false, start: find.o.start_time, end: find.o.end_time,},
                                    find.o.name
                                );
                                break;
                            default:
                                break;
                        }
                    }
                    start = start + (24 * 60 * 60 * 1000);
                }
//            scheduler.config.first_hour = Math.floor(allSchedule.begin_scale / 60);
//            scheduler.config.last_hour = Math.floor(allSchedule.end_scale / 60);
                if(Math.floor(allSchedule.begin_scale / 60) == 24 && Math.floor(allSchedule.end_scale / 60) == 0){
                    scheduler.config.first_hour = 0;
                    scheduler.config.last_hour = 24;
                }
                else {
                    scheduler.config.first_hour = Math.floor(allSchedule.begin_scale / 60);
                    scheduler.config.last_hour = Math.floor(allSchedule.end_scale / 60);
                }
                scheduler.updateView();
                $(".gray_section").parent('.dhx_month_body').parent('td').addClass("past-day");
                $(".red_section").parent('.dhx_month_body').parent('td').addClass("past-day-red");
                spinner.calendar.hide();
            }
        }

        /*получить текучего пользователя*/
        function getCurrentUser(){
            $.ajax({
                url:'/api/users/' + __authUserId__,
                type:'get',
                dataType:'json',
                headers: {
                    'Authorization': 'Bearer ' + safeData.api_token
                },
                success:function(json){
                    __authUser__ = json;
                    console.log(__authUser__);
                    /*достаем всех пользователей и по калбеку рисуем меню*/
                    getAllUsers(menuUser.showMenu);
                },
                error:function(json){
                    __authUser__ = {id:0,user_role_id:0};
                },
            })
        }
        getCurrentUser();

        /*Отоюразить мини календарь*/
        function show_minical(){
            if (scheduler.isCalendarVisible()){
                scheduler.destroyCalendar();
            } else {
                scheduler.renderCalendar({
                    position:"dhx_cal_date",
                    date:scheduler._date,
                    navigation:true,
                    handler:function(date,calendar){
                        scheduler.setCurrentView(date);
                        scheduler.destroyCalendar()
                    }
                });
            }
        }


        /*Вспомагательные функции*/
        //  Remove all children of element
        function HtmlChildRemove ( elem ) {  //  Rename func
            while ( elem.lastChild ) {
                elem.removeChild ( elem.lastChild );
            }
        }

        /*Просто все пользователи*/
        var allUsers = {};

        /*get all users*/
        function getAllUsers(callback){
            $.ajax({
                url:'/api/users',
                type:'get',
                dataType:'json',
                headers: {
                    'Authorization': 'Bearer ' + safeData.api_token
                },
                success:function(json){
                    callback(json);
                    if(json.length > 0){
                        for(var i=0; i<json.length; i++){
                            allUsers[json[i].id] = json[i];
                        }
                    }
                }
            });
        }

        /*get all users*/
        function getAllUsersByDate(date,callback){
            $.ajax({
                url:'/api/users?date=' + date,
                type:'get',
                dataType:'json',
                headers: {
                    'Authorization': 'Bearer ' + safeData.api_token
                },
                success:function(json){
                    callback(json);
                }
            });
        }

        /*get all services*/
        function getAllServices(callback){
            $.ajax({
                url:'/api/services',
                type:'get',
                dataType:'json',
                headers: {
                    'Authorization': 'Bearer ' + safeData.api_token
                },
                success:function(json){
                    callback(json);
                }
            });
        }

        /*get all events*/
        function getAllEvents(callback){
            $.ajax({
                url:'/api/events',
                type:'get',
                dataType:'json',
                headers: {
                    'Authorization': 'Bearer ' + safeData.api_token
                },
                success:function(json){
                    callback(json);
                }
            });
        }

        //This will sort your array
        function SortByName(a, b){
            var aName = a.name.toLowerCase();
            var bName = b.name.toLowerCase();
            return aName.localeCompare(bName);
        }

        //This will sort your array
        function SortByCategory(a, b){
            var aName = a.category.toLowerCase();
            var bName = b.category.toLowerCase();
            return aName.localeCompare(bName);
        }

        /*filter events by user_id*/
        function filterEventsByUser(events,user_id){
            if (user_id != 0){
                if(events.length > 0){
                    let newEvents = [];
                    for(let i=0; i<events.length; i++){
                        if(events[i].user.id == user_id){
                            newEvents.push(events[i]);
                        }
                    }
                    return newEvents;
                }
                else {
                    return events;
                }
            }
            else {
                return events;
            }
        }

        /*get all clients*/
        function getAllClients(callback){
            $.ajax({
                url:'/api/clients',
                type:'get',
                dataType:'json',
                headers: {
                    'Authorization': 'Bearer ' + safeData.api_token
                },
                success:function(json){
                    callback(json);
                }
            });
        }

        function formatMdy(strDate){
            var dateParts = strDate.split(".");
            var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
            let newDateStr = dateObject.getMonth()+1 + "/" + dateObject.getDate() + "/" + dateObject.getFullYear();
            return newDateStr;
        }

        function formatYmdHis(strDate){
            var dateParts = strDate.split(".");
            var date = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let seconds = date.getSeconds();
            if(seconds.toString().length == 1) seconds = "0" + seconds;
            let str = year + "-" + month  + "-" + day + " " + hours + ":" + minutes  + ":" + seconds;
            return str;
        }

        function formatYmdHisToDmy(strDate){
            var date = new Date(strDate);
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let seconds = date.getSeconds();
            if(seconds.toString().length == 1) seconds = "0" + seconds;
            let str = day + "." + month  + "." + year;
            return str;
        }

        function formatDateToYmdHis(strDate){
            let date = new Date(strDate);
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let seconds = date.getSeconds();
            if(seconds.toString().length == 1) seconds = "0" + seconds;
            let str = year + "-" + month  + "-" + day + " " + hours + ":" + minutes  + ":" + seconds;
            return str;
        }

        function dateToYmdHis(date){
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let seconds = date.getSeconds();
            if(seconds.toString().length == 1) seconds = "0" + seconds;
            let str = year + "-" + month  + "-" + day + " " + hours + ":" + minutes  + ":" + seconds;
            return str;
        }

        function dateToDmyhis(date){
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let str = day + "." + month  + "." + year + " " + hours + ":" + minutes;
            return str;
        }

        function dateToHI(date){
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let str = hours + ":" + minutes;
            return str;
        }

        function dateToDmy(date){
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let str = day + "." + month  + "." + year;
            return str;
        }

        function dateToHi(date){
            let day = date.getDate();
            if(day.toString().length == 1) day = "0" + day;
            let month = parseInt(date.getMonth()+1);
            if(month.toString().length == 1) month = "0" + month;
            let year = date.getFullYear();
            let hours = date.getHours();
            if(hours.toString().length == 1) hours = "0" + hours;
            let minutes = date.getMinutes();
            if(minutes.toString().length == 1) minutes = "0" + minutes;
            let str = hours + ":" + minutes;
            return str;
        }

        function toTimestamp(strDate){
            var datum = Date.parse(strDate);
            return datum/1000;
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        /*Отобразить события на календаре*/
        function setEventsToCalendar(events,user_id){
            console.log('setEventsToCalendar');
            scheduler.clearAll();
            var events = filterEventsByUser(events,user_id);
            let newEvents = [];
            if(events.length > 0){
                for(let i=0; i<events.length; i++){
                    if(events[i].start != null && events[i].end != null){
                        let client_id = 0;
                        let client_name;
                        var services_ids = [];
                        var services_names = [];
                        if(typeof events[i].clients[0] != 'undefined'){
                            client_id = events[i].clients[0].id;
                            client_name = events[i].clients[0].name;
                        }
                        if(typeof events[i].services != 'undefined'){
                            if(events[i].services.length > 0){
                                for(let j=0; j<events[i].services.length; j++){
                                    services_ids.push(events[i].services[j].id.toString());
                                    services_names.push(events[i].services[j].name.toString());
                                }
                            }
                        }
                        newEvents.push({
                            update:true,
                            text: client_name,
                            services:services_names,
                            id:events[i].id,
                            all_day:events[i].all_day,
                            client_id: client_id,
                            service_id: services_ids,
                            user_id: events[i].user_id,
                            user_name: events[i].user.name,
                            description: events[i].description,
                            start_date: events[i].start,
                            end_date: events[i].end,
                            color:events[i].user.color
                        });
                    }
                }
                scheduler.parse(newEvents,"json");
            }
            console.log('load');
            allSchedule.render();
        }

        /*scale times mode 15/20/30/...*/
        var scaleMode = {
            currentMode: 60,
            modes: [
                {id:15,title:"15 мин",fon:"88",hourSize:88},
                {id:20,title:"20 мин",fon:"88",hourSize:66},
                {id:30,title:"30 мин",fon:"88",hourSize:44},
                {id:60,title:"60 мин",fon:"44",hourSize:44},
            ],
            /*methods*/
            render: function () {
                scheduler.xy.nav_height = 100;
                let wrapper = document.querySelector(".dhx_cal_navline");
                let ul = document.createElement('ul');
                ul.className = 'dhx_scale_times';
                let modes = scaleMode.modes;
                for(let i=0; i<modes.length; i++){
                    let li = document.createElement('li');
                    li.innerHTML = modes[i].title;
                    li.setAttribute('data-mode',modes[i].id);
                    if(modes[i].id == scaleMode.currentMode) li.className = "active";
                    li.addEventListener('click',function(){
                        scaleMode.setMode(modes[i].id);
                    },false);
                    ul.appendChild(li);
                }
                wrapper.appendChild(ul);
            },
            setMode: function(id){
                scaleMode.currentMode = id;
                let ul = document.querySelector(".dhx_scale_times");
                var format = scheduler.date.date_to_str("%H:%i");
                for(let i=0; i<ul.children.length; i++){
                    let mode = ul.children[i].getAttribute('data-mode');
                    if(mode != id){
                        ul.children[i].classList.remove('active');
                    }
                    else {
                        ul.children[i].classList.add('active');
                    }
                }
                let modes = scaleMode.modes;
                for(let i=0; i<modes.length; i++){
                    if(modes[i].id == id){
                        scheduler.config.hour_size_px = modes[i].hourSize;
                        scheduler.templates.hour_scale = function(date){
                            html="";
                            for(let i=0; i<60/id; i++){
                                html+="<div style='height:21px;line-height:21px;'>"+format(date)+"</div>";
                                date = scheduler.date.add(date,id,"minute");
                            }
                            return html;
                        }
                        scheduler.init('scheduler_here', scheduler.getState().date,scheduler.getState().mode);
                        $('.dhx_scale_holder, .dhx_scale_holder_now').css('backgroundImage', "url(/img/scale" + modes[i].fon + ".png)");
                    }
                }
            }
        }

        /*Отобразить панель размера линейки времени*/
        scaleMode.render();
        scheduler.attachEvent("onViewChange", function(new_mode,new_date){
            if(allSchedule.mainSchedule.name != undefined){
                allSchedule.render();
            }
            /*if(new_mode == 'week' || new_mode == 'day'){
            $(".dhx_scale_times li[data-mode='"+ scaleMode.currentMode +"']").trigger('click');
        }*/
            return true;
        });

        scheduler.attachEvent("onBeforeViewChange", function(old_mode,old_date,mode,date){
            spinner.calendar.show();
            return true;
        });


        /*Top menu users*/
        var menuUser = {
            countElements:0,
            currentElement:0,
            getFirstLiWidth: function(){
                let li = document.querySelector("ul > li.first");
                return li.offsetWidth;
            },
            setEvents: function(id,elem){
                __currentUserIdByCalendar__ = id;
                let ul = document.getElementById('menu_user_ul');
                if(ul != null){
                    if(ul.children.length > 0){
                        for(let i=0; i<ul.children.length; i++){
                            ul.children[i].classList.remove('active');
                        }
                    }
                }
                if(elem) {
                    elem.classList.add('active');
                }
                allSchedule.get(id);
            },
            next: function(){
                let width = menuUser.getFirstLiWidth();
                let count = menuUser.countElements;
                let current = menuUser.currentElement;
                let ul = document.getElementById('menu_user_ul');
                var cont = document.getElementById('menu_users');
                var lastLi = ul.lastChild;
                var rect = lastLi.getBoundingClientRect();
                if(rect.left < cont.offsetWidth){
                    document.getElementById('mu_btn_next').classList.add('disabled');
                    return false;
                }
                if(current < count){
                    document.getElementById('mu_btn_prev').classList.remove('disabled');
                    menuUser.currentElement ++;
                    if(ul.children.length > 0){
                        for(let i=0; i<ul.children.length; i++){
                            ul.children[i].classList.remove('first');
                            if(i == menuUser.currentElement) ul.children[i].classList.add('first');
                        }
                    }
                    let left = ul.offsetLeft - width;
                    ul.style.left = left + "px";
                }
                else {
                    document.getElementById('mu_btn_next').classList.add('disabled');
                }
                if(current == count-1){
                    document.getElementById('mu_btn_next').classList.add('disabled');
                }
            },
            prev: function(){
                let current = menuUser.currentElement;
                let ul = document.getElementById('menu_user_ul');
                if(current > 0){
                    document.getElementById('mu_btn_next').classList.remove('disabled');
                    menuUser.currentElement --;
                    if(ul.children.length > 0){
                        for(let i=0; i<ul.children.length; i++){
                            ul.children[i].classList.remove('first');
                            if(i == menuUser.currentElement) ul.children[i].classList.add('first');
                        }
                    }
                    let left = ul.offsetLeft + menuUser.getFirstLiWidth();
                    ul.style.left = left + "px";
                }
                else {
                    document.getElementById('mu_btn_prev').classList.add('disabled');
                }
                if(current == 1){
                    document.getElementById('mu_btn_prev').classList.add('disabled');
                }
            },
            /*show menu*/
            showMenu: function(users){
                if(__authUser__.user_role_id == 2){/*администратор*/
                    menuUser.countElements = users.length;
                    if(users.length > 0){
                        let wrapper = document.getElementById('menu_users');
                        let ul = document.createElement('ul');
                        ul.setAttribute('id','menu_user_ul');
                        let li = document.createElement('li');
                        li.innerHTML = 'Все записи';
                        li.setAttribute('data-id',0);
                        li.className = "first active";
                        ul.appendChild(li);
                        li.addEventListener('click',function(){menuUser.setEvents(0,this)},false);
                        for(let i=0; i<users.length; i++){
                            let li = document.createElement('li');
                            li.innerHTML = users[i].name;
                            li.setAttribute('data-id',users[i].id);
                            li.addEventListener('click',function(){menuUser.setEvents(users[i].id,this)},false);
                            ul.appendChild(li);
                        }
                        let btn_prev = document.createElement('span');
                        btn_prev.setAttribute('id','mu_btn_prev');
                        btn_prev.className = "fa fa-angle-left disabled";
                        let btn_next = document.createElement('span');
                        btn_next.setAttribute('id','mu_btn_next');
                        btn_next.className = "fa fa-angle-right";
                        btn_next.addEventListener('click',menuUser.next,false);
                        btn_prev.addEventListener('click',menuUser.prev,false);

                        wrapper.appendChild(btn_prev);
                        wrapper.appendChild(ul);
                        wrapper.appendChild(btn_next);
                        $('#menu_users ul li.active').trigger('click');
                    }
                }
                else {
                    menuUser.setEvents(__currentUserIdByCalendar__,null);
                }
            }
        };


        var custom_form = document.getElementById("event_modal");
        /**************************************************************
         config
         ***************************************************************/
        scheduler.config.xml_date="%Y-%m-%d %H:%i";
        scheduler.config.resize_month_events = false; // ресайз событий на вкладке месяц
        scheduler.config.drag_resize= false; //позволяет изменять размеры событий путем перетаскивания
        scheduler.config.details_on_create = true; // всегда при создании новой записи вызывать окно с формой
        scheduler.config.max_month_events = 3;/*Максимально количество евентов в ячейке месяца*/
        scheduler.config.auto_end_date = true;
        //    scheduler.config.first_hour = 7;
        //    scheduler.config.last_hour = 22;
        scheduler.config.time_step = 15;
        scheduler.config.active_link_view = "day"; /*клик на день  и переход ко дню*/
        scheduler.config.select = false;
        scheduler.config.show_loading = true;
        scheduler.config.multi_day = true;
        /*******************************************************agenda*****************************************************/
        scheduler.locale.labels.agenda_tab = "Повестка дня";
        /******************************************************************************************************************/
        /*limit*/
        scheduler.config.display_marked_timespans = true;/*Выделять не разрешенные ячейки*/
        scheduler.config.check_limits = true;/*активирует / отключает проверку ограничений*/
        scheduler.config.mark_now = true;/*включает / отключает отображение маркера текущего времени*/
        var startDate = new Date();
        var endDate = new Date();
        startDate.setDate(startDate.getDate()-1); // Set start date to 1 days ago
        endDate.setDate(endDate.getDate()+365); // set end date to one year in the future

        scheduler.config.limit_start = new Date(startDate);
        scheduler.config.limit_end = new Date (endDate);

        /*----------------------------------------------------------------------------*/

        scheduler.templates.event_class = function(start,end,ev){
            return "template-event-body";
        };

        /*Открыть lightbox по одиночному клику*/
        scheduler.attachEvent("onClick", function (id, e){
            scheduler.showLightbox(id);
            return true;
        });

        /*Перетаскивание*/
        scheduler.attachEvent("onBeforeEventChanged", function(ev, e, is_new, original){
            if(is_new) return true;
            var errorText = "";
            let validate = function(){
                let ev_start = ev.start_date.getTime() / 1000;
                let ev_end = ev.end_date.getTime() / 1000;
                let flag = true;
                let events = scheduler.getEvents();
                if(events.length > 0){
                    for(let i=0; i<events.length; i++){
                        if(events[i].user_id == ev.user_id && events[i].id != ev.id){
                            let event_start = new Date(events[i].start_date);
                            let event_end = new Date(events[i].end_date)
                            let event_start_timestamp = event_start.getTime() / 1000;
                            let event_end_timestamp = event_end.getTime() / 1000;
                            if(ev_start >= event_start_timestamp && ev_start < event_end_timestamp){
                                flag = false;
                                errorText = "Сотрудник "+ ev.user_name +", с " + dateToHi(event_start) + " до " + dateToHi(event_end) + " занят!";
                            }
                            if(ev_end >= event_start_timestamp && ev_end <= event_end_timestamp){
                                flag = false;
                                errorText = "Сотрудник "+ ev.user_name +", с " + dateToHi(event_start) + " до " + dateToHi(event_end) + " занят!";
                            }
                        }
                    }
                }
                return flag;
            }
            if(!validate()) {
                swal({
                    type: 'error',
                    title: errorText
                });
                scheduler.getEvent(ev.id).start_date = original.start_date;
                scheduler.getEvent(ev.id).end_date = original.end_date;
                scheduler.updateEvent(ev.id);
                scheduler.updateView();
                return false;
            }

            if(original.start_date.getDate() != ev.start_date.getDate()){
                var start = dateToDmyhis(original.start_date);
                var end = dateToDmyhis(ev.start_date);
            }
            else {
                /*Перенос в рамках одного дня*/
                var start = dateToHI(original.start_date);
                var end = dateToHI(ev.start_date);
            }
            swal({
                title: 'Вы действительно хотите перенести сеанс с ' + start + ' на ' + end + '?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Нет!',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Да!'
            }).then(function(result){
                if (result.value) {
                    ev.all_day = 0;
                    ev.start = dateToYmdHis(ev.start_date);
                    ev.end = dateToYmdHis(ev.end_date);
                    crud.updateEvent(ev,ev.id,scheduler.lightbox.callbackEventUpdated);
                }
                else {
                    scheduler.getEvent(ev.id).start_date = original.start_date;
                    scheduler.getEvent(ev.id).end_date = original.end_date;
                    scheduler.updateEvent(ev.id);
                    scheduler.updateView();
                }
            })
            return true;
        });

        scheduler.attachEvent("onLimitViolation", function  (id, ev){
            var currentDate = Math.floor(new Date().getTime() / 1000);
            var eventDate = Math.floor(ev.start_date.getTime() / 1000) + (24*60*60);
            if(ev.update) return true;
            if(eventDate > currentDate) return true;
            swal({
                title: 'Вы действительно хотите добавить запись на прошедшую дату?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Нет!',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Да!'
            }).then(function(result){
                if (result.value) {
                    scheduler.config.check_limits = false;
                    scheduler.addEvent(ev);
                    scheduler.showLightbox(id);
                }})
        });

        /*срабатывает при инициализации шаблонов планировщика*/
        scheduler.attachEvent("onTemplatesReady", function(){
            console.log('onTemplatesReady');
//        console.log(scheduler.getState());
            scheduler.templates.event_text = function(start,end,ev){
                let services = "";
                if(typeof ev.services != 'undefined'){
                    if(ev.services.length > 0){
                        for(let i=0; i<ev.services.length; i++){
                            services += "<br>- " + ev.services[i];
                        }
                    }
                }
                return "<div><span class='bold-title'>" + ev.text + "</span>"+ services +"</div>";
            };

            scheduler.date.agenda_start = function(date){
                var ndate = new Date(date.valueOf());
                ndate.setDate(Math.floor(date.getDate()/3)*3);
                return this.date_part(ndate);
            }
            scheduler.templates.agenda_date = scheduler.templates.week_date;
            scheduler.templates.agenda_scale_date = scheduler.templates.week_scale_date;
            scheduler.date.add_agenda=function(date,inc){ return scheduler.date.add(date,inc*3,"day"); }

        });

        /*перед открытием lightbox*/
        scheduler.attachEvent( "onBeforeLightbox", function ( id, event ) {
            var check = null;
            var ev = scheduler.getEvent(id);
            if(ev.update) return true;
            check = scheduler.checkInMarkedTimespan(scheduler.getEvent(id), "holiday");
            if(check && ev.checkInMarkedTimespan == undefined){
                scheduler.deleteEvent(id);
                swal({
                    title: 'Вы действительно хотите добавить запись на не рабочее время?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonText: 'Нет!',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да!'
                }).then(function(result){
                    if (result.value) {
                        ev['checkInMarkedTimespan'] = true;
                        scheduler.addEvent(ev);
                        scheduler.showLightbox(ev.id);
                    }
                    else{
                        return false;
                    }
                })
            }
            return true;
        });


        /********************init****************************/
        scheduler.init('scheduler_here', new Date(),"month");

        scheduler.modalcancel = {
            modalId: 'modal_cancel_event',
            btnCloseId: 'mce_form_close',
            btnSaveId: 'mce_btn_save',
            validateWrapperId: 'mce_validate_container',
            event_id:0,
            errors: "",
            showModal: function(){
                document.getElementById(scheduler.modalcancel.modalId).style.display = "block";
            },
            hideModal: function(){
                document.getElementById(scheduler.modalcancel.modalId).style.display = "none";
                document.getElementById('mce_comment').value = "";
                document.getElementById(scheduler.modalcancel.validateWrapperId).innerHTML = "";
                document.getElementsByClassName('mce-comment-container')[0].classList.remove('error');
                scheduler.modalcancel.errors = "";
                document.getElementById('mce_no_show_radio').checked = true;
            },
            applyCancel: function(){
                let type = document.querySelector("input[name='event_cancel']:checked").value;
                let comment = document.getElementById('mce_comment').value;
                if(scheduler.modalcancel.validate()){
                    crud.cancelEvent(scheduler.modalcancel.event_id,type,comment,scheduler.lightbox.callbackEventSoftDeleted);
                    scheduler.deleteEvent(scheduler.modalcancel.event_id);
                    scheduler.modalcancel.hideModal();
                    scheduler.lightbox.hideLightbox(false);
                }
            },
            init: function(event_id){
                scheduler.modalcancel.event_id = event_id;
                document.getElementById(scheduler.modalcancel.btnCloseId).addEventListener('click',function(){scheduler.modalcancel.hideModal()},false);
                document.getElementById(scheduler.modalcancel.btnSaveId).addEventListener('click',function(){scheduler.modalcancel.applyCancel()},false);
            },
            validate: function(){
                let flag = true;
                let comment = document.getElementById('mce_comment').value;
                var wrapper = document.getElementById(scheduler.modalcancel.validateWrapperId);
                wrapper.innerHTML = "";
                document.getElementsByClassName('mce-comment-container')[0].classList.remove('error');
                if(comment == ''){
                    document.getElementsByClassName('mce-comment-container')[0].classList.add('error');
                    flag = false;
                    scheduler.modalcancel.errors = "Оставьте комментарий!";
                    let ul = document.createElement('ul');
                    ul.className = "alert alert-danger";
                    let li = document.createElement('li');
                    li.innerHTML = scheduler.modalcancel.errors;
                    ul.appendChild(li);
                    wrapper.appendChild(ul);
                }
                return flag;
            }
        }

        scheduler.lightbox = {
            config: {
                mode:0,
                /*
            0 - просмотр, редактирование;
            1 - просмотр;
            2 - все запрещено;
            */
                state: false,
                title: 'Добавление записи'
            },
            event: {},
            /*При первом открытии лайтбокса, заполнить поля дефольными значениями*/
            setDefaultValues:function(ev){
                ev.description = '';
            },
            errors: {},
            allEvents: [],
            filterEvents: [],
            client_create_update:null,/*create or update client*/
            /*Расписание сотрудника*/
            schedule: {
                userName:'',
                mainSchedule: {},
                userSchedule: [],
                userExceptions: [],
                timeToMinutes: function (time) {
                    time = time.split(':');
                    var h = parseInt(time[0]);
                    var m = parseInt(time[1]);
                    return (h * 60) + m;
                },
                /*get расписание предприятия, графикы пользователя, исключения пользователя*/
                get: function (user_id) {
                    $.ajax({
                        url: '/api/schedules/' + user_id,
                        type: 'get',
                        dataType: 'json',
                        headers: {
                            'Authorization': 'Bearer ' + safeData.api_token
                        },
                        data: {},
                        success: function (json) {
                            scheduler.lightbox.schedule.mainSchedule = json[0][0];
                            scheduler.lightbox.schedule.userSchedule = json[1];
                            scheduler.lightbox.schedule.userExceptions = json[2];
                            console.log(scheduler.lightbox.schedule.mainSchedule);
                            console.log(scheduler.lightbox.schedule.userSchedule);
                            console.log(scheduler.lightbox.schedule.userExceptions);
                        }
                    });
                },
                /*Поиск даты в обьектах (Исключения, график пользователя, график предприятия)*/
                findDate: function (dateT) {
                    /*Поиск в исключениях*/
                    var schedule = scheduler.lightbox.schedule.userExceptions;
                    if (schedule.length > 0) {
                        for (var i = 0; i < schedule.length; i++) {
                            if (schedule[i].end_date != null) {
                                var date_start = parseInt(schedule[i].start_date) * 1000;
                                var date_end = parseInt(schedule[i].end_date) * 1000;
                                if (dateT >= date_start && dateT <= date_end) {
                                    return {
                                        type: 'exception',
                                        o: schedule[i]
                                    };
                                }
                            }
                            else {
                                var date = parseInt(schedule[i].start_date) * 1000;
                                if (date == dateT) {
                                    return {
                                        type: 'exception',
                                        o: schedule[i]
                                    };
                                }
                            }
                        }
                    }
                    /*поиск в расписании сотрудника*/
                    schedule = scheduler.lightbox.schedule.userSchedule;
                    if (schedule.length > 0) {
                        for (var i = 0; i < schedule.length; i++) {
                            var start = parseInt(schedule[i].start) * 1000;
                            var end = parseInt(schedule[i].end) * 1000;
                            if (dateT >= start && dateT <= end) {
                                var numWeek = new Date(dateT).getDay();
                                if (numWeek == 0) numWeek = 7;
                                days = JSON.parse(schedule[i].days);
                                return {
                                    type: 'user',
                                    date: dateT,
                                    day: days[numWeek],
                                    o: schedule[i]
                                };
                            }
                        }
                    }
                    /*Поиск в расмисании предприятия*/
                    schedule = scheduler.lightbox.schedule.mainSchedule;
                    var numWeek = new Date(dateT).getDay();
                    if (numWeek == 0) numWeek = 7;
                    console.log(schedule.days);
                    var days = JSON.parse(schedule.days);
                    return {
                        type: 'company',
                        date: dateT,
                        day: days[numWeek],
                        o: schedule
                    };
                },
            },
            blocks: {
                start_date : {
                    wrapper_id : "em_date_start",
                    block_id : "event_form_date_start_value",
                    config : {
                        required: true
                    },

                    render : function () {
                        var formatFunc = scheduler.date.date_to_str("%d.%m.%Y");
                        let date = formatFunc(scheduler.lightbox.event.start_date);
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.innerHTML = date;
                        } else {
                            var element = document.createElement("input");
                            element.setAttribute('type','text');
                            element.setAttribute('autocomplete','off');
                            element.value = date;

                        }
                        element.setAttribute ( "id", scheduler.lightbox.blocks.start_date.block_id );
                        wrapper_element.appendChild( element );
                        if(scheduler.lightbox.config.mode == 0){
                            $('#'+scheduler.lightbox.blocks.start_date.block_id).datepicker({
                                format: "dd.mm.yyyy",
                                todayHighlight: true
                            }).on('changeDate', function(event) {
                                let date = event.format();
                                scheduler.lightbox.blocks.end_date.set_value(date);
                            });
                        }
                        let getEvents = function(events){
                            scheduler.lightbox.allEvents = events;
                        }
                        getAllEvents(getEvents);
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.start_date.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            return scheduler.lightbox.event.start_date;  //  value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( scheduler.lightbox.blocks.start_date.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            element.setAttribute ( "innerHTML", value );
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(scheduler.lightbox.errors.start_date);
                        document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.remove('error');
                    },

                    validate: function(){
                        let calendar_start = toTimestamp(formatMdy(scheduler.lightbox.blocks.start_date.get_value()) + " " + scheduler.lightbox.blocks.start_time.get_value());
                        let calendar_end = toTimestamp(formatMdy(scheduler.lightbox.blocks.end_date.get_value()) + " " + scheduler.lightbox.blocks.end_time.get_value());
                        let flag = true;
                        let errorText = "";

                        if(scheduler.lightbox.filterEvents.length > 0){
                            let events = scheduler.lightbox.filterEvents;
                            for(let i=0; i<events.length; i++){
                                let event_start = new Date(events[i].start);
                                let event_end = new Date(events[i].end)
                                let event_start_timestamp = event_start.getTime() / 1000;
                                let event_end_timestamp = event_end.getTime() / 1000;
                                if(calendar_start >= event_start_timestamp && calendar_start < event_end_timestamp){
                                    flag = false;
                                    errorText = "Сотрудник "+ events[i].user.name +", в период с " + dateToDmyhis(event_start) + " до " + dateToDmyhis(event_end) + " занят!";
                                }
                                if(calendar_end >= event_start_timestamp && calendar_end <= event_end_timestamp){
                                    flag = false;
                                    errorText = "Сотрудник "+ events[i].user.name +", в период с " + dateToDmyhis(event_start) + " до " + dateToDmyhis(event_end) + " занят!";
                                }
                            }
                            if(!flag){
                                scheduler.lightbox.errors.date = errorText;
                                document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.add('error');
                            }
                        }
                        else {
                            delete(scheduler.lightbox.errors.date);
                            document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.remove('error');
                        }

                        /*var dateT = new Date(calendar_start * 1000);
                        dateT.setHours(0);
                        dateT.setMinutes(0);
                        dateT.setSeconds(0);
                        var schedule = scheduler.lightbox.schedule.findDate(dateT.getTime());
                        if(schedule.type == 'user' || schedule.type == 'company'){
                            if(schedule.day.status == true){
                                var user_start = scheduler.lightbox.schedule.timeToMinutes(schedule.day.start);
                                var user_end = scheduler.lightbox.schedule.timeToMinutes(schedule.day.end);
                                var cal_start = scheduler.lightbox.schedule.timeToMinutes(scheduler.lightbox.blocks.start_time.get_value());
                                var cal_end = scheduler.lightbox.schedule.timeToMinutes(scheduler.lightbox.blocks.end_time.get_value());
                                if((cal_start < user_start || cal_start > user_end) || (cal_end < user_start || cal_end > user_end)){
                                    flag = false;
                                    errorText = "Не рабочее время сотрудника";
                                    scheduler.lightbox.errors.schedule = errorText;
                                    document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.add('error');
                                }
                            }
                            else {
                                flag = false;
                                errorText = "Сотрудник " + scheduler.lightbox.schedule.userName + " - выходной!";
                                scheduler.lightbox.errors.schedule = errorText;
                                document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.add('error');
                            }
                        }
                        else if(schedule.type == 'exception'){
                            var fullDay;
                            switch(schedule.o.type) {
                                case 1:
                                    fullDay = false;
                                    break;
                                case 2:
                                    fullDay = false;
                                    break;
                                case 3:
                                    fullDay = true;
                                    break;
                                case 4:
                                    fullDay = true;
                                    break;
                                case 5:
                                    fullDay = true;
                                    break;
                                case 6:
                                    fullDay = true;
                                    break;
                                default:
                                    break;
                            }
                            if(!fullDay){
                                var user_start = scheduler.lightbox.schedule.timeToMinutes(schedule.o.start_time);
                                var user_end = scheduler.lightbox.schedule.timeToMinutes(schedule.o.end_time);
                                var cal_start = scheduler.lightbox.schedule.timeToMinutes(scheduler.lightbox.blocks.start_time.get_value());
                                var cal_end = scheduler.lightbox.schedule.timeToMinutes(scheduler.lightbox.blocks.end_time.get_value());
                                if((cal_start < user_start || cal_start > user_end) || (cal_end < user_start || cal_end > user_end)){
                                    flag = false;
                                    errorText = "Исключение в расписании - " + schedule.o.name + " (" + schedule.o.start_time + " - " + schedule.o.end_time + ")";
                                    scheduler.lightbox.errors.schedule = errorText;
                                    document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.add('error');
                                }
                            }
                            else {
                                flag = false;
                                errorText = "Исключение в расписании - " + schedule.o.name;
                                scheduler.lightbox.errors.schedule = errorText;
                                document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id).parentNode.classList.add('error');
                            }
                        }*/
                        return flag;
                    }

                },
                start_time : {
                    wrapper_id : "em_time_start",
                    block_id : "event_form_time_start_value",

                    render : function () {
                        var formatFunc = scheduler.date.date_to_str("%H:%i");
                        var time = formatFunc(scheduler.lightbox.event.start_date);
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.start_time.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.innerHTML = time;
                        } else {
                            var element = document.createElement("input");
                            element.setAttribute('type','time');
                            element.value = time;

                        }
                        element.setAttribute ( "id", scheduler.lightbox.blocks.start_time.block_id );
                        wrapper_element.appendChild( element );
                        if(scheduler.lightbox.config.mode == 0){
//                        $('#'+scheduler.lightbox.blocks.start_time.block_id).timepicker({
//                            'timeFormat': 'H:i',
//                            'minTime': '7:00am',
//                            'maxTime': '21:00am',
//                        });
                            $('#'+scheduler.lightbox.blocks.start_time.block_id).on('change',function(){
                                scheduler.lightbox.setEndTimeEvent();
                            });
                        }
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.start_time.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            return scheduler.lightbox.event.start_date;  //  value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        console.log(value);
                        var element = document.getElementById( scheduler.lightbox.blocks.start_time.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            element.setAttribute ( "innerHTML", value );
                        } else {
                            element.value = value;
                        }
                    },

                },
                end_date : {
                    wrapper_id : "em_date_end",
                    block_id : "event_form_date_end_value",
                    mode:1,
                    render : function () {
                        var formatFunc = scheduler.date.date_to_str("%d.%m.%Y");
                        let date = formatFunc(scheduler.lightbox.event.end_date);
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.end_date.wrapper_id);
                        if (this.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.innerHTML = date;
                        } else {
                            var element = document.createElement("input");
                            element.setAttribute('type','text');
                            element.value = date;

                        }
                        element.setAttribute ( "id", scheduler.lightbox.blocks.end_date.block_id );
                        wrapper_element.appendChild( element );
                        if(this.mode == 0){
                            $('#'+scheduler.lightbox.blocks.end_date.block_id).datepicker({
                                format: "dd.mm.yyyy",
                                todayHighlight: true
                            });
                        }
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.end_date.block_id );
                        if ( this.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( scheduler.lightbox.blocks.end_date.block_id );
                        if ( this.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                },
                end_time : {
                    wrapper_id : "em_time_end",
                    block_id : "event_form_time_end_value",
                    mode:1,
                    render : function () {
                        var formatFunc = scheduler.date.date_to_str("%H:%i");
                        let time = formatFunc(scheduler.lightbox.event.end_date);
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.end_time.wrapper_id);
                        if (this.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.innerHTML = time;
                        } else {
                            var element = document.createElement("input");
                            element.setAttribute('type','text');
                            element.value = time;

                        }
                        element.setAttribute ( "id", scheduler.lightbox.blocks.end_time.block_id );
                        wrapper_element.appendChild( element );
                        if(this.mode == 0){
                            $('#'+scheduler.lightbox.blocks.end_time.block_id).timepicker({
                                'timeFormat': 'H:i',
                                'minTime': '7:00am',
                                'maxTime': '21:00am',
                            });
                        }
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.end_time.block_id );
                        if ( this.mode > 0 ) {
                            return value.innerHTML;  //  value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( scheduler.lightbox.blocks.end_time.block_id );
                        if ( this.mode > 0 ) {
                            element.setAttribute ( "innerHTML", value );
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                },
                user: {
                    wrapper_id : "em_user",
                    block_id : "event_form_user_value",
                    spinner_id : "user_spinner",
                    allUsers:[],
                    config : {
                        required: true,
                        color: '#999'
                    },
                    render : function () {
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.user.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.setAttribute ( "id", scheduler.lightbox.blocks.user.block_id );
                            wrapper_element.appendChild( element );
                            /*Записываем в див*/
                            let usersList = function(users){
                                if(users.length > 0){
                                    for(let i=0; i<users.length; i++){
                                        if(scheduler.lightbox.event.user_id == users[i].id){
                                            element.innerHTML = users[i].name;
                                        }
                                    }
                                }
                            };
                            /*get users from api*/
                            getAllUsers(usersList);
                        } else {
                            var spinner = document.createElement('i');
                            spinner.setAttribute('id',scheduler.lightbox.blocks.user.spinner_id);
                            spinner.className = 'fa fa-spinner fa-spin';
                            wrapper_element.appendChild( spinner );
                            var element = document.createElement("select");
                            // element.value = scheduler.lightbox.event.user_id;
                            let option = document.createElement('option');
                            option.value = 0;
                            option.innerText = 'Выбрать сотрудника';
                            element.appendChild(option);
                            var span_btn = document.createElement('span');
                            span_btn.setAttribute ( "id", 'user_clear_sel_btn' );
                            span_btn.className = 'fa fa-times';
                            span_btn.addEventListener('click',this.clearSelItem);
                            element.setAttribute ( "id", scheduler.lightbox.blocks.user.block_id );
                            /*Заполняем селект*/
                            let usersList = function(users){
                                scheduler.lightbox.blocks.user.allUsers = users;
                                if(users.length > 0){
                                    for(let i=0; i<users.length; i++){
                                        let option = document.createElement('option');
                                        option.value = users[i].id;
                                        option.innerHTML = users[i].name + " - " + users[i].position;
                                        if(__currentUserIdByCalendar__ != 0){
                                            if(option.value == __currentUserIdByCalendar__){
                                                option.setAttribute('selected','selected');
                                                scheduler.lightbox.schedule.userName = users[i].name;
                                                scheduler.lightbox.schedule.get(users[i].id);
                                            }
                                        }
                                        else {
                                            if(option.value == scheduler.lightbox.event.user_id){
                                                option.setAttribute('selected','selected');
                                                scheduler.lightbox.schedule.userName = users[i].name;
                                                scheduler.lightbox.schedule.get(users[i].id);
                                            }
                                        }
                                        element.appendChild(option);
                                    }
                                }
                                scheduler.lightbox.blocks.user.spinnerHide();
                                wrapper_element.appendChild( element );
                                wrapper_element.appendChild( span_btn );
                                $('#' + scheduler.lightbox.blocks.user.block_id).select2();
                                $('#' + scheduler.lightbox.blocks.user.block_id).on('change',function () {
                                    scheduler.lightbox.schedule.userName = $(this).select2('data')[0].text;
                                    scheduler.lightbox.schedule.get($(this).val());
                                    scheduler.lightbox.blocks.user.filterEvents($(this).val());
                                    let users = scheduler.lightbox.blocks.user.allUsers;
                                    if(users.length > 0){
                                        for(let i=0;i<users.length;i++){
                                            if(users[i].id == $(this).val()){
                                                scheduler.lightbox.blocks.user.config.color = users[i].color;
                                            }
                                        }
                                    }
                                    /*Отсортировать услуги, которые предоставляет только этот пользователь*/
                                    var userId = $(this).val();
                                    var filterServices = scheduler.lightbox.blocks.service.allServices.filter(function(o){
                                        return o.users.some(function(id){
                                            return id.id == userId;
                                        })
                                    });
                                    var filterServices = filterServices.reduce(function(res, currentValue) {
                                        if ( res.indexOf(currentValue.category.name) === -1 ) {
                                            res.push(currentValue.category.name);
                                        }
                                        return res;
                                    }, []).map(function(cat) {
                                        return {
                                            category: cat,
                                            items: filterServices.filter(function(_el) {
                                                return _el.category.name === cat;
                                            }).map(function(_el) { return _el; })
                                        }
                                    });
                                    /*Сортировочка*/
                                    /*категорий*/
                                    filterServices.sort(SortByCategory);
                                    /*самых елементов*/
                                    filterServices = filterServices.map(function(o){
                                        return {
                                            category: o.category,
                                            items: o.items.sort(SortByName)
                                        };
                                    });
                                    $('#' + scheduler.lightbox.blocks.service.block_id).select2('destroy');
                                    document.getElementById(scheduler.lightbox.blocks.service.block_id).innerHTML = "";
                                    for(var i=0;i<filterServices.length;i++){
                                        var group = document.createElement('optgroup');
                                        group.setAttribute('label',filterServices[i].category);
                                        for(var j=0;j<filterServices[i].items.length;j++){
                                            var opt = document.createElement('option');
                                            opt.setAttribute('value',filterServices[i].items[j].id);
                                            opt.innerHTML = filterServices[i].items[j].name;
                                            group.appendChild(opt);
                                        }
                                        document.getElementById(scheduler.lightbox.blocks.service.block_id).appendChild(group);
                                    }
                                    $('#' + scheduler.lightbox.blocks.service.block_id).select2({
                                        placeholder : 'Выбрать услугу' });
                                });
                            };
                            /*get users from api*/
                            if(__currentUserIdByCalendar__ > 0){
                                getAllUsers(usersList);
                            }
                            else {
                                getAllUsersByDate(scheduler.lightbox.event.start_date.getTime() / 1000,usersList);
                            }
                        }
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.user.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            return scheduler.lightbox.event.user_id;  //  value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( scheduler.lightbox.blocks.user.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            element.setAttribute ( "innerHTML", value );
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    clearSelItem: function(){
                        let select = document.getElementById(scheduler.lightbox.blocks.user.block_id);
                        if(select.children.length > 0){
                            for(let i=0; i<select.children.length; i++){
                                select.children[i].removeAttribute('selected');
                            }
                        }
                        select.children[0].setAttribute('selected',true);
                        $('#' + scheduler.lightbox.blocks.user.block_id).select2();
                        HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.service.wrapper_id));
                        scheduler.lightbox.blocks.service.render();
                    },

                    spinnerHide:function(){
                        let spinner = document.getElementById(scheduler.lightbox.blocks.user.spinner_id);
                        spinner.parentNode.removeChild(spinner);
                    },

                    filterEvents: function(id){
                        scheduler.lightbox.filterEvents = filterEventsByUser(scheduler.lightbox.allEvents,id);
                    },

                    validateClear: function(){
                        delete(scheduler.lightbox.errors.user);
                        document.getElementById(scheduler.lightbox.blocks.user.wrapper_id).parentNode.classList.remove('error');
                    },

                    validate: function(){
                        if(scheduler.lightbox.blocks.user.get_value() == 0){
                            scheduler.lightbox.errors.user = "Выберите сотрудника";
                            document.getElementById(scheduler.lightbox.blocks.user.wrapper_id).parentNode.classList.add('error');
                            return false;
                        }
                        else {
                            delete(scheduler.lightbox.errors.user);
                            document.getElementById(scheduler.lightbox.blocks.user.wrapper_id).parentNode.classList.remove('error');
                            return true;
                        }
                    }
                },
                client: {
                    wrapper_id : "em_client",
                    block_id : "event_form_client_value",
                    spinner_id : "client_spinner",
                    client_select_id:0,/*выделенный клиент в селекте*/
                    allClients: [],
                    searchText: '',
                    config : {
                        required: true
                    },

                    render : function () {
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.client.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.setAttribute ( "id", scheduler.lightbox.blocks.client.block_id );
                            wrapper_element.appendChild( element );
                            /*Записываем в див*/
                            let clientsList = function(clients){
                                if(clients.length > 0){
                                    for(let i=0; i<clients.length; i++){
                                        if(scheduler.lightbox.event.client_id == clients[i].id){
                                            element.innerHTML = clients[i].name;
                                        }
                                    }
                                }
                            };
                            /*get clients from api*/
                            getAllClients(clientsList);
                        } else {
                            var spinner = document.createElement('i');
                            spinner.setAttribute('id',scheduler.lightbox.blocks.client.spinner_id);
                            spinner.className = 'fa fa-spinner fa-spin';
                            wrapper_element.appendChild( spinner );
                            var element = document.createElement("select");
                            let option = document.createElement('option');
                            option.value = 0;
                            option.innerText = 'Выбрать клиента';
                            element.appendChild(option);
                            /*btn clear*/
                            var span_btn = document.createElement('span');
                            span_btn.setAttribute ( "id", 'client_clear_sel_btn' );
                            span_btn.className = 'fa fa-times';
                            span_btn.addEventListener('click',this.clearSelItem);
                            /*------------------------*/
                            /*btn create*/
                            var btn_create = document.createElement('span');
                            btn_create.setAttribute ( "id", 'client_create_btn' );
                            btn_create.className = 'fa fa-plus';
                            btn_create.addEventListener('click',function(){
                                scheduler.lightbox.add_client();
                            },false);
                            btn_create.addEventListener('mousedown',function(){
                                let search = $('#' + scheduler.lightbox.blocks.client.block_id).data("select2").dropdown.$search.val();
                                scheduler.lightbox.blocks.client.searchText = search;
                            },false);
                            /*------------------------*/
                            /*btn edit*/
                            var btn_edit = document.createElement('span');
                            btn_edit.setAttribute ( "id", 'client_edit_btn' );
                            btn_edit.className = 'fa fa-pen';
                            btn_edit.addEventListener('click',function(){
                                scheduler.lightbox.update_client(scheduler.lightbox.blocks.client.client_select_id);
                            },false);
                            /*------------------------*/
                            element.setAttribute ( "id", scheduler.lightbox.blocks.client.block_id );
                            /*Заполняем селект*/
                            let clientsList = function(clients){
                                clients.sort(SortByName);/*Сортировать по имени*/
                                scheduler.lightbox.blocks.client.allClients = clients;
                                if(clients.length > 0){
                                    for(let i=0; i<clients.length; i++){
                                        let option = document.createElement('option');
                                        option.value = clients[i].id;
                                        let name = clients[i].name + " - " + phoneToMask(clients[i].phone);
                                        if(clients[i].description){
                                            name += ", " + clients[i].description;
                                        }
                                        option.innerHTML = name;
                                        /*текст для строки поиска*/
                                        var search_str = name += " " + clients[i].phone.replace(/[()-]/g, "");
                                        option.setAttribute('data-search',search_str);
                                        if(option.value == scheduler.lightbox.event.client_id){
                                            option.setAttribute('selected','selected');
                                        }
                                        element.appendChild(option);
                                    }
                                }
                                scheduler.lightbox.blocks.client.spinnerHide();
                                wrapper_element.appendChild( element );
                                wrapper_element.appendChild( btn_create );
                                wrapper_element.appendChild( btn_edit );
                                wrapper_element.appendChild( span_btn );
                                $('#' + scheduler.lightbox.blocks.client.block_id).select2({
                                    matcher: matchCustom
                                });
                                $('#' + scheduler.lightbox.blocks.client.block_id).on('change',function () {
                                    scheduler.lightbox.blocks.client.client_select_id = $(this).val();
                                });
                                $('#' + scheduler.lightbox.blocks.client.block_id).trigger('change');
                            };
                            /*get clients from api*/
                            getAllClients(clientsList);
                        }
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.client.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            return scheduler.lightbox.event.client_id;  //  value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    get_value_fio : function (id) {
                        let clients = scheduler.lightbox.blocks.client.allClients;
                        let res = '';
                        if(clients.length > 0){
                            for(let i=0; i<clients.length; i++){
                                if(clients[i].id == id){
                                    res = clients[i].name;
                                }
                            }
                        }
                        return res;
                    },

                    set_value: function (id) {
                        $('#' + scheduler.lightbox.blocks.client.block_id).val(id).trigger('change');
                    },

                    reloadData: function(selected_id){
                        scheduler.lightbox.event.client_id = selected_id;
                        let clientsList = function(clients) {
                            scheduler.lightbox.blocks.client.allClients = clients;
                            $('#' + scheduler.lightbox.blocks.client.block_id).empty();
                            if (clients.length > 0) {
                                for (let i = 0; i < clients.length; i++) {
                                    let name = clients[i].name + " - " + phoneToMask(clients[i].phone);
                                    if(clients[i].description){
                                        name += ", " + clients[i].description;
                                    }
                                    let newOption = new Option(name, clients[i].id, false, false);
                                    $('#' + scheduler.lightbox.blocks.client.block_id).append(newOption).trigger('change');
                                }
                                scheduler.lightbox.blocks.client.set_value(selected_id);
                            }
                        };
                        /*get clients from api*/
                        getAllClients(clientsList);
                    },

                    clearSelItem: function(){
                        let select = document.getElementById(scheduler.lightbox.blocks.client.block_id);
                        if(select.children.length > 0){
                            for(let i=0; i<select.children.length; i++){
                                select.children[i].removeAttribute('selected');
                            }
                        }
                        select.children[0].setAttribute('selected',true);
                        $('#' + scheduler.lightbox.blocks.client.block_id).select2();
                    },

                    spinnerHide:function(){
                        let spinner = document.getElementById(scheduler.lightbox.blocks.client.spinner_id);
                        spinner.parentNode.removeChild(spinner);
                    },

                    validateClear: function(){
                        delete(scheduler.lightbox.errors.client);
                        document.getElementById(scheduler.lightbox.blocks.client.wrapper_id).parentNode.classList.remove('error');
                    },

                    validate: function(){
                        if(scheduler.lightbox.blocks.client.get_value() == 0){
                            scheduler.lightbox.errors.client = "Выберите клиента";
                            document.getElementById(scheduler.lightbox.blocks.client.wrapper_id).parentNode.classList.add('error');
                            return false;
                        }
                        else {
                            delete(scheduler.lightbox.errors.client);
                            document.getElementById(scheduler.lightbox.blocks.client.wrapper_id).parentNode.classList.remove('error');
                            return true;
                        }
                    }
                },
                service : {
                    wrapper_id : "em_service",
                    block_id : "event_form_service_value",
                    spinner_id : "service_spinner",
                    allServices: [],
                    config : {
                        required: true
                    },

                    render : function () {
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.service.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.setAttribute ( "id", scheduler.lightbox.blocks.service.block_id );
                            /*Записываем в див*/
                            let servicesList = function(services){
                                if(services.length > 0){
                                    for(let i=0; i<services.length; i++){
                                        if ($.inArray(services[i].id.toString(), scheduler.lightbox.event.service_id) != -1) {
                                            element.innerHTML += services[i].name + "<br>";
                                        };
                                    }
                                }
                            };
                            wrapper_element.appendChild( element );
                            /*get users from api*/
                            getAllServices(servicesList);
                        } else {
                            var spinner = document.createElement('i');
                            spinner.setAttribute('id',scheduler.lightbox.blocks.service.spinner_id);
                            spinner.className = 'fa fa-spinner fa-spin';
                            wrapper_element.appendChild( spinner );
                            var element = document.createElement("select");
                            var span_btn = document.createElement('span');
                            span_btn.setAttribute ( "id", 'service_clear_sel_btn' );
                            span_btn.className = 'fa fa-times';
                            span_btn.addEventListener('click',function(){
                                scheduler.lightbox.blocks.service.clearSelItem();
                                /*set end time*/
                                scheduler.lightbox.setEndTimeEvent();
                            },false);
                            element.setAttribute ( "id", scheduler.lightbox.blocks.service.block_id );
                            element.setAttribute('multiple',true);
                            /*Заполняем селект*/
                            var servicesList = function(services) {
                                scheduler.lightbox.blocks.service.allServices = services;
                                var filterServices = scheduler.lightbox.blocks.service.allServices;
                                var userId = scheduler.lightbox.blocks.user.get_value();
                                if(userId > 0){
                                    filterServices = scheduler.lightbox.blocks.service.allServices.filter(function(o){
                                        return o.users.some(function(id){
                                            return id.id == userId;
                                        })
                                    });
                                }
                                filterServices = filterServices.reduce(function (res, currentValue) {
                                    if (res.indexOf(currentValue.category.name) === -1) {
                                        res.push(currentValue.category.name);
                                    }
                                    return res;
                                }, []).map(function (cat) {
                                    return {
                                        category: cat,
                                        items: filterServices.filter(function (_el) {
                                            return _el.category.name === cat;
                                        }).map(function (_el) {
                                            return _el;
                                        })
                                    }
                                });
                                /*Сортировочка*/
                                /*категорий*/
                                filterServices.sort(SortByCategory);
                                /*самых елементов*/
                                filterServices = filterServices.map(function(o){
                                    return {
                                        category: o.category,
                                        items: o.items.sort(SortByName)
                                    };
                                });
                                for (var i = 0; i < filterServices.length; i++) {
                                    var group = document.createElement('optgroup');
                                    group.setAttribute('label', filterServices[i].category);
                                    for (var j = 0; j < filterServices[i].items.length; j++) {
                                        var opt = document.createElement('option');
                                        opt.setAttribute('value', filterServices[i].items[j].id);
                                        opt.innerText = filterServices[i].items[j].name + ' - ' + filterServices[i].items[j].duration_text;
                                        if ($.inArray(opt.value, scheduler.lightbox.event.service_id) != -1) {
                                            opt.setAttribute('selected', 'selected');
                                        }
                                        ;
                                        group.appendChild(opt);
                                    }
                                    element.appendChild(group);
                                }
                                scheduler.lightbox.blocks.service.spinnerHide();
                                wrapper_element.appendChild( element );
                                wrapper_element.appendChild( span_btn );
                                $('#' + scheduler.lightbox.blocks.service.block_id).select2({
                                    placeholder : 'Выбрать услугу' });
                                $('#' + scheduler.lightbox.blocks.service.block_id).on('change',function(){
                                    scheduler.lightbox.setEndTimeEvent();
                                });
                                /*set end time*/
                                scheduler.lightbox.setEndTimeEvent();
                            };
                            /*get users from api*/
                            getAllServices(servicesList);
                        }
                    },

                    get_value : function () {
                        let values_ids = [];
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            return scheduler.lightbox.event.service_id;  //  value.innerHTML;
                        } else {
                            $('#' + scheduler.lightbox.blocks.service.block_id + ' option:selected').each(function() {
                                values_ids.push($(this).val());
                            });
                            return values_ids;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( scheduler.lightbox.blocks.service.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            element.setAttribute ( "innerHTML", value );
                        } else {
                            $('#' + scheduler.lightbox.blocks.service.block_id + ' option:selected').each(function() {
                                if ($.inArray($(this).val(), scheduler.lightbox.event.service_id) != -1) {
                                    $(this).prop('selected', true);
                                };
                            });
                        }
                    },

                    clearSelItem: function(){
                        $('#' + scheduler.lightbox.blocks.service.block_id + ' option:selected').each(function() {
                            $(this).prop('selected',false);
                        });
                        $('#' + scheduler.lightbox.blocks.service.block_id).select2({placeholder : 'Выбрать услугу'});
                    },

                    spinnerHide:function(){
                        let spinner = document.getElementById(scheduler.lightbox.blocks.service.spinner_id);
                        spinner.parentNode.removeChild(spinner);
                    },

                    validateClear: function(){
                        delete(scheduler.lightbox.errors.service);
                        document.getElementById(scheduler.lightbox.blocks.service.wrapper_id).parentNode.classList.remove('error');
                    },

                    validate: function(){
                        let val = scheduler.lightbox.blocks.service.get_value()
                        if(val.length == 0){
                            scheduler.lightbox.errors.service = "Выберите услугу";
                            document.getElementById(scheduler.lightbox.blocks.service.wrapper_id).parentNode.classList.add('error');
                            return false;
                        }
                        else {
                            delete(scheduler.lightbox.errors.service);
                            document.getElementById(scheduler.lightbox.blocks.service.wrapper_id).parentNode.classList.remove('error');
                            return true;
                        }
                    }
                },
                comment : {
                    wrapper_id : "em_comment",
                    block_id : "event_form_comment_value",
                    config : {
                        required: false
                    },

                    render : function () {
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.comment.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {
                            var element = document.createElement ( "div" );
                            element.innerHTML = scheduler.lightbox.event.description;
                        } else {
                            var element = document.createElement("textarea");
                            element.value = scheduler.lightbox.event.description;

                        }
                        element.setAttribute ( "id", scheduler.lightbox.blocks.comment.block_id );
                        wrapper_element.appendChild( element );
                    },

                    get_value : function () {
                        var value = document.getElementById( scheduler.lightbox.blocks.comment.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            return scheduler.lightbox.event.description;  //  value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( scheduler.lightbox.blocks.comment.block_id );
                        if ( scheduler.lightbox.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(scheduler.lightbox.errors.comment);
                        document.getElementById(scheduler.lightbox.blocks.comment.wrapper_id).parentNode.classList.remove('error');
                    },

                    validate: function(){
                        if(scheduler.lightbox.blocks.comment.get_value() == ''){
                            scheduler.lightbox.errors.comment = "Оставте комментарий";
                            document.getElementById(scheduler.lightbox.blocks.comment.wrapper_id).parentNode.classList.add('error');
                            return false;
                        }
                        else {
                            delete(scheduler.lightbox.errors.comment);
                            document.getElementById(scheduler.lightbox.blocks.comment.wrapper_id).parentNode.classList.remove('error');
                            return true;
                        }
                    }
                },
                footer : {
                    wrapper_id : "em_footer",

                    render : function () {
                        var wrapper_element = document.getElementById(scheduler.lightbox.blocks.footer.wrapper_id);
                        if (scheduler.lightbox.config.mode > 0) {

                        } else {
                            let btn_save = document.createElement('span');
                            btn_save.setAttribute('id','em_save_btn');
                            btn_save.className = "btn btn-success";
                            btn_save.innerText = "Сохранить";
                            btn_save.addEventListener('click',scheduler.lightbox.event_save,false);

                            if(scheduler.lightbox.event.update){
                                let btn_delete = document.createElement('span');
                                btn_delete.setAttribute('id','em_delete_btn');
                                btn_delete.className = "btn btn-danger";
                                btn_delete.innerText = "Удалить";
                                btn_delete.addEventListener('click',function(){
                                    swal({
                                        title: 'Вы действительно хотите удалить запись?',
                                        // html: "<div style='font-size: 14px'>Вы пытаетесь удалить событие!</div>",
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonText: 'Нет!',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Да!'
                                    }).then(function(result){
                                        if (result.value) {
                                            scheduler.deleteEvent(scheduler.lightbox.event.id);
                                            crud.deleteEvent(scheduler.lightbox.event.id,scheduler.lightbox.callbackEventDeleted);
                                        }
                                    })
                                },false);

                                let btn_cancel = document.createElement('span');
                                btn_cancel.setAttribute('id','em_cancel_btn');
                                btn_cancel.className = "btn btn-warning";
                                btn_cancel.innerText = "Отменить запись";
                                btn_cancel.addEventListener('click',function(){
                                    scheduler.modalcancel.showModal();
                                },false);

                                wrapper_element.appendChild(btn_delete);
                                wrapper_element.appendChild(btn_cancel);
                            }
                            wrapper_element.appendChild(btn_save);
                        }
                    },

                },
                validate : {
                    container_id: "validate-container",
                    wrapper_id : "em_validate",

                    check: function(){
                        scheduler.lightbox.blocks.validate.clear();
                        let flag = true;
                        if (scheduler.lightbox.blocks.start_date.config.required){
                            if(!scheduler.lightbox.blocks.start_date.validate()){
                                flag = false;
                            }
                        }
                        if (scheduler.lightbox.blocks.user.config.required){
                            if(!scheduler.lightbox.blocks.user.validate()){
                                flag = false;
                            }
                        }
                        if (scheduler.lightbox.blocks.client.config.required){
                            if(!scheduler.lightbox.blocks.client.validate()){
                                flag = false;
                            }
                        }
                        if (scheduler.lightbox.blocks.service.config.required){
                            if(!scheduler.lightbox.blocks.service.validate()){
                                flag = false;
                            }
                        }
                        if (scheduler.lightbox.blocks.comment.config.required){
                            if(!scheduler.lightbox.blocks.comment.validate()){
                                flag = false;
                            }
                        }
                        this.showErrors(scheduler.lightbox.errors);
                        return flag;
                    },

                    showErrors: function(){
                        let ul = document.createElement('ul');
                        let container = document.getElementById(scheduler.lightbox.blocks.validate.container_id);
                        container.innerHTML = "";
                        ul.setAttribute('id',scheduler.lightbox.blocks.validate.wrapper_id);
                        ul.className = "alert alert-danger";
                        let errors = scheduler.lightbox.errors;
                        if(Object.keys(errors).length > 0){
                            for(i in errors){
                                let li = document.createElement('li');
                                li.innerHTML = errors[i];
                                ul.appendChild(li);
                            }
                            container.appendChild(ul);
                            document.getElementById(scheduler.lightbox.blocks.validate.wrapper_id).style.display = "block";
                        }
                    },

                    clear: function(){
                        scheduler.lightbox.blocks.start_date.validateClear();
                        scheduler.lightbox.blocks.user.validateClear();
                        scheduler.lightbox.blocks.client.validateClear();
                        scheduler.lightbox.blocks.service.validateClear();
                        scheduler.lightbox.blocks.comment.validateClear();
                        scheduler.lightbox.errors = {};
                        let container = document.getElementById(scheduler.lightbox.blocks.validate.container_id);
                        container.innerHTML = "";
                    }
                },
            },
            /*Закрыть лайтбокс*/
            hideLightbox: function(save) {
                console.log("Закрытие лайт бокса");
                scheduler.lightbox.config.state = false;
                scheduler.lightbox.event = {};
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.comment.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.start_date.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.start_time.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.end_date.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.end_time.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.user.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.client.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.service.wrapper_id));
                HtmlChildRemove(document.getElementById(scheduler.lightbox.blocks.footer.wrapper_id));
                scheduler.lightbox.blocks.validate.clear();
                scheduler.endLightbox(save, custom_form);
            },
            /*установить конечнее время события */
            setEndTimeEvent: function(){
                var time = new Date('01/01/2001 '+ scheduler.lightbox.blocks.start_time.get_value());
                let hour = time.getHours();
                let minutes = time.getMinutes();
                var seconds_start_time = (hour * 60 * 60) + (minutes * 60);

                let service_ids = scheduler.lightbox.blocks.service.get_value();
                let services = scheduler.lightbox.blocks.service.allServices;
                if(services.length > 0){
                    for(let i=0; i<services.length; i++){
                        if($.inArray(services[i].id.toString(), service_ids) != -1){
                            seconds_start_time = parseInt(seconds_start_time) + parseInt(services[i].duration_variable);
                        }
                    }
                }
                let hour_end = Math.floor(seconds_start_time / 60 / 60);
                let minute_end = Math.floor(seconds_start_time / 60) - (hour_end * 60);
                if(hour_end.toString().length == 1) hour_end = "0" + hour_end;
                if(minute_end.toString().length == 1) minute_end = "0" + minute_end;
                let end_time = hour_end + ":" + minute_end;
                $("#event_form_time_end_value").val(end_time);
                $("#event_form_time_end_value").text(end_time);
            },
            /*сохранить событие*/
            event_save: function () {
                /*validate lightbox*/
                if(!scheduler.lightbox.blocks.validate.check()) return false;
                var event = scheduler.getEvent(scheduler.getState().lightbox_id);
                event.description = scheduler.lightbox.blocks.comment.get_value();
                event.user_id = scheduler.lightbox.blocks.user.get_value();
                event.client_id = parseInt(scheduler.lightbox.blocks.client.get_value());
                event.service_id = scheduler.lightbox.blocks.service.get_value();
                var formatFunc = scheduler.date.str_to_date("%d.%m.%Y %H:%i");
                event.start_date = formatFunc(scheduler.lightbox.blocks.start_date.get_value() + " " +scheduler.lightbox.blocks.start_time.get_value());
                event.end_date = formatFunc(scheduler.lightbox.blocks.end_date.get_value() + " " +scheduler.lightbox.blocks.end_time.get_value());
                var text = scheduler.lightbox.blocks.client.get_value_fio(event.client_id);
                $('#' + scheduler.lightbox.blocks.service.block_id + ' option:selected').each(function() {
                    text += "<br>- " + $(this).text();
                });
                event.text = text;
                if ( !event.update ) event.color = scheduler.lightbox.blocks.user.config.color;
                let eventToApi = {
                    start: formatDateToYmdHis(event.start_date),
                    end: formatDateToYmdHis(event.end_date),
                    user_id: event.user_id,
                    service_id: event.service_id,
                    client_id: event.client_id,
                    description: event.description,
                    all_day: "0",
                    color: event.color,
                    temp_id: event.id
                };

                if ( !event.update ) {									//  создание задачи
                    console.log('create event');
                    crud.createEvent(eventToApi,scheduler.lightbox.callbackEventCreated);
                } else {                                                // Редактирование тикущей задачи
                    console.log('update event');
                    crud.updateEvent(eventToApi,event.id,scheduler.lightbox.callbackEventUpdated);
                }
                event.update = true;

                scheduler.updateEvent( scheduler.getState().lightbox_id );
                scheduler.lightbox.hideLightbox(true);
            },
            /*Калбек создания события*/
            callbackEventCreated: function(json){
                if(json.status == 0){
                    scheduler.changeEventId(json.temp_id,json.id);
                    /*scheduler.getEvent(json.id).text = "Conference";
                scheduler.updateEvent(json.id);*/
                    swal({
                        type: 'success',
                        title: 'Событие успешно добавлено!'
                    });
                }
                else if(json.status == 3){
                    scheduler.deleteEvent(json.temp_id);
                    swal({
                        type: 'error',
                        title: 'Ошибка!',
                        html: "<div style='font-size:14px'>Данный промежуток времени занят!</div>"
                    });
                }
                scheduler.config.check_limits = true;
            },
            /*Калбек редактирования события*/
            callbackEventUpdated: function(json){
                if(json.status == 0){
                    swal({
                        type: 'success',
                        title: 'Событие успешно обновлено!'
                    })
                }
                else if(json.status == 3){
                    swal({
                        type: 'error',
                        title: 'Ошибка!',
                        html: "<div style='font-size:14px'>Данный промежуток времени занят!</div>"
                    });
                }
            },
            /*Калбек удаления события*/
            callbackEventDeleted: function(json){
                scheduler.lightbox.hideLightbox(false);
                swal({
                    timer: 3000,
                    showConfirmButton: false,
                    type: 'success',
                    title: 'Выполнено!',
                    html: "<div style='font-size: 14px'>Сеанс удалён!</div>"
                })
            },
            /*Калбек мягкого удаления события*/
            callbackEventSoftDeleted: function(json){
                scheduler.modalcancel.hideModal();
                scheduler.lightbox.hideLightbox(false);
                swal({
                    timer: 3000,
                    showConfirmButton: false,
                    type: 'success',
                    title: 'Выполнено!',
                    html: "<div style='font-size: 14px'>Сеанс отменен!</div>"
                })
            },
            add_client: function(){
                scheduler.lightbox.client_create_update = "create";
                win.create ( modalClient.id, { title : 'Добавить клиента', content : false, width : "570px", height : "660px", center : true } );
                modalClient.render();
                modalClient.blocks.tabs.setTabActive('tab-1');
                win.show( modalClient.id );
            },
            update_client: function(client_id){
                scheduler.lightbox.client_create_update = "update";
                if(client_id != 0){
                    win.create ( modalClient.id, { title : 'Редактировать клиента', content : false, width : "570px", height : "700px", center : true } );
                    modalClient.render();
                    modalClient.blocks.tabs.setTabActive('tab-1');
                    crud.getClientById(client_id,scheduler.lightbox.client_form_filling);

                    win.show( modalClient.id );
                }
            },
            client_form_filling: function(client){
                console.log(client);
                if(client.name != null) modalClient.blocks.fio.set_value(client.name);
                if(client.address != null)  modalClient.blocks.address.set_value(client.address);
                if(client.phone != null)  modalClient.blocks.phone.set_value(phoneToMask(client.phone));
                if(client.email != null)  modalClient.blocks.email.set_value(client.email);
                if(client.description != null)  modalClient.blocks.comment.set_value(client.description);
                if(client.birthday != null)  modalClient.blocks.birthday.set_value(client.birthday);
                if(client.social_links != []) modalClient.blocks.soc_fb.set_value(client.social_links[0]);
                if(client.social_links != []) modalClient.blocks.soc_insta.set_value(client.social_links[1]);
                if(client.phone_additional != null) modalClient.blocks.phone_additional.set_value(client.phone_additional);
                if(client.email_additional != null) modalClient.blocks.email_additional.set_value(client.email_additional);
            }
        };


        /*Открыть лайтбокс*/
        scheduler.showLightbox = function(id){
            scheduler.lightbox.schedule.get();
            if(scheduler.callEvent("onBeforeLightbox", [id, scheduler.lightbox.event]) == false){
                scheduler.lightbox.event = {};
                return false;
            }else{
                scheduler.lightbox.config.state = true;
            }

            var ev = scheduler.getEvent(id);
            if(ev){
                if(!ev.update) {
                    scheduler.lightbox.setDefaultValues(ev);
                    var start_d = ev.start_date;
                    start_d.setHours(0);
                    start_d.setMinutes(0);
                    start_d.setSeconds(0);
                    var start_time = '00:00';
                    var findDate = allSchedule.findDate(start_d.getTime());
                    if(findDate.type == 'user' || findDate.type == 'company'){
                        start_time = findDate.day.start;
                    }
                    else if(findDate.type == 'exception') {
                        start_time = findDate.o.start_time;
                    }
                    if(!start_time) start_time = allSchedule.findCompanyStarttime(start_d.getTime());
                }

                for(i in ev){
                    scheduler.lightbox.event[i] = ev[i];
                }


                /*set config*/
                scheduler.lightbox.config.state = true;
                document.getElementById('em_title').innerText = scheduler.lightbox.config.title;
                document.getElementById('em_form_close').addEventListener('click',function(){
                    scheduler.lightbox.hideLightbox(false);
                },false);

                scheduler.lightbox.blocks.comment.render();
                scheduler.lightbox.blocks.start_date.render();
                scheduler.lightbox.blocks.start_time.render();
                if(!ev.update) scheduler.lightbox.blocks.start_time.set_value(start_time);
                scheduler.lightbox.blocks.end_date.render();
                scheduler.lightbox.blocks.end_time.render();
                scheduler.lightbox.blocks.user.render();
                scheduler.lightbox.blocks.client.render();
                scheduler.lightbox.blocks.service.render();
                scheduler.lightbox.blocks.footer.render();
                scheduler.modalcancel.init(ev.id);

                scheduler.startLightbox(id, custom_form);
            }
        }

        scheduler.attachEvent("onEventSave", function (id, ev, is_new){
            console.log(ev);
            return true;
        }) ;



        /********************MODAL CLIENT**************************/
        var modalClient = {
            id: "modalClient",
            wrapper_main_id: "tab_main",
            wrapper_history_id: "tab_history",
            wrapper_social_id: "mc_wrapper_social",
            wrapper_comment_id: "mc_wrapper_comment",
            wrapper_btns_id: "mc_wrapper_btns",
            wrapper_validate_id: "mc_wrapper_validate",
            config: {
                mode: 0,
                /*
            0 - просмотр, редактирование;
            1 - просмотр;
            */
            },
            errors: {},
            blocks: {
                tabs: {
                    render: function () {
                        let wrapper = document.getElementById(modalClient.id).querySelector(".mod_win_content");
                        let tabs_nav = document.createElement('div');
                        tabs_nav.className = "tabs-nav";
                        let input1 = document.createElement('input');
                        input1.name = "tab";
                        input1.setAttribute('type','radio');
                        input1.setAttribute('id','tab-1');
                        input1.setAttribute('checked','checked');
                        let input2 = document.createElement('input');
                        input2.name = "tab";
                        input2.setAttribute('type','radio');
                        input2.setAttribute('id','tab-2');
                        let label1 = document.createElement('label');
                        label1.setAttribute('for','tab-1');
                        label1.className = "label-tab";
                        label1.innerHTML = "Основная информация";
                        let label2 = document.createElement('label');
                        label2.setAttribute('for','tab-2');
                        label2.className = "label-tab";
                        label2.innerHTML = "История взаимодействия";
                        tabs_nav.appendChild(input1);
                        tabs_nav.appendChild(label1);
                        tabs_nav.appendChild(input2);
                        tabs_nav.appendChild(label2);

                        let tabs_container = document.createElement('div');
                        tabs_container.className = "tabs-container";
                        let tab_1 = document.createElement('div');
                        tab_1.className = "tab tab-1";
                        tab_1.setAttribute('id',modalClient.wrapper_main_id);
                        let tab_2 = document.createElement('div');
                        tab_2.className = "tab tab-2";
                        tab_2.setAttribute('id',modalClient.wrapper_history_id);
                        tab_2.innerHTML = "history";
                        tabs_container.appendChild(tab_1);
                        tabs_container.appendChild(tab_2);
                        tabs_nav.appendChild(tabs_container);
                        wrapper.appendChild(tabs_nav);
                    },
                    setTabActive: function(tab_id){
                        let inputs = document.querySelectorAll("input[name='tab']");
                        if(inputs.length > 0){
                            for(let i=0; i<inputs.length; i++){
                                inputs[i].removeAttribute('checked');
                            }
                        }
                        document.getElementById(tab_id).setAttribute('checked','checked');
                    }
                },
                fio: {
                    block_id: "mc_fio_field",
                    config : {
                        required: true
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let container = document.createElement('div');
                        container.className = "group";
                        let label = document.createElement('label');
                        label.innerHTML = "ФИО*";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.fio.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Введите ФИО');
                            input.setAttribute('id',modalClient.blocks.fio.block_id);
                        }
                        container.appendChild(label);
                        container.appendChild(input);
                        wrapper.appendChild(container);
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.fio.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.fio.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.fio);
                        document.getElementById(modalClient.blocks.fio.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.fio.get_value() == ''){
                            modalClient.errors.fio = "ФИО - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.fio.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.fio);
                            document.getElementById(modalClient.blocks.fio.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                address: {
                    block_id: "mc_address_field",
                    config : {
                        required: false
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let container = document.createElement('div');
                        container.className = "group";
                        let label = document.createElement('label');
                        label.innerHTML = "Адрес";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.address.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Введите адрес');
                            input.setAttribute('id',modalClient.blocks.address.block_id);
                        }
                        container.appendChild(label);
                        container.appendChild(input);
                        wrapper.appendChild(container);
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.address.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.address.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.address);
                        document.getElementById(modalClient.blocks.address.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.address.get_value() == ''){
                            modalClient.errors.address = "Адрес - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.address.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.address);
                            document.getElementById(modalClient.blocks.address.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                birthday: {
                    block_id: "mc_birthday_field",
                    config : {
                        required: false
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let container = document.createElement('div');
                        container.className = "group birthday";
                        let wrapperInput = document.createElement('div');
                        wrapperInput.className = "wrapper-input";
                        let icon = document.createElement('span');
                        icon.className = "fa fa-calendar icon";
                        icon.addEventListener('click',function(){
                            $('#' + modalClient.blocks.birthday.block_id).focus();
                        },false);
                        let label = document.createElement('label');
                        label.innerHTML = "Дата рождения";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.birthday.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Выбрать');
                            input.setAttribute('id',modalClient.blocks.birthday.block_id);
                            icon.style.cursor = "pointer";
                        }

                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        container.appendChild(label);
                        container.appendChild(wrapperInput);
                        wrapper.appendChild(container);
                        $('#' + modalClient.blocks.birthday.block_id).datepicker({
                            format: "dd.mm.yyyy",
                            todayHighlight: true,
                            autoclose: true,
                        });
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.birthday.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        console.log(value);
                        value = formatYmdHisToDmy(value);
                        var element = document.getElementById ( modalClient.blocks.birthday.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.birthday);
                        document.getElementById(modalClient.blocks.birthday.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.birthday.get_value() == ''){
                            modalClient.errors.birthday = "Дата рождения - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.birthday.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.birthday);
                            document.getElementById(modalClient.blocks.birthday.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                phone: {
                    block_id: "mc_phone_field",
                    config : {
                        required: true
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let container = document.createElement('div');
                        container.className = "group";
                        let wrapperInput = document.createElement('div');
                        wrapperInput.className = "wrapper-input";
                        let icon = document.createElement('span');
                        icon.className = "fa fa-phone icon";
                        let label = document.createElement('label');
                        label.innerHTML = "Тел*";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.phone.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Введите телефон');
                            input.setAttribute('id',modalClient.blocks.phone.block_id);
                        }
                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        container.appendChild(label);
                        container.appendChild(wrapperInput);
                        wrapper.appendChild(container);
                        if(modalClient.config.mode == 0){
                            $('#mc_phone_field').mask('+38 (000) 000-00-00',{placeholder: "+38 (___) ___-__-__"});
                        }
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.phone.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.phone.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.phone);
                        document.getElementById(modalClient.blocks.phone.block_id).classList.remove('error');
                    },

                    validate: function(){
                        var phone = modalClient.blocks.phone.get_value();
                        if(phone == ''){
                            modalClient.errors.phone = "Телефон - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.phone.block_id).classList.add('error');
                            return false;
                        }
                        else if(phone.length < 19) {
                            modalClient.errors.phone = "Телефон - слишком короткий ноер!";
                            document.getElementById(modalClient.blocks.phone.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.phone);
                            document.getElementById(modalClient.blocks.phone.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                phone_additional: {
                    block_id: "mc_phone_additional_field",
                    wrapper_phones_id: "mx_wrapper_phone_additional",
                    phone_count: 0,

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let wrapper_phones = document.createElement('div');
                        wrapper_phones.setAttribute('id',modalClient.blocks.phone_additional.wrapper_phones_id);
                        wrapper_phones.className = "wrapper-phones";
                        if ( modalClient.config.mode > 0 ) {

                        }
                        else {
                            let container = document.createElement('div');
                            container.className = "group";
                            container.setAttribute('id','phone_insert');
//                        let label = document.createElement('label');
//                        label.setAttribute('id','phone_additional_label_title');
//                        label.innerHTML = "Тел " + (parseInt(modalClient.blocks.phone_additional.phone_count) + parseInt(2));
                            let btn_add_phone = document.createElement('span');
                            btn_add_phone.className = "fa fa-plus btn-plus";
                            btn_add_phone.addEventListener('click',function(){modalClient.blocks.phone_additional.add_phone()},false);
                            var iText = document.createElement('i');
                            iText.innerText = "Добавить телефон";
//                        container.appendChild(label);
                            btn_add_phone.appendChild(iText);
                            container.appendChild(btn_add_phone);
                            wrapper_phones.appendChild(container);
                        }
                        wrapper.appendChild(wrapper_phones);
                    },

                    add_phone: function ( value = null ) {
                        var block_id = modalClient.blocks.phone_additional.block_id + "_" + modalClient.blocks.phone_additional.phone_count;
                        var wrapper = document.getElementById(modalClient.blocks.phone_additional.wrapper_phones_id);
                        var container = document.createElement('div');
                        container.className = "group";
                        var wrapperInput = document.createElement('div');
                        wrapperInput.className = "wrapper-input";
                        var icon = document.createElement('span');
                        icon.className = "fa fa-phone icon";
                        var label = document.createElement('label');
                        label.innerHTML = "Тел " + (parseInt(modalClient.blocks.phone_additional.phone_count) + parseInt(2));
                        if ( modalClient.config.mode > 0 ) {
                            var input = document.createElement('div');
                            input.setAttribute('id',block_id);
                            input.className = "read_only";
                            input.setAttribute('name','phone_additional[]');
                            if(value) input.innerHTML = value;
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Введите телефон');
                            input.setAttribute('id',block_id);
                            input.name = "phone_additional[]";
                            if(value) input.setAttribute('value', value);
                        }
                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        container.appendChild(label);
                        container.appendChild(wrapperInput);
                        wrapper.insertBefore(container,document.getElementById('phone_insert'));
                        modalClient.blocks.phone_additional.phone_count ++;
                        if(modalClient.config.mode == 0){
                            console.log('#' + block_id);
                            $('#' + block_id).mask('+38 (000) 000-00-00',{placeholder: "+38 (___) ___-__-__"});
                        }
                    },

                    get_value : function () {
                        let phones = document.querySelectorAll("[name='phone_additional[]']");
                        let res = [];
                        if ( modalClient.config.mode > 0 ) {
                            if(phones.length > 0){
                                for(let i=0; i<phones.length; i++ ){
                                    if(phones[i].innerHTML != '') res.push(phones[i].innerHTML);
                                }
                            }
                        } else {
                            if(phones.length > 0){
                                for(let i=0; i<phones.length; i++ ){
                                    if(phones[i].value != '') res.push(phones[i].value);
                                }
                            }
                        }
                        return res;
                    },

                    set_value : function ( values ) { /*array of values*/
                        let phones = JSON.parse(values);
                        if(phones.length > 0){
                            for(let i=0; i<phones.length; i++){
                                modalClient.blocks.phone_additional.add_phone(phones[i]);
                            }
                        }
                    },
                },
                email: {
                    block_id: "mc_email_field",
                    config : {
                        required: false
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let container = document.createElement('div');
                        container.className = "group";
                        let wrapperInput = document.createElement('div');
                        wrapperInput.className = "wrapper-input";
                        let icon = document.createElement('span');
                        icon.className = "fa fa-at icon";
                        let label = document.createElement('label');
                        label.innerHTML = "E-emails";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.email.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Введите e-emails');
                            input.setAttribute('id',modalClient.blocks.email.block_id);
                        }
                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        container.appendChild(label);
                        container.appendChild(wrapperInput);
                        wrapper.appendChild(container);
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.email.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.email.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.email);
                        document.getElementById(modalClient.blocks.email.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.email.get_value() == ''){
                            modalClient.errors.email = "E-emails - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.email.block_id).classList.add('error');
                            return false;
                        }
                        else if(!validateEmail(modalClient.blocks.email.get_value())){
                            modalClient.errors.email = "E-emails - не коректен!";
                            document.getElementById(modalClient.blocks.email.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.email);
                            document.getElementById(modalClient.blocks.email.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                email_additional: {
                    block_id: "mc_email_additional_field",
                    wrapper_emails_id: "mx_wrapper_email_additional",
                    email_count: 0,

                    render: function(){
                        let wrapper = document.getElementById(modalClient.id).querySelector(".left-side");
                        let wrapper_emails = document.createElement('div');
                        wrapper_emails.setAttribute('id',modalClient.blocks.email_additional.wrapper_emails_id);
                        wrapper_emails.className = "wrapper-emails";
                        if ( modalClient.config.mode > 0 ) {

                        }
                        else {
                            let container = document.createElement('div');
                            container.className = "group";
                            container.setAttribute('id','email_insert');
//                        let label = document.createElement('label');
//                        label.setAttribute('id','email_additional_label_title');
//                        label.innerHTML = "E-emails " + (parseInt(modalClient.blocks.email_additional.email_count) + parseInt(2));
                            var iText = document.createElement('i');
                            iText.innerText = "Добавить e-emails";
                            let btn_add_email = document.createElement('span');
                            btn_add_email.className = "fa fa-plus btn-plus";
                            btn_add_email.addEventListener('click',function(){modalClient.blocks.email_additional.add_email()},false);

//                        container.appendChild(label);
                            btn_add_email.appendChild(iText)
                            container.appendChild(btn_add_email);

                            wrapper_emails.appendChild(container);
                        }
                        wrapper.appendChild(wrapper_emails);
                    },

                    add_email: function(value = null){
                        let block_id = modalClient.blocks.email_additional.block_id + "_" + modalClient.blocks.email_additional.email_count;
                        let wrapper = document.getElementById(modalClient.blocks.email_additional.wrapper_emails_id);
                        let container = document.createElement('div');
                        container.className = "group";
                        let wrapperInput = document.createElement('div');
                        wrapperInput.className = "wrapper-input";
                        let icon = document.createElement('span');
                        icon.className = "fa fa-at icon";
                        let label = document.createElement('label');
                        label.innerHTML = "E-emails " + (parseInt(modalClient.blocks.email_additional.email_count) + parseInt(2));
                        if ( modalClient.config.mode > 0 ) {
                            var input = document.createElement('div');
                            input.setAttribute('id',block_id);
                            input.className = "read_only";
                            input.setAttribute('name','email_additional[]');
                            if(value) input.innerHTML = value;
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','Введите e-emails');
                            input.setAttribute('id',block_id);
                            input.name = "email_additional[]";
                            if(value) input.setAttribute('value', value);
                        }
                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        container.appendChild(label);
                        container.appendChild(wrapperInput);
                        wrapper.insertBefore(container,document.getElementById('email_insert'));
                        modalClient.blocks.email_additional.email_count ++;
                    },

                    get_value : function () {
                        let emails = document.querySelectorAll("[name='email_additional[]']");
                        let res = [];
                        if ( modalClient.config.mode > 0 ) {
                            if(emails.length > 0){
                                for(let i=0; i<emails.length; i++ ){
                                    if(emails[i].innerHTML != '') res.push(emails[i].innerHTML);
                                }
                            }
                        } else {
                            if(emails.length > 0){
                                for(let i=0; i<emails.length; i++ ){
                                    if(emails[i].value != '') res.push(emails[i].value);
                                }
                            }
                        }
                        return res;
                    },

                    set_value : function ( values ) { /*array of values*/
                        let emails = JSON.parse(values);
                        if(emails.length > 0){
                            for(let i=0; i<emails.length; i++){
                                modalClient.blocks.email_additional.add_email(emails[i]);
                            }
                        }
                    },
                },
                soc_fb: {
                    block_id: "mc_soc_fb_field",
                    config : {
                        required: false
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.wrapper_social_id);
                        let wrapperInput = document.createElement('div');
                        wrapperInput.className = "soc";
                        let icon = document.createElement('span');
                        icon.className = "fab fa-facebook-f icon";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.soc_fb.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','https://facebook.com');
                            input.setAttribute('id',modalClient.blocks.soc_fb.block_id);
                        }
                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        wrapper.appendChild(wrapperInput);
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.soc_fb.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.soc_fb.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.soc_fb);
                        document.getElementById(modalClient.blocks.soc_fb.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.soc_fb.get_value() == ''){
                            modalClient.errors.soc_fb = "Facebook - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.soc_fb.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.soc_fb);
                            document.getElementById(modalClient.blocks.soc_fb.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                soc_insta: {
                    block_id: "mc_soc_insta_field",
                    config : {
                        required: false
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.wrapper_social_id);
                        let wrapperInput = document.createElement('div');
                        wrapperInput.className = "soc";
                        let icon = document.createElement('span');
                        icon.className = "fab fa-instagram icon";
                        if(modalClient.config.mode > 0){
                            var input = document.createElement('div');
                            input.setAttribute('id',modalClient.blocks.soc_insta.block_id);
                            input.className = "read_only";
                        }
                        else {
                            var input = document.createElement('input');
                            input.setAttribute('type','text');
                            input.setAttribute('placeholder','https://www.instagram.com');
                            input.setAttribute('id',modalClient.blocks.soc_insta.block_id);
                        }
                        wrapperInput.appendChild(input);
                        wrapperInput.appendChild(icon);
                        wrapper.appendChild(wrapperInput);
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.soc_insta.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.soc_insta.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.setAttribute ( "value", value );
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.soc_insta);
                        document.getElementById(modalClient.blocks.soc_insta.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.soc_insta.get_value() == ''){
                            modalClient.errors.soc_insta = "Instagram - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.soc_insta.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.soc_insta);
                            document.getElementById(modalClient.blocks.soc_insta.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                comment: {
                    block_id: "mc_comment_field",
                    config : {
                        required: false
                    },

                    render: function(){
                        let wrapper = document.getElementById(modalClient.wrapper_comment_id);
                        if(modalClient.config.mode > 0){
                            var textarea = document.createElement('div');
                            textarea.setAttribute('id',modalClient.blocks.comment.block_id);
                            textarea.className = "read_only";
                        }
                        else {
                            var textarea = document.createElement('textarea');
                            textarea.setAttribute('id',modalClient.blocks.comment.block_id);
                            textarea.setAttribute('placeholder','Комментарий');
                        }
                        wrapper.appendChild(textarea);
                    },

                    get_value : function () {
                        var value = document.getElementById( modalClient.blocks.comment.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            return value.innerHTML;
                        } else {
                            return value.value;
                        }
                    },

                    set_value : function ( value ) {
                        var element = document.getElementById ( modalClient.blocks.comment.block_id );
                        if ( modalClient.config.mode > 0 ) {
                            element.innerHTML = value;
                        } else {
                            element.value = value;
                        }
                    },

                    validateClear: function(){
                        delete(modalClient.errors.comment);
                        document.getElementById(modalClient.blocks.comment.block_id).classList.remove('error');
                    },

                    validate: function(){
                        if(modalClient.blocks.comment.get_value() == ''){
                            modalClient.errors.comment = "Комментарий - обязательное поле для заполнения!";
                            document.getElementById(modalClient.blocks.comment.block_id).classList.add('error');
                            return false;
                        }
                        else {
                            delete(modalClient.errors.comment);
                            document.getElementById(modalClient.blocks.comment.block_id).classList.remove('error');
                            return true;
                        }
                    }
                },
                btn_save: {
                    block_id: "mc_btn_save_field",

                    render: function(){
                        let wrapper = document.getElementById(modalClient.wrapper_btns_id);
                        if(modalClient.config.mode > 0){

                        }
                        else {
                            var btn = document.createElement('span');
                            btn.setAttribute('id',modalClient.blocks.btn_save.block_id);
                            btn.className = "btn btn-success";
                            btn.innerHTML = "Сохранить изменения";
                            btn.addEventListener('click',function(){
                                if(modalClient.validate.check()){
                                    modalClient.save();
                                }
                            },false);
                            wrapper.appendChild(btn);
                        }
                    }
                },
                btn_cances: {
                    block_id: "mc_btn_cances_field",

                    render: function(){
                        let wrapper = document.getElementById(modalClient.wrapper_btns_id);
                        if(modalClient.config.mode > 0){
                            var btn = document.createElement('span');
                            btn.setAttribute('id',modalClient.blocks.btn_cances.block_id);
                            btn.className = "btn btn-warning";
                            btn.innerHTML = "Отменить";
                        }
                        else {
                            var btn = document.createElement('span');
                            btn.setAttribute('id',modalClient.blocks.btn_cances.block_id);
                            btn.className = "btn btn-warning";
                            btn.innerHTML = "Отменить";
                        }
                        btn.addEventListener('click',function(){win.close(modalClient.id);},false);
                        wrapper.appendChild(btn);
                    }
                },
                btn_make_appointment: {
                    block_id: "mc_btn_make_appointment_field",

                    render: function(){
                        let wrapper = document.getElementById(modalClient.wrapper_main_id).querySelector(".right-side");
                        if(modalClient.config.mode > 0){

                        }
                        else {
                            var btn = document.createElement('span');
                            btn.setAttribute('id',modalClient.blocks.btn_make_appointment.block_id);
                            btn.className = "btn btn-success";
                            btn.innerHTML = "Запись на прием";
                            wrapper.appendChild(btn);
                        }
                    }
                },
            },
            render: function(){
                modalClient.blocks.tabs.render();
                /*Розделяем main tab на две колонки левая и правая  и футтер*/
                let tab_main = document.getElementById(modalClient.wrapper_main_id);
                let body = document.createElement('div');
                body.className = "body";
                let left_side = document.createElement('div');
                left_side.className = "left-side";
                let right_side = document.createElement('div');
                right_side.className = "right-side";
                body.appendChild(left_side);
                body.appendChild(right_side);
                let footer = document.createElement('div');
                footer.className = "footer";
                let social = document.createElement('div');
                social.setAttribute('id',modalClient.wrapper_social_id);
                social.className = "wrapper-social";
                let comment = document.createElement('div');
                comment.setAttribute('id',modalClient.wrapper_comment_id);
                let btns = document.createElement('div');
                btns.setAttribute('id',modalClient.wrapper_btns_id);
                btns.className = "btns-panel";
                let validate = document.createElement('div');
                validate.setAttribute('id',modalClient.wrapper_validate_id);
                validate.className = "validate-container";
                footer.appendChild(social);
                footer.appendChild(comment);
                footer.appendChild(validate);
                footer.appendChild(btns);
                tab_main.appendChild(body);
                tab_main.appendChild(footer);
                /*****************************************************************/
                modalClient.blocks.fio.render();
                modalClient.blocks.address.render();
                modalClient.blocks.birthday.render();
                modalClient.blocks.phone.render();
                modalClient.blocks.phone_additional.render();
                modalClient.blocks.email.render();
                modalClient.blocks.email_additional.render();
                modalClient.blocks.soc_fb.render();
                modalClient.blocks.soc_insta.render();
                modalClient.blocks.comment.render();
                modalClient.blocks.btn_cances.render();
                modalClient.blocks.btn_save.render();
//            modalClient.blocks.btn_make_appointment.render();
                if(scheduler.lightbox.blocks.client.searchText != ''){
                    if(scheduler.lightbox.blocks.client.searchText.match('^[а-яА-Я ]*$') != null){
                        modalClient.blocks.fio.set_value(scheduler.lightbox.blocks.client.searchText);
                    }
                    else {
                        modalClient.blocks.phone.set_value(scheduler.lightbox.blocks.client.searchText);
                        $('#mc_phone_field').focus();
                        $('#mc_phone_field').trigger("input");
                    }
                }
            },
            validate : {
                wrapper_id: "mc_ul_validate",
                check: function(){
                    modalClient.validate.clear();
                    let flag = true;
                    if (modalClient.blocks.fio.config.required){
                        if(!modalClient.blocks.fio.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.address.config.required){
                        if(!modalClient.blocks.address.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.birthday.config.required){
                        if(!modalClient.blocks.birthday.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.phone.config.required){
                        if(!modalClient.blocks.phone.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.email.config.required){
                        if(!modalClient.blocks.email.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.soc_fb.config.required){
                        if(!modalClient.blocks.soc_fb.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.soc_insta.config.required){
                        if(!modalClient.blocks.soc_insta.validate()){
                            flag = false;
                        }
                    }
                    if (modalClient.blocks.comment.config.required){
                        if(!modalClient.blocks.comment.validate()){
                            flag = false;
                        }
                    }
                    this.showErrors(modalClient.errors);
                    return flag;
                },

                showErrors: function(){
                    let ul = document.createElement('ul');
                    let container = document.getElementById(modalClient.wrapper_validate_id);
                    container.innerHTML = "";
                    ul.setAttribute('id',modalClient.validate.wrapper_id);
                    ul.className = "alert alert-danger";
                    let errors = modalClient.errors;
                    if(Object.keys(errors).length > 0){
                        for(i in errors){
                            let li = document.createElement('li');
                            li.innerHTML = errors[i];
                            ul.appendChild(li);
                        }
                        container.appendChild(ul);
                        document.getElementById(modalClient.validate.wrapper_id).style.display = "block";
                    }
                },

                clear: function(){
                    modalClient.blocks.fio.validateClear();
                    modalClient.errors = {};
                    let container = document.getElementById(modalClient.wrapper_validate_id);
                    container.innerHTML = "";
                }
            },
            save: function (){
                let create_update = scheduler.lightbox.client_create_update;
                var birthday = null;
                if(modalClient.blocks.birthday.get_value()){
                    birthday = formatYmdHis(modalClient.blocks.birthday.get_value());
                }
                let data = {
                    name: modalClient.blocks.fio.get_value(),
                    address: modalClient.blocks.address.get_value(),
                    birthday: birthday,
                    description: modalClient.blocks.comment.get_value(),
                    email: modalClient.blocks.email.get_value(),
                    phone: maskToPhone(modalClient.blocks.phone.get_value()),
                    social_links: [modalClient.blocks.soc_fb.get_value(),modalClient.blocks.soc_insta.get_value()],
                    phone_additional: JSON.stringify(modalClient.blocks.phone_additional.get_value()),
                    email_additional: JSON.stringify(modalClient.blocks.email_additional.get_value()),
                }
                if(create_update == 'create'){
                    console.log('create');
                    crud.createClient(data,modalClient.callbackClientCreated);
                }
                else if(create_update == 'update'){
                    console.log('update');
                    crud.updateClient(data, scheduler.lightbox.blocks.client.client_select_id, modalClient.callbackClientUpdated);
                }
            },
            close: function(){
                win.close(modalClient.id);
            },
            /*Калбек создания клиента*/
            callbackClientCreated: function(json){
                modalClient.close();
                swal({
                    timer: 1500,
                    showConfirmButton: false,
                    type: 'success',
                    title: 'Клиент добавлен!',
                });
                scheduler.lightbox.blocks.client.reloadData(json.id);
            },
            /*Калбек обновления клиента*/
            callbackClientUpdated: function(json){
                modalClient.close();
                swal({
                    timer: 1500,
                    showConfirmButton: false,
                    type: 'success',
                    title: 'Клиент обновлен!',
                });
            },
        }


        scheduler.attachEvent("onDataRender", function(){
            /**************************************************************************************************************/
            console.log('onDataRender');
            spinner.calendar.hide();
            /**************************************************************************************************************/
            $(".gray_section").parent('.dhx_month_body').parent('td').addClass("past-day");
        });

        function matchCustom(params, data) {
            if ($.trim(params.term) === '') {
                return data;
            }
            if (typeof data.element.dataset.search === 'undefined') {
                return null;
            }
            var term = params.term.toLowerCase();
            var search = data.element.dataset.search.toLowerCase();
            if (search.indexOf(term) > -1) {
                var modifiedData = $.extend({}, data, true);
                return modifiedData;
            }
            return null;
        }

        /*+38 (111) 111-11-11 to 1111111111*/
        function maskToPhone(mask){
            let phone = "";
            for(let i=3; i<mask.length; i++) {
                if (mask[i].match(/^\d+/)) phone += mask[i];
            }
            return phone;
        }

        /*+38 (111) 111-11-11 to 1111111111*/
        function phoneToMask(phone){
            let mask = "+38 (";
            mask += phone[0] + phone[1] + phone[2] + ") ";
            mask += phone[3] + phone[4] + phone[5] + "-";
            mask += phone[6] + phone[7] + "-";
            mask += phone[8] + phone[9];
            return mask;
        }

        /*Получить get параметр с адресной строки*/
        function gup( name, url ) {
            if (!url) url = location.href;
            name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            var regexS = "[\\?&]"+name+"=([^&#]*)";
            var regex = new RegExp( regexS );
            var results = regex.exec( url );
            return results == null ? 0 : results[1];
        }

        $(document).ready(function () {
            $(".gray_section").parent('.dhx_month_body').parent('td').addClass("past-day");
//        $(".fill-header").parent('.dhx_month_body').parent('td').addClass("past-day");

            $("#agenda_tab").on('click',function(){
                scheduler.updateView(new Date());
            })

//        auto height scheduler
            $('.calendar-container').css('height',window.innerHeight - 40)

            /*наведение на событие в режиме недели дня*/
            $('.calendar-container').on('mouseover','.dhx_cal_event',function(){
                var wrapperHeight = $(this).height();
                var height = $(this).find('.dhx_body div').height() + 10;
                if((wrapperHeight - height) < 18){
                    $(this).find('.dhx_body').height(height);
                }
            })

        });
    </script>
@endpush