@extends('layouts.control')

@section('title', __('Услуги'))

@section('content')
    <div id="app" v-cloak>
        <!--------------------------------------------MODAL ADD-SERVICE------------------------------------------------>
        <div class="overlay" :class="modalService.open">
            <div class="modal-add-service" :class="modalService.open">
                <div class="ms-header">
                    <i class="fa fa-plus"></i>
                    <span class="ms-title">@{{ modalService.title }}</span>
                    <i class="ms-form-close fa fa-times" v-on:click="modalService.close()"></i>
                </div>
                <div class="ms-body">
                    <!------------------category-------------------->
                    <div class="ms-categories-container">
                        <span class="text">Категория</span>
                        <div class="ms-categories">
                            <select2-multiple-control ref="selectCategory" :settings="{multiple: false}" v-model="modalService.service.service_category_id" :options="serviceCategories"></select2-multiple-control>
                            <span class="categories-create-btn fa fa-plus" v-on:click="addCategory()"></span>
                            <span class="categories-edit-btn fa fa-pen" v-on:click="editCategory(modalService.service.service_category_id)"></span>
                            <span class="categories-clear-sel-btn fa fa-trash" v-on:click="clearCategorySelect()"></span>
                        </div>
                    </div>
                    <!---------------------------------------------->
                    <!---------------------name--------------------->
                    <div class="ms-name-container">
                        <span class="text">Название услуги</span>
                        <input type="text" v-model="modalService.service.name">
                    </div>
                    <!---------------------------------------------->
                    <!------------------users-------------------->
                    <div class="ms-users-container">
                        <span class="text">Добавить сотрудника</span>
                        <span class="users-clear-sel-btn fa fa-trash" v-on:click="clearUsersSelect()"></span>
                        <div class="ms-users">
                            <select2-multiple-control ref="selectUsers" :settings="{closeOnSelect:false}" v-model="modalService.service.users" :options="allUsersFromSelect"></select2-multiple-control>
                        </div>
                    </div>
                    <!---------------------------------------------->
                    <!--------------------times--------------------->
                    <div class="ms-times-container">
                        <span class="text">Время выполнения</span>
                        <div class="times-box">
                            <input type="time" v-model="times" placeholder="Время">
                        </div>
                    </div>
                    <!---------------------------------------------->
                    <!---------------------description--------------------->
                    <div class="ms-description-container">
                        <span class="text">Описание</span>
                        <textarea v-model="modalService.service.description"></textarea>
                    </div>
                    <!---------------------------------------------->
                </div>
                <div class="ms-footer">
                    <span class="btn btn-warning" v-on:click="modalService.close()">Отмена</span>
                    <span class="btn btn-success" v-on:click="modalService.save()">Сохранить</span>
                </div>
            </div>
        </div>
        <!------------------------------------------------------------------------------------------------------------->
        <!------------------------------------------MODAL ADD/EDIT CATEGORY-------------------------------------------->
        <div class="modal-add-category" :class="modalCategory.open">
            <div class="mc-header">
                <i class="fa fa-plus"></i>
                <span class="mc-title">@{{ modalCategory.title }}</span>
                <i class="mc-form-close fa fa-times" v-on:click="modalCategory.close()"></i>
            </div>
            <div class="mc-body">
                <!---------------------name--------------------->
                <div class="mc-name-container">
                    <span class="text">Название</span>
                    <input type="text" v-model="modalCategory.category.name">
                </div>
                <!---------------------------------------------->
                <!---------------------description--------------------->
                <div class="mc-description-container">
                    <span class="text">Описание</span>
                    <textarea v-model="modalCategory.category.description"></textarea>
                </div>
                <!---------------------------------------------->
            </div>
            <div class="mc-footer">
                <span class="btn btn-warning" v-on:click="modalCategory.close()">Отмена</span>
                <span class="btn btn-success" v-on:click="modalCategory.save()">Сохранить</span>
            </div>
        </div>
        <!------------------------------------------------------------------------------------------------------------->
        <div class="header">
            <span class="title">Услуги</span>
            <span class="btn-def add-service-btn" v-on:click="addService()">Добавить Услугу</span>
        </div>
        <filter-categories v-bind:categories="serviceCategories"></filter-categories>
        <div class="search-box">
            <i class="fa fa-search icon icon-search"></i>
            <i class="fa fa-times icon icon-close" v-on:click="searchClear()"></i>
            <input type="text" v-model="searchText" @keyup="filterSearch">
            <span class="text-search">Поиск</span>
        </div>
        <services v-bind:services="services"></services>
        <div class="pagination-box">
            <vue-ads-pagination v-if="showPaginateAfterLoad" :total-items="countServices" :page="page" :items-per-page="50" v-on:page-change="pageChange">
                <template slot-scope="props">
                    <div class="pr-2 leading-loose">
                    <!--@{{ start }} - @{{ end }} из @{{ countServices }}-->
                    </div>
                </template>
            </vue-ads-pagination>
        </div>
    </div>
@endsection



@push('css')
    <link rel="stylesheet" href="{{ asset('css/services.css') }}">
@endpush


@push('scripts')
    <script src="{{ asset('components/services/build.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            console.log('jquery test');
        });

        /*Клик вне элемента*/
        $(document).mouseup(function (e) {
            var container = $(".menu");
            if (container.has(e.target).length === 0){
                container.children('ul').removeClass('open');
            }

            var container = $("#filter_modal");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul').removeClass('open');
            }

            var container = $("#filter_modal_roles");
            if (container.has(e.target).length === 0){
                container.find('#filter-ul-roles').removeClass('open');
            }

            /*Открыть поле поиска*/
            $('.icon-search').on('click',function (e) {
                $(this).parent('.search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).css('display','none');
                $(this).siblings('.icon-close').css('display','inline-block');
            });
            /*Открыть поле поиска*/
            $('.text-search').on('click',function (e) {
                $(this).parent('.search-box').addClass('open');
                $(this).siblings('input').focus();
                $(this).siblings('.icon-search').css('display','none');
                $(this).siblings('.icon-close').css('display','inline-block');
            });
            /*Закрыть поле поиска*/
            $('.icon-close').on('click',function (e) {
                $(this).parent('.search-box').removeClass('open');
                $(this).css('display','none');
                $(this).siblings('.icon-search').css('display','inline-block');
            });
        });
    </script>
@endpush