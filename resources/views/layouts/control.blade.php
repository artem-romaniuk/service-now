@include('layouts.header')

<div class="layout">
{{--
    <div class="layout-nav">
        <!-- <a class="brand" href="{{ url('/') }}">
            <img src="{{ asset('assets/img/brand.svg') }}" alt="{{ config('app.name', 'Laravel') }}">
        </a> -->
        
        <!-- Layout profile -->
        <div class="layout-profile">
            <div class="photo">
                <i class="fas fa-camera"></i>
                <!-- <img src="" alt=""> -->
            </div>
            <div class="role"></div>
            <div class="email"></div>
        </div>
        <!--/ Layout profile -->

        <div class="sidenav-divider"></div>

        <!-- Layout navigation -->
        <nav class="nav flex-column">
            <a class="nav-link active" href="#">@lang('Schedule')</a>
            <a class="nav-link" href="#">@lang('Clients')</a>
            <a class="nav-link" href="#">@lang('Users')</a>
            <a class="nav-link" href="#">@lang('Statuses')</a>
            <a class="nav-link" href="#">@lang('Roles')</a>
            <a class="nav-link" href="#">@lang('Services')</a>
            <a class="nav-link" href="#">@lang('Categories')</a>
            <a class="nav-link" href="#">@lang('Sessions')</a>
        </nav>
        <!--/ Layout navigation -->

    </div>
--}}
    <!-- Layout navigation -->
    <!-- <div class="layout-nav">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">@lang('Schedule')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('clients.index') }}">@lang('Clients')</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="usersDropDown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('Users')
                        </a>
                        <div class="dropdown-menu" aria-labelledby="usersDropDown">
                            <a class="dropdown-item" href="{{ route('users.index') }}">@lang('Users')</a>
                            <a class="dropdown-item" href="{{ route('user-statuses.index') }}">@lang('Statuses')</a>
                            <a class="dropdown-item" href="{{ route('user-roles.index') }}">@lang('Roles')</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="servicesDropDown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('Services')
                        </a>
                        <div class="dropdown-menu" aria-labelledby="servicesDropDown">
                            <a class="dropdown-item" href="{{ route('services.index') }}">@lang('Services')</a>
                            <a class="dropdown-item" href="{{ route('service-categories.index') }}">@lang('Categories')</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">@lang('Sessions')</a>
                    </li>
                </ul>
                <div class="ml-auto">
                    <button type="button" class="btn btn-outline-secondary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Logout')</button>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>

        </nav>
    </div> -->
    <!-- / Layout navigation -->



    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif

    <!-- Layout content -->
    <div class="layout-content">
        @yield('content')
    </div>
    <!-- Layout content -->

</div>

@include('layouts.footer')