<script>
    function toogleMenu(){
        document.getElementById('main-menu').classList.toggle('open');
        document.getElementById('toggle-main-menu-btn').classList.toggle('is-active');
    }
    document.getElementById('toggle-main-menu-btn').addEventListener('click',function(){
        toogleMenu();
    },false);
</script>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

@stack('scripts')
</body>
</html>