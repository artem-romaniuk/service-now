<div class="main-menu" id="main-menu">
    <div class="hamburger hamburger--slider" id="toggle-main-menu-btn">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <div class="m-body">
        <aside>
            <div class="m-header">
                <a class="logo" href="{{ route('home') }}">
                    <img src="{{ asset('img/menu-logo.png') }}" alt="logotype">
                </a>
                <span class="name">{{ auth()->user()->name }}</span>
                <span class="position">{{ auth()->user()->position }}</span>
            </div>
            <ul>
                <li @if (url()->current() == route('home'))class="active"@endif><a href="{{ route('home') }}">Календарь</a></li>
                <li @if (url()->current() == route('users.index'))class="active"@endif><a href="{{ route('users.index') }}">Сотрудники</a></li>
                <li @if (url()->current() == route('schedules.index'))class="active"@endif><a href="{{ route('schedules.index') }}">Расписания</a></li>
                <li @if (url()->current() == route('services.index'))class="active"@endif><a href="{{ route('services.index') }}">Услуги</a></li>
                <li @if (url()->current() == route('clients.index'))class="active"@endif><a href="{{ route('clients.index') }}">Клиенты</a></li>

                <li class="nav-item logout-link">
                    <a href="{{ route('logout') }}" class="nav-link disabled" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="{{ trans('auth/login.logout') }}">
                        Выход
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            <div class="m-footer">
                <span class="copyright">&copy; Service Now - 2018</span>
            </div>
        </aside>
    </div>
</div>

<style>
    .main-menu {
        position: absolute;
        width: 40px;
        height: 40px;
        background-color: #fff;
        z-index: 10;
    }
    .main-menu .m-body {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        transform: scaleX(0);
        opacity: 0;
        transition: all .3s;
        display:inline-block;
        transform-origin:left;
        z-index: -1;
    }
    .main-menu aside {
        width: 400px;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: #fff;
    }
    .main-menu.open {
        width: 100%;
        height: 100%;
        background-color: rgba(110, 110, 110, 0.29);
    }
    .main-menu.open .m-body {
        transform: scaleX(1);
        opacity: 1;
        transition: all .3s;
        transform-origin:left;
    }
    /**************************header*************************/
    .main-menu aside .m-header {
        padding: 0 15px 15px 15px;
    }
    .main-menu aside .m-header a {
        display: inline-block;
        width: 200px;
        height: auto;
        margin-left: 50%;
        transform: translateX(-50%);
        margin-top: 2px;
    }
    .main-menu aside .m-header a img {
        width: 100%;
        height: 100%;
    }
    .main-menu aside .m-header .name {
        display: block;
        color: #333;
        padding: 3px 0;
        margin-top: 10px;
    }
    .main-menu aside .m-header .position {
        display: block;
        color: #999;
    }
    /*********************************************************/
    /****************************menu*************************/
    .main-menu aside ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .main-menu aside ul li {
        height: 60px;
        line-height: 60px;
        padding: 0 10px;
        transition: all .3s linear;
        background-color: #fff;
    }
    .main-menu aside ul li a {
        text-decoration: none;
        color: #646464;
        font-size: 22px;
        display: block;
        width: 100%;
        height: 60px;
        line-height: 60px;
        padding: 0 15px;
    }
    .main-menu aside ul li:hover {
        background-color: rgba(75, 116, 255, 0.81);
        transition: all .3s linear;
    }
    .main-menu aside ul li:hover a{
        color: #fff;
        transition: all .3s linear;
    }
    .main-menu aside ul li.active {
        background-color: #4b74ff;
    }
    .main-menu aside ul li.active a {
        color: #fff;
    }
    /*********************************************************/
    /**************************footer*************************/
    .main-menu aside .m-footer {
        position: absolute;
        width: 100%;
        padding: 15px;
        text-align: center;
        left: 0;
        bottom: 0;
    }
    .main-menu aside .m-footer .copyright {
        color: #d6d6d6;
    }
</style>