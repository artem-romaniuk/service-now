<div>
    @auth
        <duv>
            @if (Auth::user()->image != '')
                <img src="{{ Auth::user()->image }}" alt="{{ Auth::user()->name }}" />
            @endif
                <p>{{ Auth::user()->name }}</p>
                <p><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></p>
        </duv>
    @endauth

    <div>
        <ul>
            <li><a href="{{ route('home') }}">@lang('Home')</a></li>
            <li>
                <a href="{{ route('users.index') }}">@lang('Users')</a>
                <ul>
                    <li><a href="{{ route('user-statuses.index') }}">@lang('Statuses')</a></li>
                    <li><a href="{{ route('user-roles.index') }}">@lang('Roles')</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('services.index') }}">@lang('Services')</a>
                <ul>
                    <li><a href="{{ route('service-categories.index') }}">@lang('Categories')</a></li>
                </ul>
            </li>
            <li><a href="{{ route('clients.index') }}">@lang('Clients')</a></li>
            <li><a href="#">@lang('Schedule')</a></li>
            <li><a href="#">@lang('Agenda')</a></li>
            <li><a href="#">@lang('Activity')</a></li>
            <li><a href="#">@lang('Settings')</a></li>
        </ul>
    </div>
    
</div>