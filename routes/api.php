<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api', /*'middleware' => ['auth:api']*/], function ()
{
    Route::resource('users', 'UserController');
    Route::resource('events', 'EventController');
    Route::resource('clients', 'ClientController');
    Route::resource('services', 'ServiceController');
    Route::resource('service-categories', 'ServiceCategoryController');
    Route::resource('stories', 'StoryController');
    Route::resource('story-types', 'StoryTypeController');
    Route::resource('user-roles', 'UserRoleController');
    Route::resource('schedules', 'ScheduleController');
    Route::resource('schedule-exception', 'ScheduleExceptionController');
    Route::resource('sms', 'SmsController');
});
