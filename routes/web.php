<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

//Auth::routes();

/* Authentication Routes */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

/* Password Reset Routes */
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/* Registration Routes
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
*/

Route::group(['namespace' => 'Control', 'middleware' => ['auth']], function ()
{
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('users/user', 'UserController@user');
    Route::resource('users', 'UserController');
    Route::resource('user-roles', 'UserRoleController');
    Route::resource('user-statuses', 'UserStatusController');
    Route::resource('schedules', 'ScheduleController');
    Route::resource('events', 'EventController');
    Route::resource('clients', 'ClientController');
    Route::resource('services', 'ServiceController');
    Route::resource('service-categories', 'ServiceCategoryController');
    Route::resource('stories', 'StoryTypeController');
});

/*
DB::listen(function($query) {
    var_dump($query->sql, $query->bindings);
});
*/