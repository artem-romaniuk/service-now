/*******************************function API*********************************/
var crud = {
    /***************************Events******************************/
    createEvent: function(event,callback){
        $.ajax({
            url:'/api/events',
            type:'post',
            dataType:'json',
            data:{
                // _token: __token__,
                start: event.start,
                end: event.end,
                user_id: event.user_id,
                service_id: event.service_id,
                client_id: event.client_id,
                description: event.description,
                all_day: event.all_day,
                temp_id: event.temp_id
            },
            success: function(json){
                callback(json);
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Произошла ошибка!',
                    // text: 'Something went wrong!',
                })
            }
        });
    },
    updateEvent: function(event, event_id, callback){
        $.ajax({
            url:'/api/events/' + event_id,
            type:'post',
            dataType:'json',
            data:{
                // _token: __token__,
                start: event.start,
                end: event.end,
                user_id: event.user_id,
                service_id: event.service_id,
                client_id: event.client_id,
                description: event.description,
                all_day: event.all_day,
                color: event.color,
                // event_id: event_id,
                _method:'put',
            },
            success: function(json){
                callback(json);
            }
        });
    },
    deleteEvent: function(event_id,callback){
        $.ajax({
            url:'/api/events/'+event_id,
            type:'post',
            dataType:'json',
            data:{
                // _token: __token__,
                _method: 'delete'
            },
            success: function(json){
                callback(json);
            }
        });
    },
    cancelEvent: function(event_id,type,comment,callback){
        $.ajax({
            url:'/api/events/' + event_id,
            type:'post',
            dataType:'json',
            data:{
                // _token: __token__,
                id:event_id,
                type:type,
                comment:comment,
                action: 'cancel',
                _method: 'delete',
            },
            success: function(json){
                callback(json);
            }
        });
    },
    /***************************Clients******************************/
    /*get client by id*/
    getClientById: function(id,callback){
        $.ajax({
            url:'/api/clients/' + id,
            type:'get',
            dataType:'json',
            success:function(json){
                callback(json);
            }
        });
    },
    createClient: function(client,callback){
        $.ajax({
            url:'/api/clients',
            type:'post',
            dataType:'json',
            data:{
                // _token: __token__,
                name: client.name,
                address: client.address,
                birthday: client.birthday,
                phone: client.phone,
                phone_additional: client.phone_additional,
                email: client.email,
                email_additional: client.email_additional,
                description: client.description,
                social_links: client.social_links
            },
            success: function(json){
                callback(json);
            }
        });
    },
    updateClient: function(client,client_id,callback){
        $.ajax({
            url:'/api/clients/'+client_id,
            type:'post',
            dataType:'json',
            data:{
                // _token: __token__,
                name: client.name,
                address: client.address,
                birthday: client.birthday,
                phone: client.phone,
                phone_additional: client.phone_additional,
                email: client.email,
                email_additional: client.email_additional,
                description: client.description,
                social_links: client.social_links,
                _method:'put',
            },
            success: function(json){
                callback(json);
            }
        });
    },
    /****************************Services*****************************/
    /*Получить все категории сервисов*/
    getServiceCategories: function(callback){
        $.ajax({
            url:'/api/service-categories',
            type:'get',
            dataType:'json',
            success:function(json){
                callback(json);
            }
        });
    },
    /*Получить одну категорию сервисов по id*/
    getServiceCategoriesById: function(callback,category_id){
        $.ajax({
            url:'/api/service-categories/' + category_id,
            type:'get',
            dataType:'json',
            success:function(json){
                callback(json);
            }
        });
    },
}
/*******************************function API***********************************/