/**
 * Javascript library for creating windows
 */

$(document).keydown(function(e) {
	
	var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
	
	switch ( key ) {
		case 27 : {
			if ( win._activeWindowId ) {
				win.close ( win._activeWindowId );
				e.stopPropagation();
				e.preventDefault();
				e.cancelBubble = true;
				e.stopImmediatePropagation();
			}
		}
		break;
	}
});


var win = {

	_windows : [],
	_urlString : {},
	_activeWindowId : false,
	_config : {},
	_onLoad : {},
	_onClose : {},

	create : function ( winId, config ) {

		win._config = config;

		//  Remove old window if exists
		if ( document.getElementById ( winId ) ) {
			win.close ( winId );
		}

		// Add new window ID in windows list
		if ( win._windows.indexOf ( winId ) < 0 ) {
			win._windows.push ( winId );
		}
		
		
		
		// Prepare Title
		if ( win._config.title == undefined || ! win._config.title ) {
			win._config.title = "<div class='mod_win_title'>" + locales[user_locale].Information + "</div>";
		} else {
			win._config.title = "<div class='mod_win_title'>" + win._config.title + "</div>";
		}
		
		// Prepare Content
		if ( win._config.content == undefined || ! win._config.content ) {
			win._config.content = "<div class='mod_win_content'></div>";
		} else {
			win._config.content = "<div class='mod_win_content'>" + win._config.content + "</div>";
		}
		
		
		// Create tag-wrapper for new window
		var w = document.createElement ( "div" );
		w.id = winId;
		w.className = "mod_win";
		
		// Set position and sizes
		if ( win._config.top ) {
			w.style.top = win._config.top;
		} else {
			w.style.top = "50px";
		}
		
		if ( win._config.left ) {
			w.style.left = win._config.left;
		}
		
		if ( win._config.right ) {
			w.style.right = win._config.right;
		} else {
			if ( ! win._config.left ) {
				w.style.left = "100px";
			}
		}

		if ( win._config.width ) {
			if ( parseInt(win._config.width) > document.documentElement.clientWidth ) {
				w.style.width = document.documentElement.clientWidth + "px";
			} else {
				w.style.width = win._config.width;
			}
		} else {
			w.style.width = "200px";
		}
		
		if ( win._config.height ) {
			if ( parseInt(win._config.height) > document.documentElement.clientHeight ) {
				w.style.height = document.documentElement.clientHeight + "px";
			} else {
				w.style.height = win._config.height;
			}
		} else {
			w.style.height = "150px";
		}


			if ( win._config.center == true ) {
				
				if ( w.style.height.indexOf ( "%" ) >= 0 ) {
					var _h = ( document.documentElement.clientHeight * parseInt(w.style.height) / 100 );
				} else {
					var _h = parseInt(w.style.height);
				}
				w.style.height = _h + "px";
				
				if ( w.style.width.indexOf ( "%" ) >= 0 ) {
					var _w = ( document.documentElement.clientWidth * parseInt(w.style.width) / 100 );
				} else {
					var _w = parseInt(w.style.width);
				}

				w.style.width = _w + "px";
				//w.style.top = "calc(50% + " + ( parseInt(document.documentElement.clientHeight) - (parseInt(w.style.height)/2) ) + "px)";
				w.style.top = ( parseInt(document.documentElement.clientHeight)/2 - (parseInt(w.style.height)/2) ) + "px";
				w.style.left = "calc(50% - " + (parseInt(w.style.width)/2) + "px)";
			}
		


		w.innerHTML = win._config.title;
		w.innerHTML += "<div class='mod_win_close mod_win_titbtn' data-win-id=" + winId + " title='Закрыть'></div>";
		w.innerHTML += win._config.content;

		document.body.appendChild ( w );
		
		// Set 'mod_win_close' on click handler
		var element_close = document.getElementById(winId).getElementsByClassName ( "mod_win_close" )[0];
		element_close.addEventListener ( "click", function (e) {
			var _winId = e.target.getAttribute('data-win-id');
			if ( _winId ) {
				win.close ( _winId );
			}
		});

		// Add on drag event listener

		var mod_win_title = document.getElementById(winId).getElementsByClassName ( "mod_win_title" )[0];
		var mod_win_content = document.getElementById(winId).getElementsByClassName ( "mod_win_content" )[0];


		mod_win_title.addEventListener ( "touchstart", 

			function (e) {

				e.preventDefault();
	
				mod_win_content.style.display = "none";
	
				// Function moving element to new posiotion, saving shift
				function handleTouchMove (e) {
					w.style.top = e.changedTouches[0].pageY - top_shift + 'px';
					w.style.left = e.changedTouches[0].pageX - left_shift + 'px';
				}
	
				//  Save shift
				var top_shift = e.changedTouches[0].pageY - w.offsetTop;
				var left_shift = e.changedTouches[0].pageX - w.offsetLeft;
	
				// Prepare to move
				w.style.opacity = 0.7;
				w.style.position = "fixed";
				
				handleTouchMove(e);
	
	
				// Handle event 'mousemove' - mode element on page
				document.ontouchmove = function(e) {
					e.preventDefault();
					handleTouchMove(e);
				}
				
				// Handle event 'onmouseup' - reset listeners
				w.ontouchend = function() {
					document.ontouchmove = null;
					w.ontouchend = null;
					w.style.opacity = 1;
					mod_win_content.style.display = "block";
				}
	
			},
		
			{ passive : false }
		);


		
		mod_win_title.onmousedown = function (e) {

			mod_win_content.style.display = "none";

			// Function moving element to new posiotion, saving shift
			function handleMouseMove (e) {
				w.style.left = e.pageX - left_shift + 'px';
				w.style.top = e.pageY - top_shift + 'px';
			}

			//  Save shift
			var top_shift = e.pageY - w.offsetTop;
			var left_shift = e.pageX - w.offsetLeft;

			// Prepare to move
			w.style.opacity = 0.7;
			w.style.position = "fixed";
			
			handleMouseMove(e);


			// Handle event 'mousemove' - mode element on page
			document.onmousemove = function(e) {
				handleMouseMove(e);
			}
			
			// Handle event 'onmouseup' - reset listeners
			w.onmouseup = function() {
				document.onmousemove = null;
				w.onmouseup = null;
				w.style.opacity = 1;
				mod_win_content.style.display = "block";
			}

		}
		
		
		//  Resize 
		
		var res = document.createElement ( "div" );
		res.id = winId + "_resize";
		res.className = "mod_win_resize";
		// document.getElementById(winId).appendChild(res);
		
		res.onmousedown = function (e) {
			// return false;
			
				//console.log ( "onmousedown" );
			
			res.style.width = "100%";
			res.style.height = "100%";
			res.style.backgroundColor = "rgba(255,255,255,0.5)";

			// Function moving element to new posiotion, saving shift
			function handleMouseResize (e) {
					//console.log ( "handleMouseResize" );
				w.style.width = _width_default - ( x_default - e.pageX ) + 'px';
				w.style.height = _height_default - ( y_default - e.pageY ) + 'px';
					/*
					console.log ( "X" );
					console.log ( e.pageX );
					console.log ( e.pageX - left_shift );
					
					console.log ( "Y" );
					console.log ( e.pageY );
					console.log ( e.pageY - top_shift );
					
					
					console.log ( "-------------------" );
					*/
			}

			//  Save shift
			var top_shift = e.pageY - w.offsetTop;
			var left_shift = e.pageX - w.offsetLeft;
			
			var x_default = e.pageX;
			var y_default = e.pageY;
			
			var _width_default = parseInt(w.style.width);
			var _height_default = parseInt(w.style.height);

			// Prepare to Resize
			//w.style.opacity = 0.9;
			w.style.position = "fixed";
			
			handleMouseResize(e);


			// Handle event 'mousemove' - mode element on page
			document.onmousemove = function(e) {
				handleMouseResize(e);
			}
			
			// Handle event 'onmouseup' - reset listeners
			w.onmouseup = function() {
				document.onmousemove = null;
				w.onmouseup = null;
				//w.style.opacity = 1;
				res.style.width = "16px";
				res.style.height = "16px";
				res.style.backgroundColor = "rgba(255,255,255,0.0)";
			}

		}
		

	},
	
	_zIndexReduceAll : function () {
		if ( win._windows.length > 0 ) {
			win._windows.forEach ( function ( item, index , arr ) {
				var _el = document.getElementById(item);
				if ( _el ) {
					_el.style.zIndex = (_el.style.zIndex||2000)-1;
					_el.classList.remove("window_active");
					_el.classList.add("window_inactive");
				}
			});
		}	
	},

	show : function ( winId ) {
		
			//console.log ( win._windows );
		
		// hideMenu();
		
		if ( ! winId ) {
			console.log ( "Empty window ID" );
			return false;
		}
		
		win._zIndexReduceAll();

		var w = document.getElementById ( winId );
		w.style.display = "block";
		w.style.zIndex = 3001;
		w.classList.remove("window_inactive");
		w.classList.add("window_active");
		
		win._activeWindowId = winId;

		w.onmousedown = function (e) {
			win._zIndexReduceAll();
			w.style.zIndex = 3001;
			w.classList.remove("window_inactive");
			w.classList.add("window_active");
			win._activeWindowId = w.id;
		}

	},

	close : function ( winId ) {

		if ( winId ) {

			var remove_target = document.getElementById ( winId );
			
			if ( remove_target.classList.contains ( "win_static_content" ) ) {
				remove_target.style.display = "none";
				remove_target.onmousedown = null;
			} else {
				remove_target.parentNode.removeChild ( remove_target );
			}
			
			win._windows.splice ( win._windows.indexOf ( winId ), 1 );
			win._activeWindowId = false;

			if ( win._windows.length ) {
				win.show ( win._windows[win._windows.length-1] );
			}
			
			
			if ( win._onClose[winId] != undefined && win._onClose[winId].length > 0 ) {
				window[win._onClose[winId]]();
				win._onClose[winId] = false;
			}
			
			if ( win._onLoad[winId] != undefined && win._onLoad[winId].length > 0 ) {
				window[win._onLoad[winId]]();
				win._onLoad[winId] = false;
			}

			win._urlString[winId] = false;

		}

	},
	
	setHtmlContent : function ( winId, content ) {
		var _target = document.getElementById ( winId ); 
		var _target_content = _target.getElementsByClassName ( "mod_win_content" )[0];
		_target_content.innerHTML = content;
	},

	loadUrl : function ( winId, urlString, requstType, postData, inFrame, append ) {

		if ( ! winId ) {
			console.error ( "Define window ID" );
			return false;
		}
		
		if ( requstType != "post" ) {
			win._urlString[winId] = urlString;
		}

		
		var _target_content = document.getElementById ( winId ).getElementsByClassName ( "mod_win_content" )[0];
		
		if ( inFrame ) {
			document.getElementById(winId).classList.add("modal_frame");
			var target_style = window.getComputedStyle(_target_content, null);
			
			var fr = document.createElement("iframe");
			if ( requstType != "post" ) {
				fr.src = urlString;
			}
			fr.name = "frame_" + timestamp();
			fr.width = "100%";
			fr.height = "99.2%"; //(parseInt(target_style.height) - 5) + "px";
			_target_content.appendChild(fr);
		} else {
		
			if ( requstType && requstType == "post" ) {
				win_ajax_post ( postData, urlString, _target_content, "text", win._onLoad[winId]||null, append );
			} else {
				load_url_by_ajax ( urlString, _target_content, win._onLoad[winId]||null, append );
			}

		}

	},
	
	reload : function ( winId ) {
		if ( winId && win._urlString.hasOwnProperty(winId) ) {
			win.loadUrl ( winId, win._urlString[winId] );
		}
	},

	addButtonReload : function ( winId ) {

		var win_title = document.createElement ( "div" );
		win_title.className = "mod_win_reload mod_win_titbtn";
		win_title.setAttribute ( "data-win-id", winId );
		win_title.setAttribute ( "title", locales[user_locale].ReloadWindowContent );

		var w = document.getElementById ( winId );
		w.appendChild ( win_title );

		win_title.addEventListener ( "click", function (e) {
			// data-win-id=" + winId + "
			var _winId = e.target.getAttribute('data-win-id');
			win.reload ( _winId );
		});

	},
	
	
	addButtonSizing : function ( winId ) {

		var _pos_right = (document.getElementById(winId).getElementsByClassName("mod_win_titbtn").length * 28) + "px";
		
		var win_collapse = document.createElement ( "div" );
		win_collapse.style.right = _pos_right;
		win_collapse.className = "mod_win_collapsed mod_win_titbtn";
		win_collapse.setAttribute ( "data-win-id", winId );
		win_collapse.setAttribute ( "title", locales[user_locale].Collapse );
		var w = document.getElementById ( winId );
		w.appendChild ( win_collapse );

		var win_restore = document.createElement ( "div" );
		win_restore.style.right = _pos_right;
		win_restore.className = "mod_win_restore";
		win_restore.setAttribute ( "data-win-id", winId );
		win_restore.setAttribute ( "title", locales[user_locale].Restore );
		win_restore.style.display = "none"; 
		var w = document.getElementById ( winId );
		w.appendChild ( win_restore );
		
		
		
		win_collapse.addEventListener ( "click", function (e) {

			var _winId = e.target.getAttribute('data-win-id');
			var _w = document.getElementById(winId);
			
			// Save position
			_w.setAttribute ( "data-t", _w.style.top );
			_w.setAttribute ( "data-l", _w.style.left );
			_w.setAttribute ( "data-w", _w.style.width );
			_w.setAttribute ( "data-h", _w.style.height );
			
			_w.style.position = "fixed";
			_w.style.top = "auto";
			_w.style.bottom = "0";
			_w.style.left = (document.getElementsByClassName("window_collapsed").length * 320) + "px";
			_w.style.width = "300px";
			_w.style.height = "30px";
			
			// Show  Restore button
			var _rest_btn = document.getElementById ( winId ).getElementsByClassName ( "mod_win_restore" )[0];
			_rest_btn.style.display = "block";
			
			// Hide Collapse button
			var _col_btn = document.getElementById ( winId ).getElementsByClassName ( "mod_win_collapsed" )[0];
			_col_btn.style.display = "none";
			
			// Hide resizing button
			var _col_btn = document.getElementById ( winId ).getElementsByClassName ( "mod_win_resize" )[0];
			_col_btn.style.display = "none";
			
			_w.setAttribute ( "data-stclp", 1 );
			_w.classList.add ( "window_collapsed" );
			
		});
		
		
		win_restore.addEventListener ( "click", function (e) {

			var _winId = e.target.getAttribute('data-win-id');
			var _w = document.getElementById(winId);
			
			//  Restore position
			_w.style.top = _w.getAttribute ( "data-t" );
			_w.style.left = _w.getAttribute ( "data-l" );
			_w.style.width = _w.getAttribute ( "data-w" );
			_w.style.height = _w.getAttribute ( "data-h" );
			_w.style.bottom = "auto";
			
			// Hide Restore button
			var _rest_btn = document.getElementById ( winId ).getElementsByClassName ( "mod_win_restore" )[0];
			_rest_btn.style.display = "none";
			
			// Show Collapse button
			var _col_btn = document.getElementById ( winId ).getElementsByClassName ( "mod_win_collapsed" )[0];
			_col_btn.style.display = "block";
			
			// Show resizing button
			var _col_btn = document.getElementById ( winId ).getElementsByClassName ( "mod_win_resize" )[0];
			_col_btn.style.display = "block";
			
			_w.setAttribute ( "data-stclp", 0 );
			_w.classList.remove ( "window_collapsed" );
			
		});

	},
	
	
	addButtonStacked : function ( winId ) {
		
		// Set 'mod_win_close' on click handler
		
		var _pos_right = (document.getElementById(winId).getElementsByClassName("mod_win_titbtn").length * 28) + "px";
		
		var win_btn_squared = document.createElement ( "div" );
		win_btn_squared.style.right = _pos_right;
		win_btn_squared.className = "ic icon_stacked mod_win_titbtn";
		win_btn_squared.setAttribute ( "data-win-id", winId );
		win_btn_squared.setAttribute ( "title", "Click" );
		var w = document.getElementById ( winId );
		w.appendChild ( win_btn_squared );
		
		var element_close = document.getElementById(winId).getElementsByClassName ( "icon_stacked" )[0];
		element_close.addEventListener ( "click", function (e) {
			var _winId = e.target.getAttribute('data-win-id');
			if ( _winId ) {
				var _w = document.getElementById(winId);
				_w.style.top = "0";
				_w.style.left = "0";
				_w.style.width = "100%";
				_w.style.height = "50%";
			}
		});

	},
	
	onLoad : function ( winId, execFunctionOnClose ) {
		
		if ( ! winId ) {
			console.error ( "Define window ID" );
			return false;
		}
		
		win._onLoad[winId] = execFunctionOnClose;

	},
	
	
	onClose : function ( winId, execFunctionOnClose ) {
		
		if ( ! winId ) {
			console.error ( "Define window ID" );
			return false;
		}

		win._onClose[winId] = execFunctionOnClose;

	},
	
	closeActive : function () {
		win.close ( win._activeWindowId );
	},
	
	register : function ( winId ) {
		win._windows.push ( winId );
	},
	
	getActive : function () {
		return win._activeWindowId;
	},
	
	setActive : function ( winId ) {
		
		win._zIndexReduceAll();

		var w = document.getElementById ( winId );
		w.style.display = "block";
		w.style.zIndex = 3001;
		w.classList.remove("window_inactive");
		w.classList.add("window_active");
		
		win._activeWindowId = winId;

	}

}

function win_close () {
	win.close ( win._activeWindowId );
}

function win_reload () {
	win.reload ( win._activeWindowId );
}



/**
 * @descr Выполнить ajax запрос
 */
function win_ajax_post ( data, path, targetContent, dataType, execFunction, append ) {
	
	if ( ! dataType ) {
		var dataType = "text"; 
	}

	if ( dataType == "json" ) {
		var contentType = "application/json; charset=utf-8";
		data = JSON.stringify(data);
	} else {
		var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
	}


	$.ajax({
		async: true,
		type: "POST",
		url: path,
		data: data,
		cache: false,
		dataType: dataType,
		contentType: contentType,
		beforeSend: function(xhr) {},
		statusCode: {
			400: function() { show_flash_message("Некорректный запрос", "error", 2000); $("#spinner").hide(); },
			403: function() { show_flash_message("Нет доступа", "error", 2000); $("#spinner").hide(); },
			404: function() { show_flash_message("Не найдено", "error", 2000); $("#spinner").hide(); },
			500: function() { show_flash_message("Ошибка", "error", 2000); $("#spinner").hide(); }
		}
	}).done(function(data){

		if ( dataType == "json" ) {
			if ( typeof data == "object" ) {
				//$(targetContent).html ( data.data );
				var content = data.data;
			} else {
				//$(targetContent).html ( data );
				var content = data;
			}
		} else {
			//$(targetContent).html ( data );
			var content = data;
		}
		
		if ( append ) {
			$(targetContent).append ( content );
		} else {
			$(targetContent).html ( content );
		}

		$("#spinner").hide();

		// Do after request
		if ( execFunction != undefined && execFunction.length ) {
			window[execFunction](data);
		}

	});

}


/** Do able draging element by className */

function doDragable ( element, className ) {

	var _title = element.getElementsByClassName(className)[0];

	_title.onmousedown = function ( event ) {

		event = event || window.event;
		event.stopPropagation();

		//  Save shift
		var top_shift = event.pageY - element.offsetTop;
		var left_shift = event.pageX - element.offsetLeft;
	
		// Prepare element to move
		element.style.opacity = 0.7;
		element.style.position = "fixed";
		
		
		// Handle event 'mousemove' - mode element on page
		document.onmousemove = function ( event ) {
			handleMouseMove ( event, element, top_shift, left_shift );
		}
		
		// Handle event 'onmouseup' - reset listeners
		_title.onmouseup = function () {
			document.onmousemove = null;
			element.onmouseup = null;
			element.style.opacity = 1;
		}
		
	};

	// Function moving element to new posiotion, saving shift
	function handleMouseMove ( event, element, _top_shift, _left_shift ) {
		event = event || window.event;
		element.style.left = event.pageX - _left_shift + 'px';
		element.style.top = event.pageY - _top_shift + 'px';
	}

}